<?php
// Template Name: Aereo
get_header('newtmpl');
?>


<main>

	<section id="page-aereo" class="py-5">
			<div class="container aereo">
				<h1 class="text-center text-white text-uppercase bottom-line-header line-center mb-5">Aéreos</h1>

				<!--PESQUISA-->
				<div id="pesquisa" class="py-3 bg-laranja">
					<div class="container">
					<div class="form-check form-check-inline mb-3">
						<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" checked>
						<label class="form-check-label" for="inlineRadio1">Ida e volta</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
						<label class="form-check-label" for="inlineRadio2">Somente ida</label>
					</div>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
						<label class="form-check-label" for="inlineRadio3">Multiplos trechos</label>
					</div>

					<div class="row">
						<div class="col-lg-8">
							<div class="row">
								<!--Origem e destino-->
								<div class="col-12 col-sm-12 col-lg-3">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1"><svg aria-hidden="true" data-prefix="far" data-icon="map-marker-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-map-marker-alt fa-w-12 text-laranja"><path fill="currentColor" d="M192 0C85.903 0 0 86.014 0 192c0 71.117 23.991 93.341 151.271 297.424 18.785 30.119 62.694 30.083 81.457 0C360.075 285.234 384 263.103 384 192 384 85.903 297.986 0 192 0zm0 464C64.576 259.686 48 246.788 48 192c0-79.529 64.471-144 144-144s144 64.471 144 144c0 54.553-15.166 65.425-144 272zm-80-272c0-44.183 35.817-80 80-80s80 35.817 80 80-35.817 80-80 80-80-35.817-80-80z" class=""></path></svg></span>
										</div>
										<input type="text" class="form-control" placeholder="Origem" aria-label="Username" aria-describedby="basic-addon1">
									</div>
								</div>
								<div class="col-12 col-sm-12 col-lg-3">
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1"><svg aria-hidden="true" data-prefix="far" data-icon="map-marker-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-map-marker-alt fa-w-12 text-laranja"><path fill="currentColor" d="M192 0C85.903 0 0 86.014 0 192c0 71.117 23.991 93.341 151.271 297.424 18.785 30.119 62.694 30.083 81.457 0C360.075 285.234 384 263.103 384 192 384 85.903 297.986 0 192 0zm0 464C64.576 259.686 48 246.788 48 192c0-79.529 64.471-144 144-144s144 64.471 144 144c0 54.553-15.166 65.425-144 272zm-80-272c0-44.183 35.817-80 80-80s80 35.817 80 80-35.817 80-80 80-80-35.817-80-80z" class=""></path></svg></span>
										</div>
										<input type="text" class="form-control" placeholder="Destino" aria-label="Username" aria-describedby="basic-addon1">
									</div>
								</div>

								<!--Data-->
								<div class="col-6 col-sm-6 col-lg-3">
									<div class="form-group">
										<div class="input-group date" data-provide="datepicker">
											<div class="input-group-prepend">
												<span class="input-group-text" id="basic-addon1">
													<svg aria-hidden="true" data-prefix="fal" data-icon="calendar-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-calendar-alt fa-w-14 text-laranja"><path fill="currentColor" d="M400 64h-48V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H128V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h352c8.8 0 16 7.2 16 16v48H32v-48c0-8.8 7.2-16 16-16zm352 384H48c-8.8 0-16-7.2-16-16V192h384v272c0 8.8-7.2 16-16 16zM148 320h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 96h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm192 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12z" class=""></path></svg>
												</span>
											</div>
											<input type='text' class="form-control" placeholder="Partida" id="partida" name="partida" />
										</div>
									</div>
								</div>
								<div class="col-6 col-sm-6 col-lg-3">
									<div class="form-group">
										<div class="input-group date" data-provide="datepicker">
											<div class="input-group-prepend">
												<span class="input-group-text" id="basic-addon1">
													<svg aria-hidden="true" data-prefix="fal" data-icon="calendar-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-calendar-alt fa-w-14 text-laranja"><path fill="currentColor" d="M400 64h-48V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H128V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h352c8.8 0 16 7.2 16 16v48H32v-48c0-8.8 7.2-16 16-16zm352 384H48c-8.8 0-16-7.2-16-16V192h384v272c0 8.8-7.2 16-16 16zM148 320h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 96h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm192 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12z" class=""></path></svg>
												</span>
											</div>
											<input type='text' class="form-control" placeholder="Retorno" id="retorno" name="retorno" />
										</div>
									</div>
								</div>
							</div><!--row-->
						</div><!--col-lg-8-->
						
						<div class="col-lg-4">
							<div class="row">
								<!--Select-->
								<div class="col-4 col-sm-3 col-lg-4 pr-0">
									<select class="form-control classic" id="exampleFormControlSelect1">
										<option selected>Adultos</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
								</div>	
								<div class="col-4 col-sm-3 col-lg-4 pr-0">
									<select class="form-control classic" id="exampleFormControlSelect1">
										<option selected>Crianças</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
								</div>	
								<div class="col-4 col-sm-3 col-lg-4">
									<select class="form-control classic" id="exampleFormControlSelect1">
										<option selected>Bebês</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="text-right">
						<a href="#" class="btn btn-lg bg-azul text-white px-5">
							PROCURAR	
						</a>
					</div>
					<a href="page-orcamento.php" class="font-14 nav-link text-center"><u>Não encontrou a viagem que gostaria?</u></a>
				</div>
				</div>
			</div>
		</section>

	<!-- <?php
	require_once (TEMPLATEPATH."/includes/instagram.php");
	?> -->

</main>

<?php get_footer('newtmpl'); ?>