<?php
// Template Name: Contato
get_header('newtmpl');
?>


	<main>
		<section id="contato-header" class="py-5">
			<div class="container">
				<h1 class="text-center text-white text-uppercase bottom-line-header line-center">CONTATO</h1>
			</div>
		</section>

		<section id="page-contato" class="py-5">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<p>Entre em contato, tire suas dúvidas, solicite alguma informação ou deixe sua opinião. Entraremos em contato assim que possível.</p>
						<div class="form-orcamento bg-cinza p-4 mt-4">
							<h5 class="font-weight-bold">Fomulário de contato</h5>
						<!--	<form>
								<div class="row">
									<div class="col-12 col-sm-12 col-lg-12 pb-2">
										<input type="text" class="form-control" placeholder="Nome">
									</div>
									<div class="col-12 col-sm-12 col-lg-6 pb-2">
										<input type="text" class="form-control" placeholder="Email">
									</div>
									<div class="col-12 col-sm-12 col-lg-6 pb-2">
										<input type="text" class="form-control" placeholder="Telefone">
									</div>
									<div class="col-12 col-sm-12 col-lg-12 pb-2">
										<input type="text" class="form-control" placeholder="Assunto">
									</div>
								</div>
								<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" placeholder="Informações complementares"></textarea>
								<button type="submit" class="btn bg-laranja text-white px-5 mt-3 mb-2">ENVIAR</button>
							</form> -->
							<?php echo do_shortcode( '[contact-form-7 id="170" title="Contato"]' ); ?>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="bg-laranja p-5">
							<h3 class="text-azul text-center font-weight-bold">Na Mendes Tur orçamos a viagem ideal para você</h3>
							<p class="text-white text-center mt-3">Acesse o botão a seguir e responda o formulário. Assim que enviado, nossa equipe irá procurar o melhor pacote de viagens para você.</p>
							<p class="text-white text-center">Entraremos em contato o quanto antes com as melhores ofertas e pacotes para você e sua família!</p>
							<a href="<?php echo get_permalink( get_page_by_path( 'orcamento' ) ); ?>" class="btn btn-lg bg-azul text-white text-uppercase mt-3 px-5">
								SOLICITAR ORÇAMENTO
							</a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section id="maps" class="py-5">
			<div class="container">
				<div class="row">
					<div class="col-lg-4 bg-azul pt-5 px-5">
						<h4 class="font-weight-bold text-laranja bottom-line">Miramar Shopping</h4>
						<div class="media mt-4 mb-2">
							<i class="fas fa-map-marker-alt mr-2 mt-2 text-laranja"></i>
							<div class="media-body">
								<a href="https://goo.gl/maps/FVUfuHc3DKy" target="_blank" class="text-white mb-0">Av. Floriano Peixoto, 44 - lj.38 -<br> Santos - SP</a>
							</div>
						</div>
						<div class="media my-3">
							<i class="fas fa-clock mr-2 mt-2 text-laranja"></i>
							<div class="media-body">
								<p class="text-white mb-0">Seg. a Sext. - 10h às 20h<br> Sábado - 10h às 18h</p>
							</div>
						</div>
						<div class="media my-3">
							<i class="fas fa-phone mr-2 mt-1 text-laranja fa-flip-horizontal"></i>
							<div class="media-body">
								<p class="text-white mb-0">(13) 3208-9000</p>
							</div>
						</div>
						<div class="media my-3">
							<i class="fas fa-envelope mr-2 mt-1 text-laranja"></i>
							<div class="media-body">
								<a href="mailto:matriz@mendestur.com.b" class="text-white">matriz@mendestur.com.br</a>
							</div>
						</div>
						<div class="media my-3">
							<i class="fas fa-address-card mr-2 mt-1 text-laranja"></i>
							<div class="media-body">
								<p class="text-white">CNPJ:  57.954.299/0001-30</p>
							</div>
						</div>
					</div>
					<div class="col-lg-8 mapa p-0">
						<iframe src="https://snazzymaps.com/embed/65925" width="100%" height="370px" style="border:none;"></iframe>
					</div>
					<p id="container-como-chegar" style="position: absolute;">
						<a id="como-chegar" target="_blank" href="https://goo.gl/maps/YTHhKFWkVTt" class="btn bg-laranja text-white btn-sm"">
							Como chegar
						</a>
					</p>
				</div>

				<div class="row mt-5">
					<div class="col-lg-4 bg-azul pt-5 px-5">
						<h4 class="font-weight-bold text-laranja bottom-line">Praimar Shopping</h4>
						<div class="media mt-4 mb-2">
							<i class="fas fa-map-marker-alt mr-2 mt-2 text-laranja"></i>
							<div class="media-body">
								<a href="https://goo.gl/maps/Xt3TrRFRfKm" target="_blank" class="text-white mb-0">Rua Alexandre Martins, 80 - lj. 101 -<br> Santos - SP</a>
							</div>
						</div>
						<div class="media my-3">
							<i class="fas fa-clock mr-2 mt-2 text-laranja"></i>
							<div class="media-body">
								<p class="text-white mb-0">Seg. a Sext. - 10h às 21h<br> Sábado - 10h às 18h</p>
							</div>
						</div>
						<div class="media my-3">
							<i class="fas fa-phone mr-2 mt-1 text-laranja fa-flip-horizontal"></i>
							<div class="media-body">
								<p class="text-white mb-0">(13) 3279-9000</p>
							</div>
						</div>
						<div class="media my-3">
							<i class="fas fa-envelope mr-2 mt-1 text-laranja"></i>
							<div class="media-body">
								<a href="mailto:matriz@mendestur.com.b" class="text-white">filial@mendestur.com.br</a>
							</div>
						</div>
						<div class="media my-3">
							<i class="fas fa-address-card mr-2 mt-1 text-laranja"></i>
							<div class="media-body">
								<p class="text-white">CNPJ:  57.954.299/0002-11</p>
							</div>
						</div>
					</div>
					<div class="col-lg-8 mapa p-0">
						<iframe src="https://snazzymaps.com/embed/65931" width="100%" height="370px" style="border:none;"></iframe>
					</div>
					<p id="container-como-chegar" style="position: absolute;">
						<a id="como-chegar" target="_blank" href="https://goo.gl/maps/pETsV4SbcX72" class="btn bg-laranja text-white btn-sm"">
							Como chegar
						</a>
					</p>
				</div>
			</div>
		</section>

	 	<!-- <?php
        require_once (TEMPLATEPATH."/includes/instagram.php");
        ?> -->

    </main>

<?php get_footer('newtmpl'); ?>