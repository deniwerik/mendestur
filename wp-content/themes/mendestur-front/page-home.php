<?php // Template Name: Home
get_header('newtmpl');
?>


   <main>
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/img/carousel-01.jpg" alt="First slide">
                    <div class="hero">
                        <div class="row">
                            <div class="col-sm-12 col-lg-2 align-self-stretch d-flex align-items-center bg-white p-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/logo-abreu.jpg" class="img-fluid img-logo">
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-stretch p-3 border-right">
                                <h4 class="mb-0 text-uppercase">Tesouros da turquia</h4>
                                <p class="mb-0">Saída até março de 2019</p>
                                <h4 class="text-laranja mb-0 font-weight-bold">10 dias</h4>
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-center p-3">
                                <p class="mb-0 font-14">A partir de</p>
                                <p class="mb-0 text-laranja font-44 font-weight-bold">R$ 167,60</p>
                                <p class="mb-0 font-14 text-laranja font-weight-bold">Total R$ 1.676,00</p>
                            </div>
                            <div class="col-sm-4 col-lg-2 align-self-stretch d-flex align-items-center p-0">
                                <a href="http://mendestur.com.br/destinos/tesouros-da-turquia/" class="btn btn-lg btn-border text-white">
                                    CONFIRA
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/img/banner/buenos-aires-02.png" alt="First slide">
                    <div class="hero">
                        <div class="row">
                            <div class="col-sm-12 col-lg-2 align-self-stretch d-flex align-items-center bg-white p-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/banner/logo-ancoradouro.png" class="img-fluid img-logo">
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-stretch p-3 border-right">
                                <h4 class="mb-0 text-uppercase">Buenos Aires</h4>
                                <p class="mb-0">Embarque 06/09 a 09/09</p>
                                <h4 class="text-laranja mb-0 font-weight-bold">10 dias</h4>
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-center p-3">
                                <p class="mb-0 font-14">Entrada de USD 215,00 +</p>
                                <p class="mb-0 text-laranja font-44 font-weight-bold">9x USD 56,00</p>
                                <p class="mb-0 font-14 text-laranja font-weight-bold">Total: USD 719,00</p>
                            </div>
                            <div class="col-sm-4 col-lg-2 align-self-stretch d-flex align-items-center p-0">
                                <a href="http://mendestur.com.br/destinos/buenos-aires/" class="btn btn-lg btn-border text-white">
                                    CONFIRA
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="carousel-item">
                    <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/img/banner/bariloche.jpg" alt="First slide">
                    <div class="hero">
                        <div class="row">
                            <div class="col-sm-12 col-lg-2 align-self-stretch d-flex align-items-center bg-white p-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/banner/logo-cvc.png" class="img-fluid img-logo">
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-stretch p-3 border-right">
                                <h4 class="mb-0 text-uppercase">Bariloche</h4>
                                <p class="mb-0">Saída: 07/julho</p>
                                <h4 class="text-laranja mb-0 font-weight-bold">08 dias</h4>
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-center p-3">
                                <p class="mb-0 font-14">Por apenas:</p>
                                <p class="mb-0 text-laranja font-44 font-weight-bold">12x R$ 414,00</p>
                                <p class="mb-0 font-14 text-laranja font-weight-bold">Total: R$ 4.968,00</p>
                            </div>
                            <div class="col-sm-4 col-lg-2 align-self-stretch d-flex align-items-center p-0">
                                <a href="http://mendestur.com.br/destinos/bariloche/" class="btn btn-lg btn-border text-white">
                                    CONFIRA
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="carousel-item">
                    <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/img/banner/foz-do-iguacu.jpg" alt="First slide">
                    <div class="hero">
                        <div class="row">
                            <div class="col-sm-12 col-lg-2 align-self-stretch d-flex align-items-center bg-white p-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/banner/logo-ancoradouro.png" class="img-fluid img-logo">
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-stretch p-3 border-right">
                                <h4 class="mb-0 text-uppercase">Foz do Iguacú</h4>
                                <p class="mb-0">Embarque: 12/07 a 15/07</p>
                                <h4 class="text-laranja mb-0 font-weight-bold">03 noites</h4>
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-center p-3">
                                <p class="mb-0 font-14">Entrada de R$ 342,00 +</p>
                                <p class="mb-0 text-laranja font-44 font-weight-bold">9x R$ 89,00</p>
                                <p class="mb-0 font-14 text-laranja font-weight-bold">Total: R$ 1.143,00</p>
                            </div>
                            <div class="col-sm-4 col-lg-2 align-self-stretch d-flex align-items-center p-0">
                                <a href="http://mendestur.com.br/destinos/foz-do-iguacu/" class="btn btn-lg btn-border text-white">
                                    CONFIRA
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="carousel-item">
                    <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/img/banner/circuito-de-portugal.jpg" alt="First slide">
                    <div class="hero">
                        <div class="row">
                            <div class="col-sm-12 col-lg-2 align-self-stretch d-flex align-items-center bg-white p-4">
                                 <img src="<?php echo get_template_directory_uri(); ?>/img/logo-abreu.jpg" class="img-fluid img-logo">
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-stretch p-3 border-right">
                                <h4 class="mb-0 text-uppercase">Circuito de Portugual</h4>
                                <p class="mb-0">Saída: até março/2019</p>
                                <h4 class="text-laranja mb-0 font-weight-bold">10 dias/09 noites</h4>
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-center p-3">
                                <p class="mb-0 font-14">A partir de:</p>
                                <p class="mb-0 text-laranja font-44 font-weight-bold">10x R$ 534,70</p>
                                <p class="mb-0 font-14 text-laranja font-weight-bold">Total: R$ 5.347,00</p>
                            </div>
                            <div class="col-sm-4 col-lg-2 align-self-stretch d-flex align-items-center p-0">
                                <a href="http://mendestur.com.br/destinos/circuito-de-portugal/" class="btn btn-lg btn-border text-white">
                                    CONFIRA
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="carousel-item">
                    <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/img/banner/italia-brilhante.jpg" alt="First slide">
                    <div class="hero">
                        <div class="row">
                            <div class="col-sm-12 col-lg-2 align-self-stretch d-flex align-items-center bg-white p-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/banner/logo-ancoradouro.png" class="img-fluid img-logo">
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-stretch p-3 border-right">
                                <h4 class="mb-0 text-uppercase">Itália Brilhante</h4>
                                <p class="mb-0">Saídas: 2018/2019</p>
                                <h4 class="text-laranja mb-0 font-weight-bold">09 dias/07 noites</h4>
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-center p-3">
                                <p class="mb-0 font-14">A partir de:</p>
                                <p class="mb-0 text-laranja font-44 font-weight-bold">Total: 695€</p>
                            </div>
                            <div class="col-sm-4 col-lg-2 align-self-stretch d-flex align-items-center p-0">
                                <a href="http://mendestur.com.br/destinos/italia-brilhante/" class="btn btn-lg btn-border text-white">
                                    CONFIRA
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="carousel-item">
                    <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/img/banner/triangulo-magico.jpg" alt="First slide">
                    <div class="hero">
                        <div class="row">
                            <div class="col-sm-12 col-lg-2 align-self-stretch d-flex align-items-center bg-white p-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/banner/logo-ancoradouro.png" class="img-fluid img-logo">
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-stretch p-3 border-right">
                                <h4 class="mb-0 text-uppercase">Triângulo mágico</h4>
                                <p class="mb-0">Saídas: 2018</p>
                                <h4 class="text-laranja mb-0 font-weight-bold">09 dias</h4>
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-center p-3">
                                <p class="mb-0 font-14">Entrada de R$ 835,56 + </p>
                                <p class="mb-0 text-laranja font-44 font-weight-bold">9x R$ 216,62</p>
                                <p class="mb-0 font-14 text-laranja font-weight-bold">Total: R$ 2.785,20|660€</p>
                            </div>
                            <div class="col-sm-4 col-lg-2 align-self-stretch d-flex align-items-center p-0">
                                <a href="http://mendestur.com.br/destinos/triangulo-magico-praga-budapeste-e-viena/" class="btn btn-lg btn-border text-white">
                                    CONFIRA
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="carousel-item">
                    <img class="d-block w-100" src="<?php echo get_template_directory_uri(); ?>/img/banner/marrocos-espetacular.jpg" alt="First slide">
                    <div class="hero">
                        <div class="row">
                            <div class="col-sm-12 col-lg-2 align-self-stretch d-flex align-items-center bg-white p-4">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/banner/logo-ancoradouro.png" class="img-fluid img-logo">
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-stretch p-3 border-right">
                                <h4 class="mb-0 text-uppercase">Marrocos Espetacular</h4>
                                <p class="mb-0">Saídas: 2018/2019</p>
                                <h4 class="text-laranja mb-0 font-weight-bold">08 dias</h4>
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-center p-3">
                                <p class="mb-0 font-14">Entrada de R$ 677,31 + </p>
                                <p class="mb-0 text-laranja font-44 font-weight-bold">9x R$ 175,59</p>
                                <p class="mb-0 font-14 text-laranja font-weight-bold">Total: R$ 2.257,70|535€</p>
                            </div>
                            <div class="col-sm-4 col-lg-2 align-self-stretch d-flex align-items-center p-0">
                                <a href="http://mendestur.com.br/destinos/marrocos-espetacular/" class="btn btn-lg btn-border text-white">
                                    CONFIRA
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <img src="<?php echo get_template_directory_uri(); ?>/img/icons/arrows-01.svg" class="prev-icon">
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <img src="<?php echo get_template_directory_uri(); ?>/img/icons/arrows-02.svg" class="prev-icon">
                <span class="sr-only">Next</span>
            </a>
        </div>


		<?php
		require_once (TEMPLATEPATH."/includes/busca.php");
		?>

        <!-- <section id="banners" class="py-5">
            <div class="container">
                <div class="bg-01">
                    <div class="hover-zoom" style="background-position: center top; background-image: url(<?php echo get_template_directory_uri(); ?>/img/01_Banner_home_fixo.jpg);">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/01_Banner_home_fixo.jpg" class="img-fluid d-block d-sm-none">
                        <div class="bg-skew-azul">
                            <div class="px-4 no-skew">
                                <h4 class="text-white text-uppercase bottom-line">Cruzeiros Exclusivos</h4>
                                <h5 class="text-white">A primeira na Baixada Santista a oferecer os melhores cruzeiros para você e sua família!</h5>
                                <a href="/cruzeiro" class="btn btn-lg btn-border text-laranja px-5 mt-2">
                                    CONFIRA
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="row pt-3">
                        <div class="col-sm-6 col-lg-6">
                            <div class="hover-zoom" style="background-position: center top; background-image: url(<?php echo get_template_directory_uri(); ?>/img/bg-02.jpg);">
                                <div class="hero text-center">
                                    <h5 class="text-uppercase font-weight-light">Conheça a neve</h5>
                                    <h4 class="text-uppercase font-weight-bold">Planeje suas férias de Inverno</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-6">
                            <div class="hover-zoom" style="background-position: center top; background-image: url(<?php echo get_template_directory_uri(); ?>/img/bg-03.jpg);">
                                <div class="hero text-center">
                                    <h5 class="text-uppercase font-weight-light">As maravilhas do nordeste</h5>
                                    <h4 class="text-uppercase font-weight-bold">Viagem de 8 dias para Maceió</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section> -->

		<?php
		/*$taxonomy     = 'categoria_do_cruzeiro';
		$orderby      = 'name';
		$show_count   = false;
		$pad_counts   = false;
		$hierarchical = true;
		$title        = '';

		$args = array(
			'taxonomy'     => $taxonomy,
			'orderby'      => $orderby,
			'show_count'   => $show_count,
			'pad_counts'   => $pad_counts,
			'hierarchical' => $hierarchical,
			'title_li'     => $title
		);
		wp_list_categories( $args );*/

		//        $taxonomy = 'categoria_do_cruzeiro';
		//					$tax_terms = get_terms(["taxonomy"=>$taxonomy,"hide_empty"=>true]);
		//
		//					foreach ($tax_terms as $tax_term) {
		//						$frota = get_field( 'frota', $tax_term );
		//						/*echo '<div class="category-grid-box">
		//                                <div class="category-grid-content">' .
		//                                    '<a href="' . esc_attr(get_term_link($tax_term, $taxonomy)) . '" title="' . sprintf( __( "View all posts in %s" ), $tax_term->name ) . '" ' . '>' . $tax_term->name.'</a>
		//                                </div>
		//                              </div>';*/
		//
		//						/* $category_classes = array();
		//						 $categories = get_the_terms( 'categoria_do_cruzeiro', array( 'slug' => 'msc'  ));
		//						 if ( $categories ) {
		//							 foreach ( $categories as $category ) {
		//								 $categories_included[] = $category->term_id;
		//								 $color = get_field('color', 'project_category_'.$category->term_id);
		//							 }
		//						 }*/
		//					}
		?>

        <section id="cruzeiros" class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="input-group">
                            <h3 class="text-uppercase text-white align-self-center mb-0 mr-3">CRUZEIROS</h3>
                            <a href="<?php echo get_permalink( get_page_by_path( 'cruzeiro' ) ); ?>" class="btn btn-lg btn-border text-white px-4">
                                VER TODOS
                            </a>
                        </div>
                    </div>
                </div>



                <div id="slider-carousel" class="owl-carousel owl-theme pt-5">


					<?php
					/*$args = array (
						'post_type' => 'cruzeiros',
						'order'   => 'ASC'
					);
					$the_query = new WP_Query ( $args );

					if ($the_query->have_posts()){
						while ($the_query->have_posts()){
							$the_query->the_post();
							*/

					$taxonomy = 'categoria_do_cruzeiro';
					/*$tax_terms = get_terms(["taxonomy"=>$taxonomy,"hide_empty"=>false]);*/
					$tax_terms = get_terms( array(
						"taxonomy"=>$taxonomy,
                        "hide_empty"=>false
					));

					foreach ($tax_terms as $tax_term) {
						$bglogo = get_field( 'bglogo_cruzeiro', $tax_term );
						$logo_cruzeiro = get_field('logo_cruzeiro', $tax_term);
						$imagem_cruzeiro = get_field("imagem_cruzeiro",$tax_term);
						$titulo = get_field('titulo_cruzeiro',$tax_term);
						$slug = get_template_directory_uri() . "/".$tax_term->slug;



						?>

                        <div class="item">
                            <a href="<?= $slug;?>">
                                <div class="card">
                                    <div class="card-header <?= $bglogo;?>">
                                        <img class="img-fluid m-auto img-logo" src="<?= $logo_cruzeiro;?>" alt="Card image cap">
                                    </div>
                                    <img class="img-fluid" src="<?= $imagem_cruzeiro;?>" alt="Card image cap">
                                    <div class="card-body">
                                        <h5 class="font-weight-bold"><?= $titulo;?></h5>
                                        <p class="font-14 mb-0">Cabines</p>
                                        <p class="text-laranja font-weight-bold mb-0">em até <span class="h4 font-weight-bold">10x</span></p>
                                    </div>
                                </div>
                            </a>
                        </div>

						<?php
					}
					/*}
				}
				wp_reset_query();
				wp_reset_postdata();*/
					?>

                    <!--<div class="item">
                        <a href="#">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-fluid w-50 m-auto" src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/logo-pullmantur.png" alt="Card image cap">
                                </div>
                                <img class="img-fluid" src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/01.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="font-weight-bold">Pullmantur Cruzeiros</h5>
                                    <p class="font-14 mb-0">Cabines</p>
                                    <p class="text-laranja font-weight-bold mb-0">em até <span class="h4 font-weight-bold">10x</span></p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="item">
                        <a href="#">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-fluid w-50 m-auto" src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/logo-pullmantur.png" alt="Card image cap">
                                </div>
                                <img class="img-fluid" src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/01.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="font-weight-bold">Pullmantur Cruzeiros</h5>
                                    <p class="font-14 mb-0">Cabines</p>
                                    <p class="text-laranja font-weight-bold mb-0">em até <span class="h4 font-weight-bold">10x</span></p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="item">
                        <a href="#">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-fluid w-50 m-auto" src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/logo-pullmantur.png" alt="Card image cap">
                                </div>
                                <img class="img-fluid" src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/01.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="font-weight-bold">Pullmantur Cruzeiros</h5>
                                    <p class="font-14 mb-0">Cabines</p>
                                    <p class="text-laranja font-weight-bold mb-0">em até <span class="h4 font-weight-bold">10x</span></p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="item">
                        <a href="#">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-fluid w-50 m-auto" src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/logo-pullmantur.png" alt="Card image cap">
                                </div>
                                <img class="img-fluid" src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/01.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="font-weight-bold">Pullmantur Cruzeiros</h5>
                                    <p class="font-14 mb-0">Cabines</p>
                                    <p class="text-laranja font-weight-bold mb-0">em até <span class="h4 font-weight-bold">10x</span></p>
                                </div>
                            </div>
                        </a>
                    </div>-->

                </div>
            </div>
        </section>


       <section id="destinos" class="py-5 bg-light">
            <div class="container">
                <div class="input-group">
                    <h3 class="text-uppercase text-azul align-self-center mb-0 mr-3">DESTINOS</h3>
                    <a href="<?php echo get_permalink( get_page_by_path( 'destino' ) ); ?>" class="btn btn-lg bg-azul text-white px-4">
                        VER TODOS
                    </a>
                </div>

                <div class="row pt-5 align-items-stretch">

					<?php
					$args = array("post_type"=>"destinos","order"=>"ASC");
					$the_query = new WP_Query($args);

					if ($the_query->have_posts()){
						while ($the_query->have_posts()){
							$the_query->the_post();

							?>


                            <div class="col-sm-4 col-md-6 col-lg-3 mb-4 d-flex align-items-stretch">
                                <div class="card">
                                    <img class="img-fluid card-img-top" src="<?php the_field('imagem');?>" alt="Card image cap">
                                    <div class="card-body">
                                        <p class="font-14 mb-0 mt-1"><?php the_field('saida');?></p>
                                        <h5 class="font-weight-bold text-laranja mb-0"><?php
                                            $titulo = get_field('titulo');
                                            if(strlen($titulo) > 24){
                                                $titulo = substr($titulo,0,21);
                                                $titulo .= "...";
                                            }

                                            echo($titulo);
                                            
                                            ?>
                                        </h5>
                                        <p class="font-weight-bold mb-0"><?php the_field('dias');?></p>
                                        <div class="border my-2"></div>
                                        <p class="font-14 mb-0"><?php the_field('entrada');?></p>
										<h4 class="text-azul font-weight-bold mb-0"><?php the_field('valor_parcelado');?></h4>
                                        <p class="font-14 mb-0 mt-1"><?php the_field('valor_total');?></p>
                                    </div>
                                    <div class="card-footer">
                                        <div class="row">
                                            <div class="col-4 col-sm-4 col-lg-4 p-0 align-self-center">
                                                <img src="<?php the_field('logo');?>" class="img-fluid img-logo">
                                            </div>
                                            <div class="col-8 col-sm-8  col-lg-8">
                                                <a href="<?php the_permalink();?>"
                                                   class="btn btn-lg bg-laranja text-white px-4">
                                                    DETALHES
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

							<?php
						}
					}
					wp_reset_query();
					wp_reset_postdata();
					?>

                    <!--<div class="col-sm-4 col-lg-3 mb-4">
                        <div class="card">
                            <img class="img-fluid card-img-top" src="<?php /*echo get_template_directory_uri(); */?>/img/destinos/01.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                                <p class="font-weight-bold mb-0">8 dias</p>
                                <div class="border"></div>
                                <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                                <div class="row">
                                    <div class="col-8 col-sm-8 col-lg-8">
                                        <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                    </div>
                                    <div class="col-4 col-sm-4 col-lg-4">
                                        <img src="<?php /*echo get_template_directory_uri(); */?>/img/destinos/logo-abreu.jpg" class="img-fluid">
                                    </div>
                                </div>
                                <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                    VER DETALHES
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-3 mb-4">
                        <div class="card">
                            <img class="img-fluid card-img-top" src="<?php /*echo get_template_directory_uri(); */?>/img/destinos/01.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                                <p class="font-weight-bold mb-0">8 dias</p>
                                <div class="border"></div>
                                <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                                <div class="row">
                                    <div class="col-8 col-sm-8 col-lg-8">
                                        <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                    </div>
                                    <div class="col-4 col-sm-4 col-lg-4">
                                        <img src="<?php /*echo get_template_directory_uri(); */?>/img/destinos/logo-abreu.jpg" class="img-fluid">
                                    </div>
                                </div>
                                <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                    VER DETALHES
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-3 mb-4">
                        <div class="card">
                            <img class="img-fluid card-img-top" src="<?php /*echo get_template_directory_uri(); */?>/img/destinos/01.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                                <p class="font-weight-bold mb-0">8 dias</p>
                                <div class="border"></div>
                                <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                                <div class="row">
                                    <div class="col-8 col-sm-8 col-lg-8">
                                        <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                    </div>
                                    <div class="col-4 col-sm-4 col-lg-4">
                                        <img src="<?php /*echo get_template_directory_uri(); */?>/img/destinos/logo-abreu.jpg" class="img-fluid">
                                    </div>
                                </div>
                                <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                    VER DETALHES
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-3 mb-4">
                        <div class="card">
                            <img class="img-fluid card-img-top" src="<?php /*echo get_template_directory_uri(); */?>/img/destinos/01.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                                <p class="font-weight-bold mb-0">8 dias</p>
                                <div class="border"></div>
                                <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                                <div class="row">
                                    <div class="col-8 col-sm-8 col-lg-8">
                                        <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                    </div>
                                    <div class="col-4 col-sm-4 col-lg-4">
                                        <img src="<?php /*echo get_template_directory_uri(); */?>/img/destinos/logo-abreu.jpg" class="img-fluid">
                                    </div>
                                </div>
                                <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                    VER DETALHES
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-3 mb-4">
                        <div class="card">
                            <img class="img-fluid card-img-top" src="<?php /*echo get_template_directory_uri(); */?>/img/destinos/01.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                                <p class="font-weight-bold mb-0">8 dias</p>
                                <div class="border"></div>
                                <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                                <div class="row">
                                    <div class="col-8 col-sm-8 col-lg-8">
                                        <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                    </div>
                                    <div class="col-4 col-sm-4 col-lg-4">
                                        <img src="<?php /*echo get_template_directory_uri(); */?>/img/destinos/logo-abreu.jpg" class="img-fluid">
                                    </div>
                                </div>
                                <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                    VER DETALHES
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-3 mb-4">
                        <div class="card">
                            <img class="img-fluid card-img-top" src="<?php /*echo get_template_directory_uri(); */?>/img/destinos/01.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                                <p class="font-weight-bold mb-0">8 dias</p>
                                <div class="border"></div>
                                <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                                <div class="row">
                                    <div class="col-8 col-sm-8 col-lg-8">
                                        <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                    </div>
                                    <div class="col-4 col-sm-4 col-lg-4">
                                        <img src="<?php /*echo get_template_directory_uri(); */?>/img/destinos/logo-abreu.jpg" class="img-fluid">
                                    </div>
                                </div>
                                <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                    VER DETALHES
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-3 mb-4">
                        <div class="card">
                            <img class="img-fluid card-img-top" src="<?php /*echo get_template_directory_uri(); */?>/img/destinos/01.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                                <p class="font-weight-bold mb-0">8 dias</p>
                                <div class="border"></div>
                                <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                                <div class="row">
                                    <div class="col-8 col-sm-8 col-lg-8">
                                        <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                    </div>
                                    <div class="col-4 col-sm-4 col-lg-4">
                                        <img src="<?php /*echo get_template_directory_uri(); */?>/img/destinos/logo-abreu.jpg" class="img-fluid">
                                    </div>
                                </div>
                                <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                    VER DETALHES
                                </a>
                            </div>
                        </div>
                    </div>-->


                 </div>
            </div>
        </section>


        <section id="home-lua-de-mel" class="py-5">
            <div class="container">
                <img src="<?php echo get_template_directory_uri(); ?>/img/icons/luxury.svg" class="img-fluid m-auto d-block icon">

                <div class="text-white text-center mt-3">
                    <h3 class="text-uppercase font-weight-bold">Lua de mel</h3>

                    <h5 class="my-4"><strong>Ganhe</strong> sua viagem de <strong>Lua de Mel</strong> de presente.<br> Para isso, basta fazer uma Lista de <strong>Casamento na Mendes Tur</strong>.<br> Os convidados podem comprar <strong>parte da viagem</strong> ou até mesmo, oferecer <strong>a viagem inteira</strong> de presente.</h5>

                    <a href="<?php echo get_permalink( get_page_by_path( 'lua-de-mel' ) ); ?>" class="btn btn-lg bg-laranja text-white text-uppercase px-5">
                        saiba mais
                    </a>
                </div>
            </div>
        </section>

        <section id="home-cambio" class="py-5">
        	<div class="container">
				<h3 class="text-uppercase text-azul text-center font-weight-bold">câmbio ideal para você</h3>
				<h5 class="text-uppercase text-laranja text-center mt-3">Sua viagem tranquila e segura,<br> Sem abrir mão da economia.</h5>
				<div class="row mt-5">
					<div class="col-12 col-sm-12 col-md-4 col-lg-4 pr-0">
						<div class="list-group" id="list-tab" role="tablist">
                            <a class="list-group-item list-group-item-action active" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">MOEDA EM ESPÉCIE</a>
							<a class="list-group-item list-group-item-action" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">MONEYGRAM</a>
							<a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">CARTÃO PRÉ-PAGO INTERNACIONAL</a>
							<a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">CHEQUES INTERNACIONAIS </a>			
							<a class="list-group-item list-group-item-action" id="list-settings-mais" data-toggle="list" href="#list-mais" role="tab" aria-controls="settings">E MUITO MAIS</a>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-8 col-lg-8 pl-0">
						<div class="tab-content" id="nav-tabContent">
							<div class="tab-pane fade" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cambio/moneygram.jpg" class="img-fluid pb-3">
								<p>Com o serviço de envio de dinheiro MoneyGram você envia e recebe valores do exterior em apenas 10 minutos, ou seja, seu dinheiro chega a mais de 190 países com agilidade e total segurança e você nem precisa ter conta bancária, basta contar com a sua corretora de câmbio!</p>
							</div>
							<div class="tab-pane fade show active" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cambio/cartao.jpg" class="img-fluid pb-3">
								<p>Mais conveniência para sua viagem.</p>
								<p>Com o cartão pré-pago internacional Cash Passport Mastercard Platinum você conta com segurança e praticidade na hora de usar seu dinheiro lá fora.</p>
							</div>
							<div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cambio/cheques.jpg" class="img-fluid pb-3">
								<p>Cheques internacionais e Travellers Checks</p>
								<p>Precisa trocar cheques de dividendos, pensões, aposentadoria, restituição de impostos, doações ou comissões? Na Confidence, você troca seus cheques internacionais de moeda estrangeira e travellers checks de uma forma muito mais simples e segura.</p>
							</div>
							<div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list"><img src="<?php echo get_template_directory_uri(); ?>/img/cambio/moeda.jpg" class="img-fluid pb-3">
								<p>Na Confidence você encontra a maior variedade para compra de moedas estrangeiras disponíveis no mercado. Desde as mais comuns até as mais exóticas, são mais de 20 tipos. E você ainda pode receber a moeda em sua casa ou trabalho por meio do nosso serviço de Delivery.</p>
							</div>
							<div class="tab-pane fade" id="list-mais" role="tabpanel" aria-labelledby="list-settings-mais">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cambio/muito-mais.jpg" class="img-fluid pb-3">
                              <p>Pessoa física</p>
                              <ul style="list-style: inside disc;">
                                 <li>Delivery de moedas</li>
                                 <li>Remessas e pagamentos internacionais</li>
                                 <li>Registro RDE-ROF</li>
                                 <li>Registro Siscoserv</li>
                             </ul>
							</div>
						</div>
					</div>
				</div>
			</div>
        </section>

        <div class="cambio bg-white p-3">
			<img src="<?php echo get_template_directory_uri(); ?>/img/cambio/logo-confidence.png" class="img-fluid m-auto d-block w-50">
			<div class="text-center">
				<a href="https://www.confidencecambio.com.br/conversor-moedas/" class="btn btn-lg bg-laranja text-white text-uppercase px-5 mt-2">
					CONFIRA
				</a>
			</div>
		</div>

		<div class="cambio bg-cinza-nv py-5 mt-2"></div>

        <section id="home-quem-somos" class="py-5">
            <div class="container">
                <h3 class="text-center text-white font-weight-bold text-uppercase">Quem somos</h3>

                <div class="text-white text-center my-4">
                    <h5>A agência <strong>Mendes Tur Câmbio e Turismo</strong> foi fundada em <strong>1987</strong> e atua no setor de câmbio, turismo de lazer e corporativo.</h5>

                    <button type="button" class="video-btn input-group justify-content-center my-4" data-toggle="modal" data-src="https://www.youtube.com/embed/NG3ifyCxMy0" data-target="#myModal">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons/player.svg" class="img-fluid mr-3 icon-player">
                        <h5 class="align-self-center font-weight-bold text-white">ASSISTA AO VÍDEO</h5>
                    </button>
                </div>
                <div class="text-center">
                    <a href="<?php echo get_permalink( get_page_by_path( 'quem-somos' ) ); ?>" class="btn btn-lg bg-azul text-white text-uppercase px-5">
                        SAIBA MAIS
                    </a>
                </div>
            </div>
        </section>

         <!--MODAL-VIDO-->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <!-- 16:9 aspect ratio -->
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always">></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div> 

        <section id="home-viagem" class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-lg-4 align-self-stretch">
                    	<a href="http://www.aguiarvistos.com.br/" target="_blank">
	                        <div class="visto bg-verde">
								<h4 class="text-center text-uppercase text-white">Já tirou seu visto?</h4>
								<img src="<?php echo get_template_directory_uri(); ?>/img/logo-aguiar.png" class="img-fluid m-auto d-block">
								<img src="<?php echo get_template_directory_uri(); ?>/img/documentos.png" class="img-fluid img-documentos">
							</div>
						</a>
                    </div>
                    <div class="col-sm-4 col-lg-4 align-self-stretch">
                        <div class="viagem border px-3 py-5">
                            <div class="mb-auto pb-2">
                                <img src="<?php echo get_template_directory_uri(); ?>/img/logo-gta.png" class="img-fluid w-25 m-auto d-block">
                            </div>
                            <h5 class="text-center">Faça uma viagem tranquila com o nosso seguro viagem!</h5>
                            <div class="text-center">
                                <a href="<?php echo get_permalink( get_page_by_path( 'seguro-viagem' ) ); ?>" class="btn btn-lg bg-laranja text-white text-uppercase px-5 mt-4">
                                    SAIBA MAIS
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-4 align-self-stretch">
                        <div class="contato bg-passeios py-5 px-4">
                            <h4 class="text-center text-uppercase text-laranja">Passeios e experiências</h4>
                            <h5 class="text-center text-white mb-4">Confira aqui os melhores passeios e experiências para a sua viagem.</h5>
                            <div class="text-center">
                                <a href="<?php echo get_permalink( get_page_by_path( 'passeios' ) ); ?>" class="btn btn-lg bg-laranja text-white text-uppercase px-5 mt-4">
                                    CONFIRA
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </section>


		<!-- <?php
		require_once (TEMPLATEPATH."/includes/instagram.php");
		?> -->

    </main>

<?php get_footer('newtmpl'); ?>