<?php
// Template Name: Destino
get_header('newtmpl');
?>
<!--  Destino -->
	<main>

		<!--PESQUISA-->
        <?php
            require_once (TEMPLATEPATH."/includes/busca.php");
        ?>

		<section id="destino-header" class="py-5">
			<div class="container">
				<h1 class="text-center text-white text-uppercase bottom-line-header line-center">Destino</h1>
			</div>
		</section>

		<section id="page-destinos" class="bg-cinza-claro py-5">
			<div class="container">
				<div class="row align-items-stretch" id="pacotes">

                    <?php
                    $args = array(
                            "post_type"=>"destinos",
                        'order'=>"ASC"
                    );
                    $the_query = new WP_Query($args);

                    if ($the_query->have_posts()){
                        while ($the_query->have_posts()){
                            $the_query->the_post();
                    ?>


					<div class="col-sm-4 col-md-6 col-lg-3 mb-4 d-flex align-items-stretch">
						<div class="card">
    							<img class="img-fluid card-img-top" src="<?php the_field('imagem');?>" alt="Card image cap">
							<div class="card-body">
                                <p class="font-14 mb-0 mt-1"><?php the_field('saida');?></p>
								<h5 class="font-weight-bold text-laranja mb-0"><?php
									$titulo = get_field('titulo');
									if(strlen($titulo) > 24){
										$titulo = substr($titulo,0,21);
										$titulo .= "...";
									}

									echo($titulo);
                                    ?>
                                </h5>
								<p class="font-weight-bold mb-0"><?php the_field('dias');?></p>
                                <div class="border my-2"></div>
								<p class="font-14 mb-0"><?php the_field('entrada');?></p>
                                <h4 class="text-azul font-weight-bold mb-0"><?php the_field('valor_parcelado');?></h4>
                                <p class="font-14 mb-0 mt-1">Total: <?php the_field('valor_total');?></p>
							</div>
                           <div class="card-footer">
                                <div class="row">
                                    <div class="col-4 col-sm-4 col-md-4 col-lg-4 p-0 align-self-center">
                                        <img src="<?php the_field('logo') ;?>" class="img-fluid">
                                    </div>
                                    <div class="col-8 col-sm-8 col-md-8 col-lg-8"> 
                                        
                                        <a href="<?php the_permalink();?>" class="btn btn-lg bg-laranja text-white px-4">
                                            DETALHES
                                        </a>
                                    </div>
                                </div>
                            </div>
						</div>
					</div>

                    <?php
                        }
                    }
                    wp_reset_query();
                    wp_reset_postdata();
                    ?>

				</div>
			</div>
		</section>

     <!--    <?php
            require_once (TEMPLATEPATH."/includes/instagram.php");
        ?> -->
	</main>

<?php get_footer('newtmpl'); ?>

<script>
    var url = "/wp-content/themes/mendestur-front/webservices/busca_destino.php";


    $.ajax({
        type:"POST",
        url: url,
        beforeSend: function () {
            $('.loader').show();
        },
        success: function (data) {

            $(data).each(function () {

                //alert(this.titulo);
                var val = this.valor;
                if (val === "0"){
                    val = "Consulte-nos";
                    this.moeda = "";
                } else {
                    val = parseFloat(this.valor) / 2;
                    val = val.toFixed(2);
                    val = val.replace(".",",");
                    /*val += "00";*/
                }

                var titulo = this.titulo;
                titulo = titulo.toLowerCase();

                if (titulo.length > 24){
                    titulo = titulo.substring(0,21);
                    titulo += "...";
                }
                var pacote = '<div class="col-sm-4 col-md-6 col-lg-3 mb-4 d-flex align-items-stretch">' +
                    '<div class="card">' +
                    '<img class="img-fluid card-img-top" src="'+this.imagem+'" onerror="this.onerror=null; this.src=\'http://dev.pxpdigital.com.br/mendestur-front/wp-content/themes/mendestur-front/img/destinos/default_destinos.jpg\'">'+
                    '<div class="card-body">'+
                    '<p class="font-14 mb-0 mt-1">'+this.saida+'</p>'+
                    '<h5 class="font-weight-bold text-laranja mb-0 capitalize">'+titulo+'</h5>'+
                    '<p class="font-weight-bold mb-0">'+this.dias+' dias</p>'+
                    '<div class="border my-2"></div>'+
                    '<p class="text-azul font-weight-bold mb-0">'+this.moeda+' <span class="h4 font-weight-bold">'+val+'</span></p>'+
                    '</div>'+
                    '<div class="card-footer">'+
                    '<div class="row">'+
                    '<div class="col-4 col-sm-4 col-md-4 col-lg-4 p-0">'+
                    '<img src="<?= get_template_directory_uri();?>/img/destinos/ancoradouro.png" class="img-fluid">'+
                    '</div>'+
                    '<div class="col-8 col-sm-8 col-md-8 col-lg-8">'+
                    '<button class="btn btn-lg bg-laranja text-white px-4 btn-detalhes" data-pacote="'+this.id+'" data-regiao="'+this.regiao+'"> DETALHES</button>'+
                '</div>'+
                '</div>'+
                /*'<a href="page-interna-destino.php" class="btn btn-lg bg-laranja text-white px-4">'+
				'VER DETALHES'+
				'</a>'+*/
                '</div>'+
                '</div>'+
                '</div>';

                $("#pacotes").append(pacote);

            });
            $(".capitalize").addClass("capitalize");
        },
        complete: function () {
            $('.loader').hide();

            $(".btn-detalhes").each(function () {

                $(this).click(function () {
                    //alert("fdsfsd");
                    var regiao = $(this).data("regiao");
                    var pacote = $(this).data("pacote");

                    var url = "<?=get_template_directory_uri();?>/webservices/guarda_ids.php";

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            id_pacote: pacote,
                            id_regiao: regiao
                        },
                        success: function (data) {
                            //alert("no succes");
                            if (data.erro === "false"){
                               // alert("no if");
                                location.href="./interna-destino?id="+data.id+"&regiao="+data.regiao;
                            } else {
                                //alert("no else");
                            }

                        }
                    });

                    /*alert(regiao);
                    alert(pacote);*/
                });
            });
        }
    });
</script>