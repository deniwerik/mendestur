<?php
get_header('newtmpl');
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php the_title(); ?>
	<?php the_content(); ?>

<?php endwhile; else: ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>

    <!--<main>
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="img/carousel-01.jpg" alt="First slide">
                    <div class="hero">
                        <div class="row">
                            <div class="col-sm-12 col-lg-2 align-self-stretch d-flex align-items-center bg-white p-4">
                                <img src="img/logo-abreu.jpg" class="img-fluid img-logo">
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-stretch p-3 border-right">
                                <h4 class="mb-0 text-uppercase">Tesouros da turquia</h4>
                                <p class="mb-0">Saída até março de 2019</p>
                                <h4 class="text-laranja mb-0 font-weight-bold">10 dias</h4>
                            </div>
                            <div class="col-sm-4 col-lg-3 align-self-stretch p-3">
                                <p class="mb-0 font-14">A partir de</p>
                                <p class="mb-0 text-laranja font-44 font-weight-bold">R$ 167,60</p>
                                <p class="mb-0 font-14 text-laranja font-weight-bold">Total R$ 1.676,00</p>
                            </div>
                            <div class="col-sm-4 col-lg-2 align-self-stretch d-flex align-items-center">
                                <a href="#" class="btn btn-lg btn-border text-white px-5">
                                    CONFIRA 
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="carousel-item">
                    <img class="d-block w-100" src="img/carousel-01.jpg" alt="First slide">
                    <div class="hero">
                        <div class="row">
                            <div class="col-sm-12 col-lg-2 align-self-stretch d-flex align-items-center bg-white p-4">
                                <img src="img/logo-abreu.jpg" class="img-fluid img-logo">
                            </div>
                            <div class="col-sm-4 col-lg-4 align-self-stretch p-3 border-right">
                                <h4 class="mb-0 text-uppercase">Tesouros da turquia</h4>
                                <p class="mb-0">Saída até março de 2019</p>
                                <h4 class="text-laranja mb-0 font-weight-bold">10 dias</h4>
                            </div>
                            <div class="col-sm-4 col-lg-3 align-self-stretch p-3">
                                <p class="mb-0 font-14">A partir de</p>
                                <p class="mb-0 text-laranja font-44 font-weight-bold">R$ 167,60</p>
                                <p class="mb-0 font-14 text-laranja font-weight-bold">Total R$ 1.676,00</p>
                            </div>
                            <div class="col-sm-4 col-lg-2 align-self-stretch d-flex align-items-center">
                                <a href="#" class="btn btn-lg btn-border text-white px-5">
                                    CONFIRA 
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <img src="<?/*= get_template_directory_uri();*/?>/img/icons/arrows-01.svg" class="prev-icon">
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <img src="<?/*= get_template_directory_uri();*/?>/img/icons/arrows-02.svg" class="prev-icon">
                <span class="sr-only">Next</span>
            </a>
        </div>


        <?php
/*        require_once (TEMPLATEPATH."/includes/busca.php");
        */?>

        <section id="banners" class="py-5">
            <div class="container">
                <div class="bg-01">
                    <img src="img/01_Banner_home_fixo.jpg" class="img-fluid d-block d-sm-none">
                    <div class="bg-skew-azul">
                        <div class="px-4 no-skew">
                            <h4 class="text-white text-uppercase bottom-line">Cruzeiros Exclusivos</h4>
                            <h5 class="text-white">A primeira na Baixada Santista a oferecer os melhores cruzeiros para você e sua família!</h5>
                            <a href="page-cruzeiro.php" class="btn btn-lg btn-border text-laranja px-5 mt-2">
                                CONFIRA 
                            </a>
                        </div>
                    </div>
                </div>

                <div class="row pt-3">
                    <div class="col-sm-6 col-lg-6">
                        <img src="img/bg-02.jpg" class="img-fluid">
                        <div class="hero text-center">
                            <h5 class="text-uppercase font-weight-light">Conheça a neve</h5>
                            <h4 class="text-uppercase font-weight-bold">Planeje suas férias de Inverno</h4>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6">
                        <img src="img/bg-03.jpg" class="img-fluid">
                        <div class="hero text-center">
                            <h5 class="text-uppercase font-weight-light">As maravilhas do nordeste</h5>
                            <h4 class="text-uppercase font-weight-bold">Viagem de 8 dias para Maceió</h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="cruzeiros" class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="input-group">
                            <h3 class="text-uppercase text-white align-self-center mb-0 mr-3">CRUZEIROS</h3>
                            <a href="page-cruzeiro.php" class="btn btn-lg btn-border text-white px-4">
                                VER TODOS
                            </a>
                        </div>
                    </div>
                </div>

                <div id="slider-carousel" class="owl-carousel owl-theme pt-5">
                    <div class="item">
                        <a href="#">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-fluid w-50 m-auto" src="img/cruzeiros/logo-pullmantur.png" alt="Card image cap">
                                </div>
                                <img class="img-fluid" src="img/cruzeiros/01.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="font-weight-bold">Pullmantur Cruzeiros</h5>
                                    <p class="font-14 mb-0">Cabines</p>
                                    <p class="text-laranja font-weight-bold mb-0">em até <span class="h4 font-weight-bold">10x</span></p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="item">
                        <a href="#">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-fluid w-50 m-auto" src="img/cruzeiros/logo-pullmantur.png" alt="Card image cap">
                                </div>
                                <img class="img-fluid" src="img/cruzeiros/01.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="font-weight-bold">Pullmantur Cruzeiros</h5>
                                    <p class="font-14 mb-0">Cabines</p>
                                    <p class="text-laranja font-weight-bold mb-0">em até <span class="h4 font-weight-bold">10x</span></p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="item">
                        <a href="#">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-fluid w-50 m-auto" src="img/cruzeiros/logo-pullmantur.png" alt="Card image cap">
                                </div>
                                <img class="img-fluid" src="img/cruzeiros/01.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="font-weight-bold">Pullmantur Cruzeiros</h5>
                                    <p class="font-14 mb-0">Cabines</p>
                                    <p class="text-laranja font-weight-bold mb-0">em até <span class="h4 font-weight-bold">10x</span></p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="item">
                        <a href="#">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-fluid w-50 m-auto" src="img/cruzeiros/logo-pullmantur.png" alt="Card image cap">
                                </div>
                                <img class="img-fluid" src="img/cruzeiros/01.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="font-weight-bold">Pullmantur Cruzeiros</h5>
                                    <p class="font-14 mb-0">Cabines</p>
                                    <p class="text-laranja font-weight-bold mb-0">em até <span class="h4 font-weight-bold">10x</span></p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="item">
                        <a href="#">
                            <div class="card">
                                <div class="card-header">
                                    <img class="img-fluid w-50 m-auto" src="img/cruzeiros/logo-pullmantur.png" alt="Card image cap">
                                </div>
                                <img class="img-fluid" src="img/cruzeiros/01.jpg" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="font-weight-bold">Pullmantur Cruzeiros</h5>
                                    <p class="font-14 mb-0">Cabines</p>
                                    <p class="text-laranja font-weight-bold mb-0">em até <span class="h4 font-weight-bold">10x</span></p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>


        <section id="destinos" class="py-5 bg-light">
            <div class="container">
                <div class="input-group">
                    <h3 class="text-uppercase text-azul align-self-center mb-0 mr-3">DESTINOS</h3>
                    <a href="destino.php" class="btn btn-lg bg-azul text-white px-4">
                        VER TODOS
                    </a>
                </div>

                <div class="row pt-5">
                    <div class="col-sm-4 col-lg-3 mb-4">
                        <div class="card">
                            <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                                <p class="font-weight-bold mb-0">8 dias</p>
                                <div class="border"></div>
                                <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                                <div class="row">
                                    <div class="col-8 col-sm-8 col-lg-8">
                                        <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                    </div>
                                    <div class="col-4 col-sm-4 col-lg-4">
                                        <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                    </div>
                                </div>
                                <a href="page-interna-destino.php" class="btn btn-lg bg-laranja text-white px-4">
                                    VER DETALHES
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4 col-lg-3 mb-4">
                        <div class="card">
                            <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                                <p class="font-weight-bold mb-0">8 dias</p>
                                <div class="border"></div>
                                <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                                <div class="row">
                                    <div class="col-8 col-sm-8 col-lg-8">
                                        <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                    </div>
                                    <div class="col-4 col-sm-4 col-lg-4">
                                        <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                    </div>
                                </div>
                                <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                    VER DETALHES
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-3 mb-4">
                        <div class="card">
                            <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                                <p class="font-weight-bold mb-0">8 dias</p>
                                <div class="border"></div>
                                <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                                <div class="row">
                                    <div class="col-8 col-sm-8 col-lg-8">
                                        <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                    </div>
                                    <div class="col-4 col-sm-4 col-lg-4">
                                        <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                    </div>
                                </div>
                                <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                    VER DETALHES
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-3 mb-4">
                        <div class="card">
                            <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                                <p class="font-weight-bold mb-0">8 dias</p>
                                <div class="border"></div>
                                <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                                <div class="row">
                                    <div class="col-8 col-sm-8 col-lg-8">
                                        <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                    </div>
                                    <div class="col-4 col-sm-4 col-lg-4">
                                        <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                    </div>
                                </div>
                                <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                    VER DETALHES
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-3 mb-4">
                        <div class="card">
                            <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                                <p class="font-weight-bold mb-0">8 dias</p>
                                <div class="border"></div>
                                <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                                <div class="row">
                                    <div class="col-8 col-sm-8 col-lg-8">
                                        <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                    </div>
                                    <div class="col-4 col-sm-4 col-lg-4">
                                        <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                    </div>
                                </div>
                                <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                    VER DETALHES
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-3 mb-4">
                        <div class="card">
                            <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                                <p class="font-weight-bold mb-0">8 dias</p>
                                <div class="border"></div>
                                <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                                <div class="row">
                                    <div class="col-8 col-sm-8 col-lg-8">
                                        <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                    </div>
                                    <div class="col-4 col-sm-4 col-lg-4">
                                        <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                    </div>
                                </div>
                                <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                    VER DETALHES
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-3 mb-4">
                        <div class="card">
                            <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                                <p class="font-weight-bold mb-0">8 dias</p>
                                <div class="border"></div>
                                <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                                <div class="row">
                                    <div class="col-8 col-sm-8 col-lg-8">
                                        <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                    </div>
                                    <div class="col-4 col-sm-4 col-lg-4">
                                        <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                    </div>
                                </div>
                                <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                    VER DETALHES
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-3 mb-4">
                        <div class="card">
                            <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                                <p class="font-weight-bold mb-0">8 dias</p>
                                <div class="border"></div>
                                <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                                <div class="row">
                                    <div class="col-8 col-sm-8 col-lg-8">
                                        <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                    </div>
                                    <div class="col-4 col-sm-4 col-lg-4">
                                        <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                    </div>
                                </div>
                                <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                    VER DETALHES
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="home-lua-de-mel" class="py-5">
            <div class="container">
                <img src="img/icons/luxury.svg" class="img-fluid m-auto d-block icon">

                <div class="text-white text-center mt-3">
                    <h3 class="text-uppercase font-weight-bold">Lua de mel</h3>

                    <h5 class="my-4"><strong>Ganhe</strong> sua viagem de <strong>Lua de Mel</strong> de presente.<br> Para isso, basta fazer uma Lista de <strong>Casamento na Mendes Tur</strong>.<br> Os convidados podem comprar <strong>parte da viagem</strong> ou até mesmo, oferecer <strong>a viagem inteira</strong> de presente.</h5>

                    <a href="page-lua-de-mel.php" class="btn btn-lg bg-laranja text-white text-uppercase px-5">
                        saiba mais
                    </a>
                </div>
            </div>
        </section>


        <section id="home-quem-somos" class="py-5">
            <div class="container">
                <h3 class="text-center text-white font-weight-bold text-uppercase">Quem somos</h3>

                <div class="text-white text-center my-4">
                    <h5>A Agencia <strong>Mendes Tur Câmbio e Turism</strong> o foi fundada em <strong>1987</strong> e é referência em seu setor no âmbito de lazer e corporativo.</h5>

                    <div class="input-group justify-content-center my-4">
                        <a href="#">
                            <img src="img/icons/player.svg" class="img-fluid mr-3 icon-player">
                            <h5 class="align-self-center font-weight-bold">ASSISTA AO VÍDEO</h5>
                        </a>
                    </div>
                </div>
                <div class="text-center">
                    <a href="page-quem-somos.php" class="btn btn-lg bg-azul text-white text-uppercase px-5">
                        SAIBA MAIS
                    </a>
                </div>
            </div>
        </section>

        <section id="home-viagem" class="pt-5">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-lg-4 align-self-stretch">
                        <div class="visto bg-cinza">
                            <h4 class="text-center text-uppercase text-laranja">Já tirou seu visto?</h4>
                            <div class="text-center">
                                <a href="http://www.aguiarvistos.com.br/" class="btn btn-lg bg-laranja text-white text-uppercase px-5" target="_blank">
                                    SAIBA MAIS
                                </a>
                            </div>
                            <img src="img/documentos.png" class="img-fluid img-documentos">
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-4 align-self-stretch">
                        <div class="viagem border px-3 py-5">
                            <div class="mb-auto pb-2">
                                <img src="img/logo-gta.png" class="img-fluid w-25 m-auto d-block">
                            </div>
                            <h5 class="text-center">Faça uma viagem tranquila com o nosso seguro viagem!</h5>
                            <div class="text-center">
                                <a href="page-seguro-viagem.php" class="btn btn-lg bg-laranja text-white text-uppercase px-5 mt-4">
                                    SAIBA MAIS
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-lg-4 align-self-stretch">
                        <div class="contato bg-azul py-5 px-4">
                            <h4 class="text-center text-uppercase text-laranja">Pensando em viajar</h4>
                            <h5 class="text-center text-white">Entre em contato, tire suas dúvidas ou deixe suas sugestões. Nossa equipe está pronta para lhe atender.</h5>
                            <div class="text-center">
                                <a href="page-contato.php" class="btn btn-lg bg-laranja text-white text-uppercase px-5 mt-4">
                                    CONTATO
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <?php
/*        require_once (TEMPLATEPATH."/includes/instagram.php");
        */?>

    </main>-->

<?php
get_footer('newtmpl');
?>