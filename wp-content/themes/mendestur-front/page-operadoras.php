<?php
// Template Name: Operadoras
get_header('newtmpl');
?>
<!-- Operadora -->
    <main>
        <section id="cruzeiro-header" class="py-5">
            <div class="container">
                <h1 class="text-center text-white text-uppercase bottom-line-header line-center">Cruzeiros</h1>
            </div>
        </section>

        <section id="page-cruzeiros" class="py-5 bg-cinza-claro">
            <div class="container">

                 <div class="row mt-5">
                    <div class="col-lg-2 bg-costa align-self-stretch py-3">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/logo-costa.png" class="img-fluid img-logo">
                    </div>
                    <div class="col-lg-3 align-self-stretch p-0">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/costa.jpg" class="img-fluid img-cruzeiro">
                    </div>
                    <div class="col-lg-7 bg-white p-4">
                        <h4 class="text-azul">Costa Cruzeiros</h4>
                        <p>Da pensão completa às atividades de bordo, as instalações esportivas e os centros de bem-estar, para não falar no entretenimento noturno, a Costa Cruzeiros inclui tudo o que você precisa para as suas férias. E para tornar o seu tempo a bordo e em terra ainda...</p>
                        <a href="/2018/cruzeiro/costa" class="btn btn-lg bg-azul text-white text-uppercase px-5 mr-3">
                            SAIBA MAIS
                        </a>
                        <a href="/2018/cruzeiro/costa/#solicitar" class="btn btn-lg bg-laranja text-white text-uppercase px-5">
                            Solicitar reservas
                        </a>
                    </div>
                </div>



                                <div class="row mt-5">
                                    <div class="col-lg-2 bg-azul-marinho align-self-stretch py-3">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/logo-msc.png"
                                             class="img-fluid img-logo">
                                    </div>
                                    <div class="col-lg-3 align-self-stretch p-0">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc.jpg"
                                             class="img-fluid img-cruzeiro">
                                    </div>
                                    <div class="col-lg-7 bg-white p-4">
                                        <h4 class="text-azul">MSC Cruzeiros</h4>
                                        <p>Explorando os mares há mais de 300 anos. O que começou com apenas um navio, cresceu e transformou-se em uma frota magnífica. No decorrer dos séculos, a família Aponte manteve sua dedicação à navegação dos mares e, em 1970, fundou a Mediterranean...</p>
                                        <a href="/2018/cruzeiro/msc"
                                           class="btn btn-lg bg-azul text-white text-uppercase px-5 mr-3">
                                            SAIBA MAIS
                                        </a>
                                        <a href="/2018/cruzeiro/msc/#solicitar"
                                           class="btn btn-lg bg-laranja text-white text-uppercase px-5">
                                            Solicitar reservas
                                        </a>
                                    </div>
                                </div>


               

                <div class="row mt-5">
                    <div class="col-lg-2 bg-norwegian align-self-stretch py-3">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/logo-norwegian.png" class="img-fluid img-logo">
                    </div>
                    <div class="col-lg-3 align-self-stretch p-0">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/norwegian.jpg" class="img-fluid img-cruzeiro">
                    </div>
                    <div class="col-lg-7 bg-white p-4">
                        <h4 class="text-azul">Norwegian Cruise Line</h4>
                        <p>Com a Norwegian Cruise Line, você estará navegando na frota mais inovadora em alto mar. Os navios são os mais premiados do mundo foram feitos para quem gosta da liberdade e flexibilidade de escolha. Suba a bordo para as férias perfeitas.</p>
                        <a href="/2018/cruzeiro/ncl" class="btn btn-lg bg-azul text-white text-uppercase px-5 mr-3">
                            SAIBA MAIS
                        </a>
                        <a href="/2018/cruzeiro/ncl/#solicitar" class="btn btn-lg bg-laranja text-white text-uppercase px-5">
                            Solicitar reservas
                        </a>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-lg-2 bg-pullmantur align-self-stretch py-3">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/logo-pullmantur-02.png" class="img-fluid img-logo">
                    </div>
                    <div class="col-lg-3 align-self-stretch p-0">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/pullmantur.jpg" class="img-fluid img-cruzeiro">
                    </div>
                    <div class="col-lg-7 bg-white p-4">
                        <h4 class="text-azul">Pullmantur Cruzeiros</h4>
                        <p>Os Cuzeiros Pullmantur são Tudo Incluído, motivo pelo que desfrutará de todos os almoços, jantares e bebidas a bordo sem nenhum custo adicional. Poderá acessar a uma completa ementa e gastronomia gourmet, com autêntica essência espanhola...</p>
                        <a href="/2018/cruzeiro/pullmantur" class="btn btn-lg bg-azul text-white text-uppercase px-5 mr-3">
                            SAIBA MAIS
                        </a>
                        <a href="/2018/cruzeiro/pullmantur/#solicitar" class="btn btn-lg bg-laranja text-white text-uppercase px-5">
                            Solicitar reservas
                        </a>
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-lg-2 bg-uniword align-self-stretch py-3">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/logo-uniword.png" class="img-fluid img-logo">
                    </div>
                    <div class="col-lg-3 align-self-stretch p-0">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/uniword-01.jpg" class="img-fluid img-cruzeiro">
                    </div>
                    <div class="col-lg-7 bg-white p-4">
                        <h4 class="text-azul">Cruzeiro Fluvial Uniwolrd</h4>
                        <p>"A diferença está nos detalhes". Tem um momento em toda experiência Uniworld que isso fica claro. Talvez seja no momento em que você está disfrutando o luxo e conforto da sua suíte. Ou saboreando alguma delicíosa receitas dos pratos criado por nossos chefes....</p>
                        <a href="/2018/cruzeiro/uniworld" class="btn btn-lg bg-azul text-white text-uppercase px-5 mr-3">
                            SAIBA MAIS
                        </a>
                        <a href="/2018/cruzeiro/uniworld/#solicitar" class="btn btn-lg bg-laranja text-white text-uppercase px-5">
                            Solicitar reservas
                        </a>
                    </div>
                </div>
            </div>
        </section>

    </main>

<?php get_footer('newtmpl'); ?>