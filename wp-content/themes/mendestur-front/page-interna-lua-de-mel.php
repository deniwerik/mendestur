<?php

get_header('newtmpl');
?>

	<main>
		<section id="lua-de-mel-interna-header" class="py-5">
			<div class="container">
				<h1 class="text-center text-white font-dancing-script">José <i class="fas fa-heart text-laranja mx-3"></i> Maria</h1>
				<h2 class="text-white text-center font-weight-light">30.04.2018</h2>
			</div>
		</section>

		<section id="page-interna-lua-de-mel" class="py-5">
			<div class="container">
				<ul class="nav mb-4">
					<li class="nav-item">
						<a class="nav-link active" href="#cerimonia"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/calendar-wedding.svg" class="img-fluid icons mr-1"><span>Cerimonia</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#presente"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/gift.svg" class="img-fluid icons mr-1"> <span>Presente</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#recado"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/love.svg" class="img-fluid icons mr-1"> <span>Recado</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#viagem"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/placeholder.svg" class="img-fluid icons mr-1"> <span>Viagem</span></a>
					</li>
				</ul>

				<div class="row justify-content-center mt-5">
					<div class="col-lg-4">
						<h5 class="text-center">Noiva (o)</h5>
						<img src="<?php echo get_template_directory_uri(); ?>/img/lua-de-mel/noiva.jpg" class="img-fluid mb-4">
						<h4 class="text-laranja text-center font-weight-bold font-italic">Maria Mendes Tur</h4>
					</div>
					<div class="col-lg-4">
						<h5 class="text-center">Noivo (a)</h5>
						<img src="<?php echo get_template_directory_uri(); ?>/img/lua-de-mel/noivo.jpg" class="img-fluid mb-4">
						<h4 class="text-laranja text-center font-weight-bold font-italic">José Mendes Tur</h4>
					</div>
				</div>
			</div>
		</section>

		<section id="cerimonia" class="bg-cinza-claro py-5">
			<div class="container">
				<div class="bg-white p-5 text-center">
					<h3 class="text-azul"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/calendar-wedding.svg" class="img-fluid icons mr-2">Informações da cerimônia</h3>
					<h4 class="text-cinza-claro">Local</h4>
					<h5>Igreja Lorem Ipsum - Av. Lorem Ipsum, 0000<br>Santos - SP - 00000-000</h5>
					<h4 class="text-cinza-claro">Data</h4>
					<h5>30/04/2018</h5>
					<h4 class="text-cinza-claro">Horário</h4>
					<h5>21h00</h5>
				</div>
			</div>
		</section>

		<section id="presente" class="py-5 text-center">
			<div class="container">
				<h3 class="text-azul"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/gift.svg" class="img-fluid icons mr-2">Presente de lua de mel</h3>
				<p>A lua de mel! A viagem dos sonhos de um casal. Você, convidado e amigo dos noivos pode ajudá-los a vivenciarem um dos melhores momentos de suas vidas.</p>

				<div class="border p-5">
					<h5>Valor Mínimo da Cota</h5>
					<h4 class="text-laranja font-weight-bold">R$ 200,00</h4>
				</div>

				<a href="#" class="btn btn-lg bg-laranja text-white px-4 my-4">
					Dados bancários para depósito
				</a>

				<p class="font-12">Sam quas dus. Ur? Pudae sime premolu ptibusandit, estium nulpa nobit offic tet idestru mquunt qui autemquamus, sum ditiasi nciumque. Berum volore eum quam, explia volupti onsenihil ma ad etur mo beat.</p>

				<h3 class="text-azul" id="recado">Deixe seu recado aos noivos</h3>

				<div class="form-orcamento bg-cinza p-4 m-5">
					<h5 class="font-weight-bold">Fomulário de Lua de Mel</h5>
					<form>
						<input type="text" class="form-control mb-2" placeholder="Nome completo*">
						<input type="text" class="form-control mb-2" placeholder="Email*">
						<input type="text" class="form-control mb-2" placeholder="Telefone*">
						<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" placeholder="Informações complementares"></textarea>
						<button type="submit" class="btn bg-laranja text-white px-5 mt-3 mb-2">ENVIAR</button>
					</form>
				</div>

				<h3 class="text-azul" id="recado">Lua de mel</h3>

				<h5>Veneza - Itália</h5>
				

				<div class="row mt-5 galeria">
					<div class="col-lg-4 p-0">
						<img src="<?php echo get_template_directory_uri(); ?>/img/lua-de-mel/veneza1.jpg" class="img-fluid" id="test1">
					</div>
					<div class="col-lg-4 p-0">
						<img src="<?php echo get_template_directory_uri(); ?>/img/lua-de-mel/veneza2.jpg" class="img-fluid">
					</div>
					<div class="col-lg-4 p-0">
						<img src="<?php echo get_template_directory_uri(); ?>/img/lua-de-mel/veneza3.jpg" class="img-fluid">
					</div>
					<div class="col-lg-4 p-0">
						<img src="<?php echo get_template_directory_uri(); ?>/img/lua-de-mel/veneza3.jpg" class="img-fluid">
					</div>
					<div class="col-lg-4 p-0">
						<img src="<?php echo get_template_directory_uri(); ?>/img/lua-de-mel/veneza2.jpg" class="img-fluid">
					</div>
					<div class="col-lg-4 p-0">
						<img src="<?php echo get_template_directory_uri(); ?>/img/lua-de-mel/veneza1.jpg" class="img-fluid">
					</div>
					<div class="col-lg-4 p-0">
						<img src="<?php echo get_template_directory_uri(); ?>/img/lua-de-mel/veneza1.jpg" class="img-fluid">
					</div>
					<div class="col-lg-4 p-0">
						<img src="<?php echo get_template_directory_uri(); ?>/img/lua-de-mel/veneza2.jpg" class="img-fluid">
					</div>
					<div class="col-lg-4 p-0">
						<img src="<?php echo get_template_directory_uri(); ?>/img/lua-de-mel/veneza3.jpg" class="img-fluid">
					</div>
				</div>
			</div>
		</section>

		 <?php
        require_once (TEMPLATEPATH."/includes/instagram.php");
        ?>

    </main>

<?php get_footer('newtmpl'); ?>