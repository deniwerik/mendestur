<?php
// Template Name: Single Destinos

get_header('newtmpl');
?>
<!-- Single Destinos -->
<?php
if (have_posts()){
	while (have_posts()) {
		the_post();; ?>


		<main>

			<!--PESQUISA-->
			<?php
			require_once( TEMPLATEPATH . "/includes/busca.php" );
			?>

			<section id="page-interna-destinos" class="py-5">
				<div class="container">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb bg-white font-14">
							<li class="breadcrumb-item"><a href="<?php echo  get_site_url(); ?>">Home</a>
							</li>
							<li class="breadcrumb-item" aria-current="page"><a href="<?php echo get_permalink( get_page_by_path( 'destino' ) ); ?>">Destinos</a></li>
							<li class="breadcrumb-item active text-laranja"
							    aria-current="page"><?php the_field('titulo'); ?></li>
						</ol>
					</nav>

					<h1 class="text-laranja pb-3"><?php the_field('titulo'); ?></h1>

					<div class="row">
						<div class="col-lg-8">
							<img src="<?php the_field('imagem'); ?>" class="img-fluid d-block w-100">

							<ul class="nav nav-pills mb-3 mt-4 border-branco" id="pills-tab" role="tablist">
								<li class="nav-item">
									<a class="nav-link active text-uppercase" id="info-tab" data-toggle="pill"
									   href="#informacoes" role="tab" aria-controls="informacoes" aria-selected="true">Informações</a>
								</li>
								<li class="nav-item">
									<a class="nav-link text-uppercase" id="incluso-tab" data-toggle="pill"
									   href="#incluso" role="tab" aria-controls="incluso"
									   aria-selected="false">Incluso</a>
								</li>
								<li class="nav-item">
									<a class="nav-link text-uppercase" id="condicoes-tab" data-toggle="pill"
									   href="#condicoes" role="tab" aria-controls="condicoes" aria-selected="false">Condições
										Gerais</a>
								</li>
							</ul>
							<div class="tab-content mb-5" id="pills-tabContent">
								<!--Informações-->
								<div class="tab-pane fade show active" id="informacoes" role="tabpanel"
								     aria-labelledby="pills-home-tab">
									<?php
/*									if ( ! empty( $pacote['descricao'] ) ) {
										echo( "<p class='mb-0'>Descrição: <strong>" . the_field('') . "</strong></p>" );
									}
									*/?>
									<!--<p class="mb-0">Local: <strong><?php /*the_field('titulo'); */?></strong></p>-->
									<p class="mb-0"><strong><?php the_field('saida'); ?></strong></p>
									<p class="mb-0"><strong><?php the_field('dias'); ?></strong>
									</p>
                                        <?php the_field("visitando");?>
									<!--<p class="mb-0">Hospedagem: <strong></strong></p>-->


								</div>
								<div class="tab-pane fade" id="incluso" role="tabpanel"
								     aria-labelledby="pills-profile-tab">
									<?php
                                    the_field('incluso');
									 ?>
								</div>
								<div class="tab-pane fade font-12" id="condicoes" role="tabpanel"
								     aria-labelledby="pills-contact-tab">
									<?php the_field('condicoes_gerais'); ?>
								</div>
							</div>

							<div class="border-top"></div>
								<div class="row">
								<div class="col-lg-6">
									<img src="<?php the_field('logo')?>" class="img-fluid mt-3"> 
								</div>
								<div class="col-lg-6 align-self-center">
									<small>Disponibilidade e preço sujeito alteração sem prévio aviso.</small>
								</div>
							</div>
						</div>
						<div class="col-lg-4 bg-cinza p-5 form-interesse">
                            <p class="font-14 mb-0 ">
                                <?php the_field('entrada');?>
                            </p>
							<p class="text-laranja font-weight-bold mb-0"> <span class="h4 font-weight-bold">
                                <?php
                               the_field('valor_parcelado');
                                ?>
                            </span>
							</p>
							<p class="font-14 text-azul font-weight-bold mb-0">
								<?php the_field('valor_total'); ?></p>
                            <p class="font-12 border-bottom pb-2">
                                <?php the_field('descricao_valor');?>
                            </p>
							<h5 class="font-weight-bold">Ficou interessado?</h5>
							<p>Caso tenha gostado desse pacote de viagem, preencha o formulário abaixo, nossa equipe
								logo entrará em contato com você, para maiores informações.</p>
							<!-- <form>
								<input type="name" class="form-control mb-3" id="exampleInputName"
								       aria-describedby="emailHelp" placeholder="Nome">
								<input type="phone" class="form-control mb-3" id="exampleInputPhone"
								       aria-describedby="emailHelp" placeholder="Telefone">
								<input type="email" class="form-control mb-3" id="exampleInputEmai1"
								       aria-describedby="emailHelp" placeholder="Email">
								<button type="submit" class="btn bg-laranja mb-2 text-white px-4">Enviar</button>
							</form> -->
							<?php echo do_shortcode( '[contact-form-7 id="218" title="Destinos_Interna" your-destino="'.the_title('','',false).'"]' ); ?>
						</div>
					</div>
				</div>
			</section>

			<section id="page-destinos" class="py-5 bg-cinza-claro">
				<div class="container">
					<div class="input-group">
						<h3 class="text-uppercase text-azul align-self-center mb-0 mr-3">VEJA MAIS DESTINOS</h3>
						<a href="<?php echo get_permalink( get_page_by_path( 'destino' ) ); ?>"
						   class="btn btn-lg bg-azul text-white px-4">
							VER TODOS
						</a>
					</div>
					<div class="row align-items-stretch pt-5">
						<?php

						$args = array("post_type"=>"destinos","order"=>"ASC");
						$the_query = new WP_Query($args);

						if ($the_query->have_posts()){
							while ($the_query->have_posts()){
								$the_query->the_post();
								?>
								<div class="col-sm-4 col-md-6 col-lg-3 mb-4 d-flex align-items-stretch">
									<div class="card">
										<img class="img-fluid card-img-top" src="<?php the_field('imagem'); ?>">
										<div class="card-body">
											<p class="font-14 mb-0 mt-1"><?php the_field('saida');?></p>
											<h5 class="font-weight-bold text-laranja mb-0"><?php
                                                $titulo = get_field('titulo');
												$titulo = mb_strtolower($titulo,'UTF-8');
												if (strlen($titulo) > 24){
													$titulo = mb_substr($titulo,0,21);
													$titulo = mb_convert_case($titulo, MB_CASE_TITLE,"UTF-8");
													$titulo .= "...";
												}
												echo $titulo;
                                                ?></h5>
											<p class="font-weight-bold mb-0"><?php the_field('dias'); ?> dias</p>
											<div class="border my-2"></div>
											<p class="font-14 mb-0"><?php the_field('entrada');?></p>
											<h4 class="text-azul font-weight-bold mb-0"><?php the_field('valor_parcelado');?></h4>
                                        	<p class="font-14 mb-0 mt-1"><?php the_field('valor_total');?></p>
										</div>
										 <div class="card-footer">
										 	<div class="row">
										 		<div class="col-4 col-sm-4 col-md-4 col-lg-4 p-0">
										 		 	<img src="<?php the_field('logo') ;?>" class="img-fluid">
										 		</div>
										 		<div class="col-8 col-sm-8 col-md-8 col-lg-8"> 
													<a href="<?php the_permalink();?>" class="btn btn-lg bg-laranja text-white px-4 btn-detalhes">
														DETALHES
													</a>
												</div>
											</div>
											<!--<a href="#" class="btn btn-lg bg-laranja text-white px-4">
												VER DETALHES
											</a>-->
										</div>
									</div>
								</div>
								<?php
							}
						}
						?>

					</div>
				</div>
			</section>
		</main>

		<?php
	}
}
		?>

<?php get_footer('newtmpl'); ?>