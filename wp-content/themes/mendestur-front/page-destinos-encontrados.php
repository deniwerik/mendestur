<?php
// Template Name: Destinos Encontrados
session_start();


if (isset($_SESSION["pacote"])){

    $pacote = array();
    $pacote = $_SESSION["pacote"];
} else {
    $pacotes = array();
    foreach ($_SESSION["pacotes"] as $key => $value){
        array_push($pacotes,$value);
    }
}

$idpacote = $_SESSION["idpacote"];
$idregiao = $_SESSION["idregiao"];

get_header('newtmpl');
?>


	<main>

		<!--PESQUISA-->
        <?php
        require_once (TEMPLATEPATH."/includes/busca.php");
        ?>

		<section id="destino-header" class="py-5">
			<div class="container">
				<h1 class="text-center text-white text-uppercase bottom-line-header line-center">Destinos encontrados</h1>
			</div>
		</section>

		<section id="page-destinos" class="bg-cinza-claro py-5">
			<div class="container">
				<div class="row">
                    <?php
                        if (isset($pacote) && $idpacote !== "no match") {
                    ?>
					<div class="col-lg-3 mb-4">
						<div class="card">
							<img class="img-fluid card-img-top" src="<?=$pacote['imagem'];?>" alt="Card image cap">
							<div class="card-body">
								<h5 class="font-weight-bold text-laranja mb-0"><?php
                                    $titulo = $pacote["titulo"];
									$titulo = mb_strtolower($titulo,'UTF-8');
									$titulo = mb_convert_case($titulo, MB_CASE_TITLE,"UTF-8");
									if (strlen($titulo) > 24){
										$titulo = mb_substr($titulo,0,21);

										$titulo .= "...";
									}
									echo $titulo;
                                    ?></h5>
								<p class="font-weight-bold mb-0"><?=$pacote['dias'];?> dias</p>
								<div class="border my-2"></div>
								<!--<p class="font-14 mb-0 mt-1">
                                    <?php
/*                                    $parc = $pacote["valor"] / 12;
                                    $parc = round($parc,2);
                                    $parc = str_replace(".",",",$parc);
                                    echo ("12x de " . $pacote['moeda'] . " " . $parc);
                                    */?>
                                </p>-->
                                <p class="text-azul font-weight-bold mb-0"><?/*=$pacote["moeda"];*/?>
                                    <span class="h4 font-weight-bold">
                                        <?php
                                            $valor = $pacote["valor"];
                                            if ($valor === "0"){
                                                $valor = "Consulte-nos";
                                            } else {
                                                $valor .= ",00";
                                            }
                                            echo $valor;
                                        ?>
                                    </span>
                                </p>

                                <div class="row">
                                    <div class="col-4 col-sm-4 col-lg-4 p-0 align-self-center">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/destinos/ancoradouro.png" class="img-fluid">
                                    </div>
                                    <div class="col-8 col-sm-8  col-lg-8">
                                         <button class="btn btn-lg bg-laranja text-white px-4 btn-detalhes"
                                        data-regiao="<?=$idregiao;?>" data-pacote="<?=$idpacote;?>">
                                        DETALHES
                                        </button>
                                    </div>
                                </div>
							</div>
						</div>
					</div>

                        <?php
                    } else {
                    foreach ($pacotes as $key => $value) {
                    ?>

                        <div class="col-lg-3 mb-4">
                            <div class="card">
                                <img class="img-fluid card-img-top" src="<?= $value['imagem']; ?>" alt="Card image cap">
                                <div class="card-body">
                                    <p class="font-14 mb-0 mt-1"><?=$value["saida"];?></p>
                                    <h5 class="font-weight-bold text-laranja mb-0"><?php
	                                    $titulo = $value["titulo"];
	                                    $titulo = mb_strtolower($titulo,'UTF-8');
	                                    $titulo = mb_convert_case($titulo, MB_CASE_TITLE,"UTF-8");
	                                    if (strlen($titulo) > 24){
		                                    $titulo = mb_substr($titulo,0,21);

		                                    $titulo .= "...";
	                                    }
	                                    echo $titulo;
                                        ?></h5>
                                    <p class="font-weight-bold mb-0"><?= $value['dias']; ?> dias</p>
                                    <div class="border my-2"></div>
<!--                                    <p class="font-14 mb-0 mt-1">-->
<!--                                        --><?php
//                                        $parc = $value["valor"] / 12;
//                                        $parc = round($parc, 2);
//                                        $parc = str_replace(".", ",", $parc);
//                                        echo("12x de " . $value['moeda'] . " " . $parc);
//                                        ?>
<!--                                        <!--2x de R$ 96,00-->
<!--                                    </p>-->
                                        <p class="text-azul font-weight-bold mb-0"><?/*= $value["moeda"]; */?>
                                            <span class="h4 font-weight-bold">
                                                <?php
                                                $valor = $value["valor"];
                                                if ($valor === "0"){
                                                    $valor = "Consulte-nos";
                                                } else {
                                                    $valor .= ",00";
                                                }
                                                echo $valor;
                                                ?>
                                            </span>
                                        </p>
                                    <div class="row">
                                        <div class="col-4 col-sm-4 col-lg-4 p-0 align-self-center">
                                            <img src="<?php echo get_template_directory_uri(); ?>/img/destinos/ancoradouro.png" class="img-fluid">
                                        </div>
                                        <div class="col-8 col-sm-8  col-lg-8">
                                            <button class="btn btn-lg bg-laranja text-white px-4 detalhes-btn"
                                            data-regiao="<?= $idregiao; ?>" data-pacote="<?= $value["id"]; ?>">
                                            DETALHES
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                    }
                        }
                    ?>




				</div>
			</div>
		</section>

      
	</main>

<?php get_footer('newtmpl'); ?>