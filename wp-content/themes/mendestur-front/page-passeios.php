<?php
// Template Name: Passeios
get_header('newtmpl');
?>

<?php
if (have_posts()){
	while (have_posts()){
	    the_post();
		?>
        <main id="iframe">

            <div class="intrinsic-container intrinsic-container-16x9">
                <iframe src="https://www.oquefazernaviagem.com.br/?token=e658d2f9-36f8-474e-a57f-2f29d11095d7"
                        scrolling="auto" frameborder="0" id="iframe1" allowfullscreen>Passeios</iframe>
            </div>

			<?php
			/*require_once (TEMPLATEPATH."/includes/instagram.php");*/
            ?>

        </main>

		<?php
	}
} else {
	_e('Sorry, no posts matched your criteria.');
}
?>

<?php
get_footer('newtmpl');
?>