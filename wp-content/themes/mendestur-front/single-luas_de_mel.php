<?php
// Template Name: Single Lua de mel
get_header('newtmpl');
?>

<?php
if (have_posts()){
	while ( have_posts() ){
		the_post();

		$args      = array( "post_type" => "luas_de_mel", "order" => "ASC" );
		$the_query = new WP_Query( $args );

		if ( $the_query->have_posts() ){
			while ( $the_query->have_posts() ){
				$the_query->the_post();
				?>

                <main>
                <section id="lua-de-mel-interna-header" class="py-5">
                    <div class="container">
                        <h1 class="text-center text-white font-dancing-script"><?php the_field('primeiro_nome_noivo') ;?> <i class="fas fa-heart text-laranja mx-3"></i> <?php the_field('primeiro_nome_noiva') ;?></h1>
                        <h2 class="text-white text-center font-weight-light"><?php the_field('data') ;?></h2>
                    </div>
                </section>

                <section id="page-interna-lua-de-mel" class="py-5">
                    <div class="container">
                        <ul class="nav mb-4">
                            <li class="nav-item">
                                <a class="nav-link active" href="#cerimonia"><img src="<?= get_template_directory_uri()?>/img/icons/calendar-wedding.svg" class="img-fluid icons mr-1"><span>Cerimonia</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#presente"><img src="<?= get_template_directory_uri()?>/img/icons/gift.svg" class="img-fluid icons mr-1"> <span>Presente</span></a>
                            </li>

							<?php
							if (get_field('recado_noivos') === true){
								?>
                                <li class="nav-item">
                                    <a class="nav-link" href="#recado"><img src="<?= get_template_directory_uri()?>/img/icons/love.svg" class="img-fluid icons mr-1"> <span>Recado</span></a>
                                </li>
								<?php
							}
							?>


							<?php
							if (get_field('lua_de_mel') === true){
								?>
                                <li class="nav-item">
                                    <a class="nav-link" href="#viagem"><img src="<?= get_template_directory_uri()?>/img/icons/placeholder.svg" class="img-fluid icons mr-1"> <span>Viagem</span></a>
                                </li>

								<?php
							}
							?>


                        </ul>

                        <div class="row justify-content-center mt-5">
                            <div class="col-md-4 col-lg-4">
                                <h5 class="text-center">Noiva (o)</h5>
                                <img src="<?php the_field('foto_noiva') ;?>" class="img-fluid mb-4">
                                <h4 class="text-laranja text-center font-weight-bold font-italic"><?php the_field('nome_completo_noiva') ;?></h4>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <h5 class="text-center">Noivo (a)</h5>
                                <img src="<?php the_field('foto_noivo') ;?>" class="img-fluid mb-4">
                                <h4 class="text-laranja text-center font-weight-bold font-italic"><?php the_field('nome_completo_noivo') ;?></h4>
                            </div>
                        </div>
                    </div>
                </section>

				<?php
				//$info = get_field('informacoes_cerimonia');

				if(get_field('informacoes_cerimonia') === true){
					?>

                    <section id="cerimonia" class="bg-cinza-claro py-5">
                        <div class="container">
                            <div class="bg-white p-5 text-center">
                                <h3 class="text-azul"><img src="<?= get_template_directory_uri()?>/img/icons/calendar-wedding.svg" class="img-fluid icons mr-2">Informações da cerimônia</h3>
                                <h4 class="text-cinza-claro font-dancing-script">Local</h4>
                                <h5><?php the_field('local_cerimonia') ;?><br><?php the_field('local_cerimonia2') ;?></h5>
                                <h4 class="text-cinza-claro font-dancing-script">Data</h4>
                                <h5><?php the_field('data') ;?></h5>
                                <h4 class="text-cinza-claro font-dancing-script">Horário</h4>
                                <h5><?php the_field('horario') ;?></h5>
                            </div>
                        </div>
                    </section>
					<?php
				}
				?>

                <section id="presente" class="py-5 text-center">
                    <div class="container">
                        <h3 class="text-azul"><img src="<?= get_template_directory_uri()?>/img/icons/gift.svg" class="img-fluid icons mr-2">Presente de lua de mel</h3>
                        <p>A lua de mel! A viagem dos sonhos de um casal. Você, convidado e amigo dos noivos pode ajudá-los a vivenciarem um dos melhores momentos de suas vidas.</p>
                        <p>Escolha o presente/momento que deseja ofertar aos noivos, confira os dados bancários e faça o depósito.</p>
                        <br>
                        <p>Simples assim! Com certeza será uma viagem inesquecível...</p>

						<?php
						$rows = get_field('presente');
						if (count($rows) == 1) {
							?>
                            <!--exibição de um unico presente/momento-->
                            <div class="border p-5">
                                <h5><?= $rows[0]['titulo_presente']; ?></h5>
                                <img src="<?= $rows[0]['imagem_presente'];?>">
                                <br><br>
                                <h4 class="text-laranja font-weight-bold">
                                    R$ <?= $rows[0]['valor_presente']; ?>
                                </h4>
                            </div>

							<?php
						} else if (count($rows) > 1){
							echo('<div class="row">');
							foreach ($rows as $row){
								echo('<div class="col-lg-6">');
								?>
                                <!-- exibição de mais de um presente/momento -->
                                <div class="border p-5" style="width: fit-content">
                                    <h5><?= $row['titulo_presente'];?></h5>
                                    <img src="<?= $row['imagem_presente'];?>" width="300px" height="300px">
                                    <br><br>
                                    <h4 class="text-laranja font-weight-bold">
                                        R$ <?= $row['valor_presente'];?>
                                    </h4>
                                </div>

								<?php
								echo("</div>");
							}
							echo("</div>");

						}
						?>

                        <a href="#" class="btn btn-lg bg-laranja text-white px-4 my-4" data-toggle="modal" data-target="#exampleModal">
                            Dados bancários para depósito
                        </a>

                        <!--MODAL-DEPOSITO-->
                        <div class="modal fade lua-de-mel" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <div class="modal-body p-4">
                                        <h4 class="text-cinza-claro">Banco:</h4>
                                        <h5><?php the_field('banco') ;?></h5>
                                        <h4 class="text-cinza-claro">Agência:</h4>
                                        <h5><?php the_field('agencia') ;?></h5>
                                        <h4 class="text-cinza-claro">Conta</h4>
                                        <h5><?php the_field('conta') ;?></h5>
                                        <h4 class="text-cinza-claro">Nome</h4>
                                        <h5><?php the_field('nome_conta') ;?></h5>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <p class="font-12">Sam quas dus. Ur? Pudae sime premolu ptibusandit, estium nulpa nobit offic tet idestru mquunt qui autemquamus, sum ditiasi nciumque. Berum volore eum quam, explia volupti onsenihil ma ad etur mo beat.</p>

						<?php
						if (get_field('recado_noivos') === true){
							?>
                            <h3 class="text-azul" id="recado">Deixe seu recado aos noivos</h3>

                            <div class="form-orcamento bg-cinza p-4 m-5">
                                <h5 class="font-weight-bold">Fomulário de Lua de Mel</h5>
                                <!-- <form>
                                    <input type="text" class="form-control mb-2" placeholder="Nome completo*">
                                    <input type="text" class="form-control mb-2" placeholder="Email*">
                                    <input type="text" class="form-control mb-2" placeholder="Telefone*">
                                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="4" placeholder="Informações complementares"></textarea>
                                    <button type="submit" class="btn bg-laranja text-white px-5 mt-3 mb-2">ENVIAR</button>
                                </form> -->
								<?php echo do_shortcode( '[contact-form-7 id="207" title="Lua_de_Mel_Recado" email-noiva="'.get_field('email_noiva').'" email-noivo="'.get_field('email_noivo').'" ]' ); ?>
                            </div>

							<?php
						}
						?>

						<?php
						if (get_field('lua_de_mel') === true){
							?>

                            <h3 class="text-azul" id="viagem">Lua de mel</h3>

                            <h5 class="font-weight-bold font-italic">Veneza - Itália</h5>

                            <div class="row mt-5 galeria">

								<?php
								if (have_rows('fotos')) {
									while ( have_rows( 'fotos' ) ) {
										the_row();
										?>

                                        <div class="col-md-4 col-lg-4 p-0">
                                            <img src="<?php the_sub_field('foto') ;?>" class="img-fluid" id="test1">
                                        </div>

										<?php
									}
								}
								?>
                                <!--<div class="col-md-4 col-lg-4 p-0">
									<img src="img/lua-de-mel/veneza2.jpg" class="img-fluid">
								</div>
								<div class="col-md-4 col-lg-4 p-0">
									<img src="img/lua-de-mel/veneza3.jpg" class="img-fluid">
								</div>
								<div class="col-md-4 col-lg-4 p-0">
									<img src="img/lua-de-mel/veneza3.jpg" class="img-fluid">
								</div>
								<div class="col-md-4 col-lg-4 p-0">
									<img src="img/lua-de-mel/veneza2.jpg" class="img-fluid">
								</div>
								<div class="col-md-4 col-lg-4 p-0">
									<img src="img/lua-de-mel/veneza1.jpg" class="img-fluid">
								</div>
								<div class="col-md-4 col-lg-4 p-0">
									<img src="img/lua-de-mel/veneza1.jpg" class="img-fluid">
								</div>
								<div class="col-md-4 col-lg-4 p-0">
									<img src="img/lua-de-mel/veneza2.jpg" class="img-fluid">
								</div>
								<div class="col-md-4 col-lg-4 p-0">
									<img src="img/lua-de-mel/veneza3.jpg" class="img-fluid">
								</div>-->
                            </div>

							<?php
						}
						?>
                    </div>
                </section>

				<?php
			}
		}
		wp_reset_query();
		wp_reset_postdata();

		?>
        </main>
		<?php
	}
}
?>

<?php get_footer('newtmpl'); ?>
