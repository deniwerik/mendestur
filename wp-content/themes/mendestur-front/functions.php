<?php

// Função para registrar os Scripts e o CSS
function mendestur_scripts(){
	// Desregistra
	wp_deregister_script('jquery');
	// Registra
	wp_register_script('jquery', get_template_directory_uri() . '/scripts/jquery-3.3.1.js', array(), "3.3.1",false);

	wp_register_script('js', get_template_directory_uri() . '/scripts/js.js', array(),false,true);

	wp_register_script('owlcarousel', get_template_directory_uri() . '/inc/owlcarousel/owl.carousel.min.js', array('jquery'), false,true);

	// Carrega o Script
	wp_enqueue_script('js');
	wp_enqueue_script('owlcarousel');
}
add_action('wp_enqueue_scripts', 'mendestur_scripts');

function mendestur_css(){
	//wp_register_style('mendestur-style', get_stylesheet_directory_uri() . '/style.css', array(), false, false);
	wp_register_style('mendestur-style', get_template_directory_uri() . '/style.css', array(), false, false);
	wp_register_style('owlcarousel', get_template_directory_uri() . "/inc/owlcarousel/owl.carousel.min.css", array(),false,false);
	wp_register_style('owltheme', get_template_directory_uri() . "/inc/owlcarousel/owl.theme.default.min.css",array(),false,false);

	wp_enqueue_style('mendestur-style');
	wp_enqueue_style('owlcarousel');
	wp_enqueue_style('owltheme');
}
add_action('wp_enqueue_scripts', 'mendestur_css');

// Funções para Limpar o Header
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'start_post_rel_link', 10, 0 );
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');

// Habilitar Menus
add_theme_support('menus');

//registrar menu
function register_my_menu() {
	register_nav_menu('menu-principal',__( 'Menu Principal' ));
}
add_action( 'init', 'register_my_menu' );

function set_nav_links($atts){
	$atts['class'] = "nav-link";
	return $atts;
}

add_filter( 'nav_menu_link_attributes', 'set_nav_links', 100, 1 );

// Custom Post Type

function custom_post_type_destinos() {
	register_post_type('destinos', array(
		'label' => 'Destinos',
		'description' => 'Destinos',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'rewrite' => array('slug' => 'destinos', 'with_front' => true),
		'query_var' => true,
		'supports' => array('title', 'editor', 'page-attributes','post-formats'),

		'labels' => array (
			'name' => 'Destinos',
			'singular_name' => 'Destino',
			'menu_name' => 'Destinos',
			'add_new' => 'Adicionar Novo',
			'add_new_item' => 'Adicionar Novo Destino',
			'edit' => 'Editar',
			'edit_item' => 'Editar Destino',
			'new_item' => 'Novo Destino',
			'view' => 'Ver Destino',
			'view_item' => 'Ver Destino',
			'search_items' => 'Procurar Destinos',
			'not_found' => 'Nenhum Destino Encontrado',
			'not_found_in_trash' => 'Nenhum Destino Encontrado no Lixo',
		)
	));
}
add_action('init', 'custom_post_type_destinos');

/*function custom_post_type_destinos_internos() {
	register_post_type('destinos_internos', array(
		'label' => 'Destinos_internos',
		'description' => 'Destinos_internos',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'rewrite' => array('slug' => 'destinos_internos', 'with_front' => true),
		'query_var' => true,
		'supports' => array('title', 'editor', 'page-attributes','post-formats'),

		'labels' => array (
			'name' => 'Destinos_internos',
			'singular_name' => 'Destino_interno',
			'menu_name' => 'Destinos_internos',
			'add_new' => 'Adicionar Novo',
			'add_new_item' => 'Adicionar Novo Destino Interno',
			'edit' => 'Editar',
			'edit_item' => 'Editar Destino Interno',
			'new_item' => 'Novo Destino Interno',
			'view' => 'Ver Destino Interno',
			'view_item' => 'Ver Destino Interno',
			'search_items' => 'Procurar Destinos Internos',
			'not_found' => 'Nenhum Destino Interno Encontrado',
			'not_found_in_trash' => 'Nenhum Destino Interno Encontrado no Lixo',
		)
	));
}
add_action('init', 'custom_post_type_destinos_internos');*/

function custom_post_type_cruzeiros() {
	register_post_type('cruzeiros', array(
		'label' => 'Cruzeiros',
		'description' => 'Cruzeiros',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'rewrite' => array('slug' => 'cruzeiros', 'with_front' => true),
		'query_var' => true,
		'supports' => array('title', 'editor', 'page-attributes','post-formats'),

		'labels' => array (
			'name' => 'Cruzeiros',
			'singular_name' => 'Cruzeiro',
			'menu_name' => 'Cruzeiros',
			'add_new' => 'Adicionar Novo',
			'add_new_item' => 'Adicionar Novo Cruzeiro',
			'edit' => 'Editar',
			'edit_item' => 'Editar Cruzeiro',
			'new_item' => 'Novo Cruzeiro',
			'view' => 'Ver Cruzeiro',
			'view_item' => 'Ver Cruzeiro',
			'search_items' => 'Procurar Cruzeiros',
			'not_found' => 'Nenhum Cruzeiro Encontrado',
			'not_found_in_trash' => 'Nenhum Cruzeiro Encontrado no Lixo',
		)
	));
}
add_action('init', 'custom_post_type_cruzeiros');

add_action( 'init', 'create_custom_tax_categoria' );
function create_custom_tax_categoria(){
	$custom_tax_nome = 'categoria_do_cruzeiro';
	$custom_post_type_nome = 'cruzeiros';
	$args = array(
		'label' => __('Categoria do Cruzeiro'),
		'hierarchical' => true,
		'rewrite' => array('slug' => 'categoria')
	);
	register_taxonomy( $custom_tax_nome, $custom_post_type_nome, $args );
}

function custom_post_type_luas_de_mel() {
	register_post_type('luas_de_mel', array(
		'label' => 'Luas de mel',
		'description' => 'Luas de mel',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'rewrite' => array('slug' => 'luas_de_mel', 'with_front' => true),
		'query_var' => true,
		'supports' => array('title', 'editor', 'page-attributes','post-formats'),

		'labels' => array (
			'name' => 'Luas de mel',
			'singular_name' => 'Lua de mel',
			'menu_name' => 'Luas de mel',
			'add_new' => 'Adicionar Novo',
			'add_new_item' => 'Adicionar Nova Lua de mel',
			'edit' => 'Editar',
			'edit_item' => 'Editar Lua de mel',
			'new_item' => 'Nova Lua de mel',
			'view' => 'Ver Lua de mel',
			'view_item' => 'Ver Lua de mel',
			'search_items' => 'Procurar Luas de mel',
			'not_found' => 'Nenhuma Lua de mel Encontrado',
			'not_found_in_trash' => 'Nenhuma Lua de mel Encontrada no Lixo',
		)
	));
}
add_action('init', 'custom_post_type_luas_de_mel');

/*function custom_post_type_navios() {
	register_post_type('navios', array(
		'label' => 'Navios',
		'description' => 'Navios',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'rewrite' => array('slug' => 'navios', 'with_front' => true),
		'query_var' => true,
		'supports' => array('title', 'editor', 'page-attributes','post-formats'),

		'labels' => array (
			'name' => 'Navios',
			'singular_name' => 'Navio',
			'menu_name' => 'Navios',
			'add_new' => 'Adicionar Novo',
			'add_new_item' => 'Adicionar Novo Navio',
			'edit' => 'Editar',
			'edit_item' => 'Editar Navio',
			'new_item' => 'Novo Navio',
			'view' => 'Ver Navio',
			'view_item' => 'Ver Navio',
			'search_items' => 'Procurar Navios',
			'not_found' => 'Nenhum Navio Encontrado',
			'not_found_in_trash' => 'Nenhum Navio Encontrado no Lixo',
		)
	));
}
add_action('init', 'custom_post_type_navios');*/


/* Contat Form 7 - Função para pegar parâmetro de short code */
add_filter( 'shortcode_atts_wpcf7', 'custom_shortcode_atts_wpcf7_filter', 10, 3 );
 
function custom_shortcode_atts_wpcf7_filter( $out, $pairs, $atts ) {
    $my_attr = 'destination-email';
 
    if ( isset( $atts[$my_attr] ) ) {
        $out[$my_attr] = $atts[$my_attr];
    }
 
    return $out;
}


?>