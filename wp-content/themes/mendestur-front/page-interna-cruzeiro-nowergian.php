<?php
// Template Name: Cruzeiro Nowergian Interna
get_header('newtmpl');
?>
<!-- Cruzeiro Nowergian Interna -->
<main>
	<section id="interna-cruzeiro-header">
		<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/header-cruzeiro-msc.jpg" class="img-fluid w-100 d-block">
		<div class="hero">
			<h1 class="text-center text-white text-uppercase bottom-line-header line-center">Norwegian Cruise Line</h1>
		</div>
	</section>

	<section id="page-interna-cruzeiro" class="pb-2">
		<div class="container">
			<ul class="nav mb-4">
				<li class="nav-item">
					<a class="nav-link active" href="#sobre">Sobre</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#solicitar">Solicitar</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#cat-cruzeiros">Cruzeiros</a>
				</li>
			</ul>

			<div class="bg-norwegian">
				<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/logo-norwegian.png" class="m-auto d-block logo">
			</div>

			<div class="row py-4" id="sobre">
				<div class="col-lg-6">
					<h2 class="text-azul">Norwegian Cruise Line</h2>
					<p>Com a Norwegian Cruise Line, você estará navegando na frota mais inovadora em alto mar. Os navios são os mais premiados do mundo foram feitos para quem gosta da liberdade e flexibilidade de escolha. Suba a bordo para as férias perfeitas.</p>
					<p class="text-uppercase font-weight-bold heading pl-2">
						Informações extras
						<span class="divider-left"></span>
					</p>
					<p><strong>Frota:</strong> 16 Navios</p>
					<p><strong>Destinos:</strong> Alasca, América do Sul, Bahamas, Bermudas, Canadá, Canal do Panamá, Caribe, Costa Pacífico, Cuba, Europa, Havai, Riviera Mexicana.</p>
					<p><strong>Serviços:</strong> Entretenimento, Atividades diversas, Bares e Lounges, SPA, Cabines e Suítes, Resturantes gratuitos.</p>

				</div>
				<div class="col-lg-6">
					<!--carousel-->
					<div id="sync1" class="owl-carousel owl-theme">
						<div class="item">
							<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/01.jpg" class="img-fluid">
							<div class="carousel-caption">
								<p class="font-14 text-white mb-0 text-uppercase font-weight-bold">getaway</p>
							</div>
						</div>
						<div class="item">
							<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc-carrousel/02.jpg" class="img-fluid">
						</div>
						<div class="item">
							<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc-carrousel/03.jpg" class="img-fluid">
						</div>
						<div class="item">
							<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc-carrousel/04.jpg" class="img-fluid">
						</div>
						<div class="item">
							<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc-carrousel/01.jpg" class="img-fluid">
						</div>
					</div>
					
					<div id="sync2" class="owl-carousel owl-theme">
						<div class="item">
							<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/01.jpg" class="img-fluid">
						</div>
						<div class="item">
							<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc-carrousel/02.jpg" class="img-fluid">
						</div>
						<div class="item">
							<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc-carrousel/03.jpg" class="img-fluid">
						</div>
						<div class="item">
							<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc-carrousel/04.jpg" class="img-fluid">
						</div>
						<div class="item">
							<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc-carrousel/01.jpg" class="img-fluid">
						</div>
					</div>
				</div>
			</div>

			<div class="text-center bg-cinza py-4">
				<a href="<?php echo get_template_directory_uri(); ?>/page-cruzeiro.html" class="btn btn-lg bg-azul text-uppercase text-white px-5">
					Buscar todos os cruzeiros	
				</a>
			</div>

			<div class="form-orcamento bg-cinza p-4 mt-5" id="solicitar">
				<h5 class="font-weight-bold">Ficou interessado?</h5>
				<p>Preencha o formulário abaixo, e solicite o cruzeiro que deseja.</p>
				<form>
					<div class="row">
						<div class="col-12 col-sm-12 col-lg-4 pb-2">
							<input type="text" class="form-control" placeholder="Nome">
						</div>
						<div class="col-12 col-sm-12 col-lg-4 pb-2">
							<input type="text" class="form-control" placeholder="Telefone">
						</div>
						<div class="col-12 col-sm-12 col-lg-4 pb-2">
							<input type="text" class="form-control" placeholder="Email">
						</div>

						<div class="col-12 col-sm-12 col-lg-4 pb-2">
							<div class="form-group mb-0">
								<div class="input-group date" data-provide="datepicker">
									<div class="input-group-prepend">
										<span class="input-group-text" id="basic-addon1">
											<i class="far fa-calendar-alt text-laranja"></i>
										</span>
									</div>
									<input type='text' class="form-control" placeholder="Mês de saída" />
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-12 col-lg-4 pb-2">
							<input type="text" class="form-control" placeholder="Destino">
						</div>
						<div class="col-12 col-sm-12 col-lg-4 pb-2">
							<select class="form-control classic" id="exampleFormControlSelect1">
								<option selected>Navio</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
							</select>
						</div>

						<div class="col-12 col-sm-12 col-lg-4 pb-2">
							<select class="form-control classic" id="exampleFormControlSelect1">
								<option selected>Porto de embarque</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
							</select>
						</div>
						<div class="col-12 col-sm-12 col-lg-4 pb-2">
							<select class="form-control classic" id="exampleFormControlSelect1">
								<option selected>Tipo de cabine</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
							</select>
						</div>
						<div class="col-6 col-sm-6 col-lg-2 pb-2">
							<select class="form-control classic" id="exampleFormControlSelect1">
								<option selected>Adultos</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
							</select>
						</div>
						<div class="col-6 col-sm-6 col-lg-2 pb-2">
							<select class="form-control classic" id="exampleFormControlSelect1">
								<option selected>Crianças</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
							</select>
						</div>
					</div>
					<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" placeholder="Informações complementares"></textarea>
					<button type="submit" class="btn bg-laranja text-white px-5 mt-3 mb-2">CONFIRMAR INTERESSE</button>
				</form>
			</div>
		</div>
	</section>

	<section id="cat-cruzeiros" class="py-5">
		<div class="container">
			<h2 class="text-azul text-uppercase mb-3">Cruzeiros</h2>

			<div class="row">
				<div class="col-lg-12 border mb-2">
					<div class="row">
						<div class="col-lg-3 py-3">
							<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/escape.jpg" class="img-fluid">
						</div>
						<div class="col-lg-3 py-3 border-right">
							<p class="font-14 mb-0">Norwegian Cruise Line</p>
							<h3 class="text-azul font-weight-bold mb-0">ESCAPE</h3>
							<p class="mb-0"><strong>Saída:</strong> Miami - 14/04/2018</p>
							<h5 class="text-laranja font-weight-bold">6 noites - Caribe</h5>
						</div>
						<div class="col-lg-3 py-3">
							<p class="font-12 mb-0">Valor Total</p>
							<h1 class="text-azul font-weight-bold">R$ 2.440,47</h1>
							<p class="mb-0 font-12">Preço por pessoa em cabine dupla interna garantida. Tarifa aérea não inclusa.</p>
							<div class="triangulo"></div>
						</div>
						<div class="col-lg-3 bg-laranja py-5 text-center">
							<a href="http://dev.pxpdigital.com.br/mendestur-front/page-interna-cruzeiro-nowergian-interna/" class="btn btn-lg btn-border text-white px-4">
								VER TODOS
							</a>
						</div>
					</div>
				</div>

				<div class="col-lg-12 border mb-2">
					<div class="row">
						<div class="col-lg-3 py-3">
							<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/escape.jpg" class="img-fluid">
						</div>
						<div class="col-lg-3 py-3 border-right">
							<p class="font-14 mb-0">Norwegian Cruise Line</p>
							<h3 class="text-azul font-weight-bold mb-0">ESCAPE</h3>
							<p class="mb-0"><strong>Saída:</strong> Miami - 14/04/2018</p>
							<h5 class="text-laranja font-weight-bold">6 noites - Caribe</h5>
						</div>
						<div class="col-lg-3 py-3">
							<p class="font-12 mb-0">Valor Total</p>
							<h1 class="text-azul font-weight-bold">R$ 2.440,47</h1>
							<p class="mb-0 font-12">Preço por pessoa em cabine dupla interna garantida. Tarifa aérea não inclusa.</p>
							<div class="triangulo"></div>
						</div>
						<div class="col-lg-3 bg-laranja py-5 text-center">
							<a href="http://dev.pxpdigital.com.br/mendestur-front/page-interna-cruzeiro-nowergian-interna/" class="btn btn-lg btn-border text-white px-4">
								VER TODOS
							</a>
						</div>
					</div>
				</div>

				<div class="col-lg-12 border mb-2">
					<div class="row">
						<div class="col-lg-3 py-3">
							<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/escape.jpg" class="img-fluid">
						</div>
						<div class="col-lg-3 py-3 border-right">
							<p class="font-14 mb-0">Norwegian Cruise Line</p>
							<h3 class="text-azul font-weight-bold mb-0">ESCAPE</h3>
							<p class="mb-0"><strong>Saída:</strong> Miami - 14/04/2018</p>
							<h5 class="text-laranja font-weight-bold">6 noites - Caribe</h5>
						</div>
						<div class="col-lg-3 py-3">
							<p class="font-12 mb-0">Valor Total</p>
							<h1 class="text-azul font-weight-bold">R$ 2.440,47</h1>
							<p class="mb-0 font-12">Preço por pessoa em cabine dupla interna garantida. Tarifa aérea não inclusa.</p>
							<div class="triangulo"></div>
						</div>
						<div class="col-lg-3 bg-laranja py-5 text-center">
							<a href="http://dev.pxpdigital.com.br/mendestur-front/page-interna-cruzeiro-nowergian-interna/" class="btn btn-lg btn-border text-white px-4">
								VER TODOS
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!-- <?php
	require_once (TEMPLATEPATH."/includes/instagram.php");
	?> -->

</main>

<?php get_footer('newtmpl'); ?>
