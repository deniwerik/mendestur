<footer class="bg-azul pt-5 pb-4">
    <div class="container">
        <h5 class="text-white mb-4">Cadastre-se e receba ofertas no seu e-mail!</h5>
        <div class="row">
            <div class="col-lg-8">
                <form>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" id="inputName" placeholder="Nome">
                        </div>
                        <div class="col">
                            <input type="password" class="form-control" id="inputEmail" placeholder="Email">
                        </div>
                        <div class="col-2">
                            <button type="submit" class="btn bg-laranja mb-2 text-white px-4">Enviar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-3 align-self-center ml-auto">
                <div class="social-media float-right">
                    <h5 class="text-white">Siga-nos:</h5>
                    <div class="btn-circle">
                        <i class="fab fa-instagram fa-1x fa-inverse" aria-hidden="true"></i>
                    </div>
                    <div class="btn-circle">
                        <i class="fab fa-facebook-f fa-1x fa-inverse" aria-hidden="true"></i>
                    </div>
                </div>

            </div>
        </div>
        <nav id="mainNav" class="navbar navbar-expand-lg navbar-light pt-4">
            <ul class="navbar-nav m-auto text-uppercase">
                <li class="nav-item">
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Aéreos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Cruzeiros</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Destinos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Orçamento</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Lua de Mel</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Câmbio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Quem somos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contato</a>
                </li>
            </ul>
        </nav>
    </div>
</footer>

<div class="bg-azul-marinho py-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-8">
                <p class="text-white mb-0 font-weight-light">© Copyright - Alguns direitos reservados <strong>Mendes Tur</strong></p>
            </div>
            <div class="col-lg-6 col-md-4">
                <p class="text-white mb-0 float-right">Desenvolvido por <span class="font-weight-bold text-laranja">PXP Digital</span></p>
            </div>
        </div>
    </div>
</div>


<!-- jQuery first, then Popper.scripts, then Bootstrap JS -->
    <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
    <script src="scripts/jquery-3.3.1.js" type="application/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script src="scripts/js.js" type="application/javascript"></script>
    
    <!--OwlCarousel-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>

    <!--DatePicker-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.pt-BR.min.js"></script>
    
    <!--Afixx-->
    <!-- <script src="https://rawgit.com/bassjobsen/affix/master/assets/js/affix.js"></script> -->

<script>
        $(document).ready(function() {
            $('#slider-carousel').owlCarousel({
                loop:true,
                margin:20,
                nav:true,
                dots: false,
                navText : ['<span class="uk-margin-small-right uk-icon"><img src="img/icons/arrows-01.svg" class="prev-icon"></span>','<span class="uk-margin-small-left uk-icon"><img src="img/icons/arrows-02.svg" class="prev-icon"></span>'],
                responsive:{
                    0:{
                        items:1
                    },
                    640:{
                        items:1
                    },
                    960:{
                        items:3
                    },
                    1000:{
                        items:4
                    }
                }
            })
        });
    </script>

    <script>
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            language: 'pt-BR',
            autoclose: true,
            zIndexOffset: 10,
        });
    </script>

    <script>
        $( document ).ready(function() {
            var affixElement = '#mainNav';

            $(affixElement).affix({
                offset: {
                    // Distance of between element and top page
                    top: function () {
                        return (this.top = $(affixElement).offset().top)
                    },
                    // when start #footer 
                    bottom: function () { 
                        return (this.bottom = $('#footer').outerHeight(true))
                    }
                }
            });

            //Page scrolling
              $('#page-interna-cruzeiro a.nav-link, #page-interna-lua-de-mel a.nav-link').bind('click', function(event) {
                var $anchor = $(this);
                $('html, body').stop().animate({
                    scrollTop: ($( $anchor.attr('href')).offset().top - 90)
                }, 1300);
                event.preventDefault();
              });
        });
    </script>

    <!--carousel-pag-cruzeiro-->
    <script>
        $(document).ready(function() {
            var bigimage = $("#big");
            var thumbs = $("#thumbs");
            var totalslides = 10;
            var syncedSecondary = true;

            bigimage.owlCarousel({
                items : 1,
                slideSpeed : 2000,
                nav: true,
                autoplay: true,
                dots: false,
                loop: true,
                responsiveRefreshRate : 200,
                navText: ['<svg width="50%" height="50%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 3px;stroke: #fff;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>','<svg width="50%" height="50%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 3px;stroke: #fff;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
            }).on('changed.owl.carousel', syncPosition);

            thumbs
                .on('initialized.owl.carousel', function () {
                thumbs.find(".owl-item").eq(0).addClass("current");
            })
            .owlCarousel({
                items : 4,
                dots: true,
                smartSpeed: 200,
                slideSpeed : 500,
                slideBy: totalslides,
                responsiveRefreshRate : 100
            }).on('changed.owl.carousel', syncPosition2);

            function syncPosition(el) {
                //if loop is set to false, then you have to uncomment the next line
                //var current = el.item.index;
        
                //to disable loop, comment this block
                var count = el.item.count-1;
                var current = Math.round(el.item.index - (el.item.count/2) - .5);
    
                if(current < 0) {
                    current = count;
                }
                if(current > count) {
                    current = 0;
                }
                //to this
                thumbs
                    .find(".owl-item")
                    .removeClass("current")
                    .eq(current)
                    .addClass("current");
                    var onscreen = thumbs.find('.owl-item.active').length - 1;
                    var start = thumbs.find('.owl-item.active').first().index();
                    var end = thumbs.find('.owl-item.active').last().index();
    
                    if (current > end) {
                        thumbs.data('owl.carousel').to(current, 100, true);
                    }
                    if (current < start) {
                        thumbs.data('owl.carousel').to(current - onscreen, 100, true);
                    }
            }

            function syncPosition2(el) {
                if(syncedSecondary) {
                    var number = el.item.index;
                    bigimage.data('owl.carousel').to(number, 100, true);
                }
            }

            thumbs.on("click", ".owl-item", function(e){
                e.preventDefault();
                var number = $(this).index();
                bigimage.data('owl.carousel').to(number, 300, true);
            });
        });
    </script>

    <!--VIDEO-MODAL-->
    <script>
        $(document).ready(function() {

            // Gets the video src from the data-src on each button

            var $videoSrc;  
            $('.video-btn').click(function() {
                $videoSrc = $(this).data( "src" );
            });
            console.log($videoSrc);



            // when the modal is opened autoplay it  
            $('#myModal').on('shown.bs.modal', function (e) {

            // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
            $("#video").attr('src',$videoSrc + "?rel=0&showinfo=0&modestbranding=1&autoplay=1" ); 
            })


            // stop playing the youtube video when I close the modal
            $('#myModal').on('hide.bs.modal', function (e) {
                // a poor man's stop video
                $("#video").attr('src',$videoSrc); 
            }) 

        // document ready  
        });
    </script>
<?php wp_footer(); ?>
</body>
</html>

