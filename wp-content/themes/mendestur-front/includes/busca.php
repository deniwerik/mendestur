<!--PESQUISA-->
<section id="pesquisa" class="py-3 bg-laranja">
    <div class="container">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link pl-0 active" id="destino-tab" data-toggle="pill" href="#destino" role="tab" aria-controls="destino" aria-selected="true">Destinos</a>
            </li>
             <li class="nav-item">
                <a class="nav-link" id="aereo-tab" data-toggle="pill" href="#aereo" role="tab" aria-controls="aereo" aria-selected="false">Aéreos</a>
            </li>
            <li class="nav-item ml-auto d-none d-sm-block">
                <a href="<?= get_template_directory_uri()?>/orcamento" class="font-14 nav-link opacity"><u>Não encontrou a viagem que gostaria?</u></a>
            </li>
        </ul>

        <div class="tab-content" id="pills-tabContent">

            <!--TAB-DESTINO-->
            <div class="tab-pane fade show active" id="destino" role="tabpanel" aria-labelledby="destino-tab">
                <div class="row">
                    <!--data-->
                    <div class='col-lg-3'>
                        <label for="exampleInputEmail1" class="mb-0 text-white">Data</label>
                        <div class="form-group">
                                <!-- Datepicker as text field -->         
                                <div class="input-group date">                                    
                                    <div class="input-group-addon" >
                                        <span class="input-group-text" id="basic-addon1">
                                            <svg aria-hidden="true" data-prefix="fal" data-icon="calendar-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-calendar-alt fa-w-14 text-laranja"><path fill="currentColor" d="M400 64h-48V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H128V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h352c8.8 0 16 7.2 16 16v48H32v-48c0-8.8 7.2-16 16-16zm352 384H48c-8.8 0-16-7.2-16-16V192h384v272c0 8.8-7.2 16-16 16zM148 320h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 96h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm192 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12z" class=""></path></svg>
                                        </span>
                                    </div>
                                    <input  type="text" class="form-control" id="data" name="data">
                                </div>
                        </div>
                    </div>

                    <!--Região-->
                    <div class="col-lg-3 mb-3">
                    	<label for="exampleInputEmail1" class="mb-0 text-white">Região</label>
                        <select class="form-control classic" name="regiao" id="regiao">
                            <option selected value="">Escolha uma região</option>
                        </select>
                    </div>

                    <!--cidade-->
                    <div class="col-lg-4">
                    	<label for="exampleInputEmail1" class="mb-0 text-white">Destino</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1"><svg aria-hidden="true" data-prefix="far" data-icon="map-marker-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-map-marker-alt fa-w-12 text-laranja"><path fill="currentColor" d="M192 0C85.903 0 0 86.014 0 192c0 71.117 23.991 93.341 151.271 297.424 18.785 30.119 62.694 30.083 81.457 0C360.075 285.234 384 263.103 384 192 384 85.903 297.986 0 192 0zm0 464C64.576 259.686 48 246.788 48 192c0-79.529 64.471-144 144-144s144 64.471 144 144c0 54.553-15.166 65.425-144 272zm-80-272c0-44.183 35.817-80 80-80s80 35.817 80 80-35.817 80-80 80-80-35.817-80-80z" class=""></path></svg></span>
                            </div>
                            <input list="destinos" name="destinos" id="destiny" class="form-control" placeholder="Escolha seu destino" aria-label="Username" aria-describedby="basic-addon1">
                            <datalist id="destinos" style="width: 800px;">

                            </datalist>
                        </div>
                    </div>

                    
                    <div class="col-lg-2">
                        <!--<a href="#" class="btn btn-lg bg-azul text-white px-5">
                            PROCURAR
                        </a>-->
                        <button class="btn btn-lg bg-azul text-white px-5 mt-3" type="button" id="buscar">PROCURAR</button>
                    </div>
                </div>



            </div>
            <div class="loader">
                <center>
                    <img class="loading-image" src="<?php echo get_template_directory_uri(); ?>/img/icons/preloader.gif" alt="loading..">
                    <p class="text-center font-weight-bold text-laranja">CARREGANDO</p>
                </center>
            </div>



            <!-- <!--TAB-AEREO-->
            <div class="tab-pane fade" id="aereo" role="tabpanel" aria-labelledby="aereo-tab">
                <div class="form-check form-check-inline mb-3">
                    <input class="form-check-input" type="radio" name="trecho" id="inlineRadio1" value="idaevolta" checked>
                    <label class="form-check-label" for="inlineRadio1">Ida e volta</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="trecho" id="inlineRadio2" value="ida">
                    <label class="form-check-label" for="inlineRadio2">Somente ida</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="trecho" id="inlineRadio3" value="multiplos">
                    <label class="form-check-label" for="inlineRadio3">Multiplos trechos</label>
                </div>


                <div class="row">
                    <div class="col-lg-8">
                        <div class="row">
                            <!--Origem e destino-->
                            <div class="col-12 col-sm-12 col-lg-3">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><svg aria-hidden="true" data-prefix="fal" data-icon="map-marker-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-map-marker-alt fa-w-12 fa-lg text-laranja"><path fill="currentColor" d="M192 96c-52.935 0-96 43.065-96 96s43.065 96 96 96 96-43.065 96-96-43.065-96-96-96zm0 160c-35.29 0-64-28.71-64-64s28.71-64 64-64 64 28.71 64 64-28.71 64-64 64zm0-256C85.961 0 0 85.961 0 192c0 77.413 26.97 99.031 172.268 309.67 9.534 13.772 29.929 13.774 39.465 0C357.03 291.031 384 269.413 384 192 384 85.961 298.039 0 192 0zm0 473.931C52.705 272.488 32 256.494 32 192c0-42.738 16.643-82.917 46.863-113.137S149.262 32 192 32s82.917 16.643 113.137 46.863S352 149.262 352 192c0 64.49-20.692 80.47-160 281.931z" class=""></path></svg></span>
                                    </div>
                                    <!--<input type="text" class="form-control" placeholder="Origem" aria-label="Username" aria-describedby="basic-addon1" id="origem" name="origem">-->
                                    <input list="origemaereo" name="origem" id="origem" placeholder="Origem" class="form-control">
                                    <datalist id="origemaereo" style="width: 800px"></datalist>


                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-lg-3">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><svg aria-hidden="true" data-prefix="fal" data-icon="map-marker-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-map-marker-alt fa-w-12 fa-lg text-laranja"><path fill="currentColor" d="M192 96c-52.935 0-96 43.065-96 96s43.065 96 96 96 96-43.065 96-96-43.065-96-96-96zm0 160c-35.29 0-64-28.71-64-64s28.71-64 64-64 64 28.71 64 64-28.71 64-64 64zm0-256C85.961 0 0 85.961 0 192c0 77.413 26.97 99.031 172.268 309.67 9.534 13.772 29.929 13.774 39.465 0C357.03 291.031 384 269.413 384 192 384 85.961 298.039 0 192 0zm0 473.931C52.705 272.488 32 256.494 32 192c0-42.738 16.643-82.917 46.863-113.137S149.262 32 192 32s82.917 16.643 113.137 46.863S352 149.262 352 192c0 64.49-20.692 80.47-160 281.931z" class=""></path></svg></span>
                                    </div>
                                    <!--<input type="text" class="form-control" placeholder="Destino" aria-label="Username" aria-describedby="basic-addon1" id="destinoaereo" name="destino">-->
                                    <input list="aereodestino" name="destino" id="destinoaereo" placeholder="Destino" class="form-control">
                                    <datalist id="aereodestino" style="width: 800px"></datalist>
                                </div>
                            </div>

                            <!--Data-->
                            <div class="col-6 col-sm-6 col-lg-3">
                            		<div class="input-group date" data-provide="datepicker">
                            			<div class="input-group-prepend">
                            				<span class="input-group-text" id="basic-addon2">
                            					<svg aria-hidden="true" data-prefix="fal" data-icon="calendar-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-calendar-alt fa-w-14 text-laranja"><path fill="currentColor" d="M400 64h-48V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H128V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h352c8.8 0 16 7.2 16 16v48H32v-48c0-8.8 7.2-16 16-16zm352 384H48c-8.8 0-16-7.2-16-16V192h384v272c0 8.8-7.2 16-16 16zM148 320h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 96h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm192 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12z" class=""></path></svg>
                            				</span>
                            			</div>
                            			<input type='text' class="form-control" placeholder="Escolha uma data" name="partida" id="partida" />
                            		</div>
                            </div>

                            <div class="col-6 col-sm-6 col-lg-3">
                                <div class="form-group">
                                    <div class="input-group date" data-provide="datepicker">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon3">
                                                <svg aria-hidden="true" data-prefix="fal" data-icon="calendar-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-calendar-alt fa-w-14 text-laranja"><path fill="currentColor" d="M400 64h-48V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H128V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h352c8.8 0 16 7.2 16 16v48H32v-48c0-8.8 7.2-16 16-16zm352 384H48c-8.8 0-16-7.2-16-16V192h384v272c0 8.8-7.2 16-16 16zM148 320h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 96h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm192 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12z" class=""></path></svg>
                                            </span>
                                        </div>
                                        <input type='text' class="form-control" placeholder="Retorno" name="retorno" id="retorno" />
                                    </div>
                                </div>
                            </div>
                        </div><!--col-lg-8-->
                    </div><!--row-->
                    

                    <div class="col-lg-4">
                        <div class="row">
                    <!--Select-->
                            <div class="col-4 col-sm-3 col-lg-4 pr-0">
                                <select class="form-control classic" id="adultos">
                                    <option selected>Adultos</option>
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                </select>
                            </div>
                            <div class="col-4 col-sm-3 col-lg-4 pr-0">
                                <select class="form-control classic" id="criancas">
                                    <option selected>Crianças</option>
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                </select>
                            </div>
                            <div class="col-4 col-sm-3 col-lg-4">
                                <select class="form-control classic" id="bebes">
                                    <option selected>Bebês</option>
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                </select>
                            </div>
                        </div><!--row-->
                    </div><!--col-lg-4-->
                </div>
                <div class="text-right">
                    <button type="button" class="btn btn-lg bg-azul text-white px-5" id="buscaraereo">PROCURAR</button>
                    <!--<a href="#" class="btn btn-lg bg-azul text-white px-5">
                        PROCURAR
                    </a>-->
                </div>
            </div><!--tab-aereo-->

        </div><!--tab-content-->
        <li class="nav-item ml-auto d-block d-sm-none">
            <a href="page-orcamento.php" class="font-14 nav-link opacity"><u>Não encontrou a viagem que gostaria?</u></a>
        </li>
    </div><!--container-->

</section>