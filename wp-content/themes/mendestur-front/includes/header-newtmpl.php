<!Doctype html>
<html lang="pt-BR">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112763572-2"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-112763572-2');
</script>

<!-- Hotjar Tracking Code for http://mendestur.com.br/2018/ -->
<script>
(function(h,o,t,j,a,r){
h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
h._hjSettings={hjid:872840,hjsv:6};
a=o.getElementsByTagName('head')[0];
r=o.createElement('script');r.async=1;
r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
a.appendChild(r);
})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mendes Tur</title>

    <!--normalize-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.css" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css" type="text/css" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!--FontAwesome-->
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>

    <!--Google-fonts-->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,700i" rel="stylesheet">

    <!--DatePicker-->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.standalone.css">

    <!--OwlCarosel-->
    <!--<link rel="stylesheet" href="<?/*= get_stylesheet_directory_uri();*/?>/inc/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="<?/*= get_stylesheet_directory_uri();*/?>/inc/owlcarousel/owl.theme.default.min.css">-->

    <!--css-->
    <!--<link rel="stylesheet" href="<?/*= get_stylesheet_directory_uri();*/?>/style.css">-->
    <script src="<?= get_template_directory_uri();?>/scripts/jquery-3.3.1.js" type="application/javascript"></script>
    <?php wp_head(); ?>
</head>

<body data-spy="scroll" data-target="#navbar-example" data-offset="20" id="page-top">

<header class="bg-light pt-3">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" class="img-fluid w-50">
            </div>
            <div class="col-lg-10">
                <p class="float-right px-2">Miramar Shopping: <span class="text-azul mr-3">(13) 3208-9000</span> Praiamar Shopping: <span class="text-azul">(13) 3279-9000</span></p>
            </div>
            <span class="border-header"></span>
        </div>
    </div>
</header>

<nav id="mainNav" class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand d-none" href="<?php echo get_permalink( get_page_by_path( 'home' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" class="img-fluid w-25"></a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <!--<span class="navbar-toggler-icon"></span>-->
            <span></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_template_directory_uri(); ?>/index.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_template_directory_uri(); ?>/page-aereo.php">Aéreos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_template_directory_uri(); ?>/page-cruzeiro.php">Cruzeiros</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_template_directory_uri(); ?>/page-destino.php">Destinos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_template_directory_uri(); ?>/page-orcamento.php">Orçamento</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_template_directory_uri(); ?>/page-lua-de-mel.php">Lua de Mel</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://www.confidencecambio.com.br/conversor-moedas/">Câmbio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_template_directory_uri(); ?>/page-quem-somos.php">Quem somos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo get_template_directory_uri(); ?>/page-contato.php">Contato</a>
                </li>
            </ul>
        </div>
    </div>
</nav>