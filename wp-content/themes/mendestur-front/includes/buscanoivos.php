<div id="pesquisa" class="p-4 bg-laranja">
	<h4 class="text-center text-white font-weight-bold text-uppercase">Você foi convidado?</h4>
	<h5 class="text-azul text-center border-bottom pb-3">Presenteie os noivos</h5>

	<p class="text-white">Procure pelo nome da noiva, do noivo ou data do casamento</p>

	<div class="row">
		<!--Origem e destino-->
		<div class="col-12 col-sm-12 col-lg-8 pb-2">
			<input type="text" class="form-control" placeholder="Noiva ou Noivo" id="noivos">
		</div

		<!--Data-->
		<div class="col-12 col-sm-12 col-lg-4 pb-2">
			<div class="form-group">
				<div class="input-group date" data-provide="datepicker">
					<div class="input-group-prepend">
								<span class="input-group-text" id="basic-addon1">
									<svg aria-hidden="true" data-prefix="fal" data-icon="calendar-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-calendar-alt fa-w-14 text-laranja"><path fill="currentColor" d="M400 64h-48V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H128V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h352c8.8 0 16 7.2 16 16v48H32v-48c0-8.8 7.2-16 16-16zm352 384H48c-8.8 0-16-7.2-16-16V192h384v272c0 8.8-7.2 16-16 16zM148 320h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 96h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm192 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12z" class=""></path></svg>
								</span>
					</div>
					<input type='text' class="form-control" placeholder="Data da cerimonia" id="data-lua"/>
				</div>
			</div>
		</div>
	</div>
	<div class="text-right">
		<button href="#" class="btn btn-lg bg-azul text-white px-4" id="procurarlua">
			PROCURAR
		</button>
	</div>
</div>