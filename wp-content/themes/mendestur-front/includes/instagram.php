<section id="instagram" class="pb-5">
    <div class="container">
        <div class="input-group justify-content-center mt-5">
            <a href="#">
                <i class="fab fa-instagram fa-3x mr-2"></i>
                <h2 class="align-self-center text-laranja text-uppercase">#mendestur</h2>
            </a>
        </div>
    </div>
</section>