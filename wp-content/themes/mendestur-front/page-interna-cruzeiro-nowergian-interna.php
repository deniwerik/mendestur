<?php
// Template Name: Interna Cruzeiro Nowergian Interna

get_header('newtmpl');
?>
<!-- Cruzeiro Interna Cruzeiro Nowergian Interna -->

	<main>
		<section id="interna-cruzeiro-header">
			<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/header-cruzeiro-msc.jpg" class="img-fluid w-100 d-block">
			<div class="hero">
				<h1 class="text-center text-white text-uppercase bottom-line-header line-center">Norwegian Cruise Line - Escape</h1>
			</div>
		</section>

		<section id="page-interna-cruzeiro" class="pb-2">
			<div class="container">
				<ul class="nav mb-4">
					<li class="nav-item">
						<a class="nav-link active" href="#info">Informações</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#solicitar">Solicitar</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#navio">Navio</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#condicoes">Condições Gerais</a>
					</li>
				</ul>

				<div class="row mb-4">
					<div class="col-lg-8 bg-cinza-claro py-3 px-4">
						<h1 class="font-weigth-bold text-azul mb-0">6 noites - Caribe</h1>
						<p class="mb-0"><strong>Saída:</strong> Miami - 14/04/2018</p>
					</div>
					<div class="col-lg-4 bg-azul">
						<div class="row py-2">
							<div class="col-lg-6 align-self-center border-right">
								<p class="font-12 text-white mb-0">Valor Total</p>
								<h2 class="font-weigth-bold text-laranja">R$ 2.440,47</h2>
							</div>
							<div class="col-lg-6 align-self-center ">
								<p class="font-12 text-white mb-0">Preço por pessoa em cabine dupla interna garantida. Tarifa aérea não inclusa.</p>
							</div>
						</div>
					</div>
				</div>

				<p class="text-uppercase font-weight-bold heading">
					Informações extras
					<span class="divider-left"></span>
				</p>

				<div class="row">
					<div class="col-lg-8">
						<p class="mb-0">6 noites - Caribe</p>
						<p class="mb-0"><strong>Saída:</strong> Miami - 14/04/2018</p>
						<p class="mb-0"><strong>Embarque:</strong> Miami, Flórida (EUA) – Nassau (Bahamas); Ocho Rios (Jamaica); George Town (Cayman).</p>
						<p class="mb-0"><strong>Desembarque:</strong> Miami, Flórida (EUA)</p>
						<p class="mb-0 font-weight-bold">Cabine Interna Garantida</p>
						<p class="font-weight-bold text-laranja">TODAS AS TAXAS INCLUÍDAS!</p>
					</div>
					<div class="col-lg-4 px-5">
						<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/logo-ncl.png" class="img-fluid">
					</div>
				</div>
			</div>
		</section>

		<section id="solicitar">
			<div class="container">
				<div class="form-orcamento bg-cinza p-4" id="solicitar">
					<h5 class="font-weight-bold">Ficou interessado?</h5>
					<p>Preencha o formulário abaixo, e solicite o cruzeiro que deseja.</p>

					<form>
						<p class="border-bottom">Dados da Viagem</p>
						<p class="mb-0"><strong>Viagem:</strong> 6 noites - Caribe</p>
						<p class="mb-0"><strong>Saída:</strong> Miami - 14/04/2018</p>
						<p class="mb-0"><strong>Navio:</strong> NCL Escape</p>
						<div class="row">
							<div class="col-6 col-sm-6 col-lg-2 pb-2">
								<select class="form-control classic" id="exampleFormControlSelect1">
									<option selected>Adultos</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>
							<div class="col-6 col-sm-6 col-lg-2 pb-2">
								<select class="form-control classic" id="exampleFormControlSelect1">
									<option selected>Crianças</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>
						</div>
						<p class="border-bottom">Dados Pessoais</p>
						<div class="row">
							<div class="col-12 col-sm-12 col-lg-4 pb-2">
								<input type="text" class="form-control" placeholder="Nome">
							</div>
							<div class="col-12 col-sm-12 col-lg-4 pb-2">
								<input type="text" class="form-control" placeholder="Telefone">
							</div>
							<div class="col-12 col-sm-12 col-lg-4 pb-2">
								<input type="text" class="form-control" placeholder="Email">
							</div>
						</div>
						<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" placeholder="Informações complementares"></textarea>
						<button type="submit" class="btn bg-laranja text-white px-5 mt-3 mb-2">CONFIRMAR INTERESSE</button>
					</form>
				</div>
			</div>
		</section>

		<section id="navio" class="py-5">
			<div class="container">
				<p class="text-uppercase font-weight-bold heading">
					Navio
					<span class="divider-left"></span>
				</p>
				<h2 class="text-azul">NCL ESCAPE</h2>
				<div class="row">
					<div class="col-lg-6">
						<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/escape-02.jpg" class="img-fluid mb-3">
						<p>Tantas maneiras diferentes de se divertir, novas experiências e atividades emocionantes para descobrir. Isto é um cruzeiro no Norwegian Escape. Encare o desafio e desça o Aqua Racer a toda velocidade em nosso Aqua Park ou teste seus limites de adrenalina na Prancha no Complexo Esportivo. Não importa qual tipo de emoção você busca, sempre tem alguma coisa para fazer seu coração bater acelerado.</p>
					</div>
					<div class="col-lg-6">
						<!--carousel-->
						<div id="sync1" class="owl-carousel owl-theme">
							<div class="item">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/01.jpg" class="img-fluid">
								<div class="carousel-caption">
									<p class="font-14 text-white mb-0 text-uppercase font-weight-bold">getaway</p>
								</div>
							</div>
							<div class="item">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc-carrousel/02.jpg" class="img-fluid">
							</div>
							<div class="item">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc-carrousel/03.jpg" class="img-fluid">
							</div>
							<div class="item">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc-carrousel/04.jpg" class="img-fluid">
							</div>
							<div class="item">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc-carrousel/01.jpg" class="img-fluid">
							</div>
						</div>
						
						<div id="sync2" class="owl-carousel owl-theme">
							<div class="item">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/01.jpg" class="img-fluid">
							</div>
							<div class="item">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc-carrousel/02.jpg" class="img-fluid">
							</div>
							<div class="item">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc-carrousel/03.jpg" class="img-fluid">
							</div>
							<div class="item">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc-carrousel/04.jpg" class="img-fluid">
							</div>
							<div class="item">
								<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/msc-carrousel/01.jpg" class="img-fluid">
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</section>

		<section id="condicoes">
			<div class="container">
				<p class="text-uppercase font-weight-bold heading">
					CONDIÇÕES GERAIS
					<span class="divider-left"></span>
				</p>
				<p class="font-12">Preço por pessoa sujeito á disponibilidade, incluindo cruzeiro marítimo em cabine dupla interna. Tarifa exclusivamente parte marítima. Cruzeiro por pessoa em cabine interna dupla com taxas portuárias e de serviços. Não inclui tarifa aérea.Tarifa sujeita a alterações e disponibilidade. Tarifa em Reais sujeita a variação cambial. Entrada de 25% (dinheiro ou cheque) e parcelamento em 9X sem juros para pagamentos em cheque (sujeito aprovação pela TCN) ou em 9X sem juros no cartão de crédito das bandeiras Visa, Amex, Dinners e Mastercard. Preços e condições promocionais à disponibilidade em cada saída e alteração sem aviso prévio. Tarifa exclusiva para vendas individuais, não válida para Grupos e Grupos de Adesão. Imagens meramente ilustrativas.</p>
			</div>
		</section>

	        <?php
        require_once (TEMPLATEPATH."/includes/instagram.php");
        ?>

    </main>

<?php get_footer('newtmpl'); ?>