<?php
// Template Name: Interna Destino
session_start();
require_once ("classes/nusoap.php");

$id = $_GET["id"];
$regiao = $_GET["regiao"];

$cliente = new nusoap_client("https://ancoradouro.com.br/mondiale/webservice/service.php?wsdl");

$parametros = array('login'=>'mendestur','senha'=>'@1D2#$65par#');

$resultado = $cliente->call('getUser',$parametros);

$chave = $resultado['chave'];

$parametros2 = array($chave, $regiao);

$resultado_pacotes = $cliente->call("getPacotes", $parametros2);

foreach ($resultado_pacotes as $value){
    if ($value["id"] === $id){
        $pacote = $value;
        break;
    }
}


/*if (isset($_SESSION["destinos_limitados"])){
    foreach ($_SESSION["destinos_limitados"] as $key => $value){
        if ($id === $value["id"]){
            $pacote = $value;
        }
    }
}

if (isset($_SESSION["pacote"])){
    $pacote = $_SESSION["pacote"];
} else {
    if (isset($_SESSION["pacotes"])){
	    foreach ($_SESSION["pacotes"] as $key => $value){
		    if ($id === $value["id"]){
			    $pacote = $value;
		    }
	    }
    }
}*/

if (isset($_SESSION["pacotes"])){
	$pacotes = $_SESSION["pacotes"];
} else if (isset($_SESSION["destinos_limitados"])){
    $pacotes = $_SESSION["destinos_limitados"];
}



get_header('newtmpl');
?>

<!-- Interna Destino -->
	<main>

		<!--PESQUISA-->
        <?php
        require_once (TEMPLATEPATH."/includes/busca.php");
        ?>
		
		<section id="page-interna-destinos" class="py-5">
			<div class="container">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb bg-white font-14">
						<li class="breadcrumb-item"><a href="<?php echo  get_site_url(); ?>">Home</a></li>
						<li class="breadcrumb-item" aria-current="page"><a href="<?php echo get_permalink( get_page_by_path( 'destino' ) ); ?>">Destinos</a></li>
						<li class="breadcrumb-item active text-laranja" aria-current="page"><?=$pacote["titulo"];?></li>
					</ol>
				</nav>

				<h1 class="text-laranja pb-3"><?=$pacote["titulo"];?></h1>

				<div class="row">
					<div class="col-lg-8">
						<img src="<?=$pacote['imagem'];?>" class="img-fluid d-block w-100">

						<ul class="nav nav-pills mb-3 mt-4 border-branco" id="pills-tab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active text-uppercase" id="info-tab" data-toggle="pill" href="#informacoes" role="tab" aria-controls="informacoes" aria-selected="true">Informações</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-uppercase" id="incluso-tab" data-toggle="pill" href="#incluso" role="tab" aria-controls="incluso" aria-selected="false">Incluso</a>
							</li>
							<li class="nav-item">
								<a class="nav-link text-uppercase" id="condicoes-tab" data-toggle="pill" href="#condicoes" role="tab" aria-controls="condicoes" aria-selected="false">Condições Gerais</a>
							</li>
						</ul>
						<div class="tab-content mb-5" id="pills-tabContent">
							<!--Informações-->
							<div class="tab-pane fade show active" id="informacoes" role="tabpanel" aria-labelledby="pills-home-tab">
                                <?php
/*                                    if (!empty($pacote['descricao'])){
                                        echo ("<p class='mb-0'>Descrição: <strong>".$pacote['descricao']."</strong></p>");
                                    }
                                */?>
								<!--<p class="mb-0">Local: <strong><?/*=$pacote['titulo'];*/?></strong></p>-->
                                <?php
                                if (!empty($pacote["saida"])){
                                ?>
								<p class="mb-0">Saída/Embarque: <strong><?=$pacote['saida'];?></strong></p>
                                <?php
                                }
                                ?>
								<p class="mb-0">Dias: <strong><?=$pacote['dias'];?> dias</strong></p>
								<!--<p class="mb-0">Hospedagem: <strong></strong></p>-->
                                <?php
                                if (!empty($pacote['visitando'])){
                                    echo("<p class='mb-0'>Visitando: <strong>".$pacote['visitando']."</strong></p>");
                                }

                                ?>

							</div>

                            <!--Incluso-->
							<div class="tab-pane fade" id="incluso" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <?php
                                $inclui = $pacote["inclui"];
                                $inclui = str_replace(";","<br>",$inclui);
                                echo ($inclui);

                                if (!empty($pacote["ninclui"])){
                                    echo("Não Inclui: " . $pacote["ninclui"]);
                                }
                                ?>
                            </div>

                            <!--Condições Gerais-->
							<div class="tab-pane fade" id="condicoes" role="tabpanel" aria-labelledby="pills-contact-tab">
                                <?=$pacote['importante'];?>
                            </div>
						</div>

						<div class="border-top"></div>
						<div class="row mt-3">
							<div class="col-lg-6">
								<img src="<?php echo get_template_directory_uri(); ?>/img/destinos/ancoradouro.png" class="img-fluid w-25">
							</div>
						</div>
					</div>
					<div class="col-lg-4 bg-cinza p-5 form-interesse">
<!--						<p class="text-laranja font-weight-bold mb-1">12x <span class="h4 font-weight-bold">-->
<!--                                --><?php
//                                $valor = floatval($pacote["valor"]) / 12;
//                                $valor = round($valor,2);
//                                $valor = str_replace(".",",",$valor);
//                                if (strlen($valor) === 2) {
//                                    $valor .= ",00";
//                                }
//                                echo($valor);
//                                ?>
<!--                            </span>-->
<!--                        </p>-->
						<p class="text-azul font-weight-bold border-bottom pb-3">Total: <?php
                            echo ($pacote["moeda"]);
                            $valor = $pacote["valor"];
							if ($valor === "0"){
								$valor = "Consulte-nos";
							} else {
								$valor .= ",00";
							}
							echo $valor;
							the_field('importante')


                            ?>
                        </p>
						<p>Caso tenha gostado desse pacote de viagem, preencha o formulário abaixo, nossa equipe logo entrará em contato com você, para maiores informações.</p>
						<form>
							<input type="name" class="form-control mb-3" id="exampleInputName" aria-describedby="emailHelp" placeholder="Nome">
							<input type="phone" class="form-control mb-3" id="exampleInputPhone" aria-describedby="emailHelp" placeholder="Telefone">
							<input type="email" class="form-control mb-3" id="exampleInputEmai1" aria-describedby="emailHelp" placeholder="Email">
							<button type="submit" class="btn bg-laranja mb-2 text-white px-4">Enviar</button>
                            <p>Disponibilidade e preço sujeito alteração sem prévio aviso.</p>
						</form>
					</div>
				</div>
			</div>
		</section>

		<section id="page-destinos" class="py-5 bg-cinza-claro">
			<div class="container">
				<div class="input-group">
					<h3 class="text-uppercase text-azul align-self-center mb-0 mr-3">VEJA MAIS DESTINOS</h3>
					<a href="<?php echo get_permalink( get_page_by_path( 'destino' ) ); ?>" class="btn btn-lg bg-azul text-white px-4">
						VER TODOS
					</a>
				</div>
				<div class="row pt-5">
                    <?php
                    if (isset($pacotes)) {
	                    foreach ( $pacotes as $key => $value ) {
		                    if ( $value["id"] !== $pacote["id"] ) {
			                    ?>
                                <div class="col-lg-3 mb-4">
						<div class="card">
							<img class="img-fluid card-img-top" src="<?=$value['imagem'];?>" alt="Card image cap">
							<div class="card-body">
								<h5 class="font-weight-bold text-laranja mb-0"><?php
                                    $titulo = $value["titulo"];
									$titulo = mb_strtolower($titulo,'UTF-8');
									$titulo = mb_convert_case($titulo, MB_CASE_TITLE,"UTF-8");
									if (strlen($titulo) > 24){
										$titulo = mb_substr($titulo,0,21);

										$titulo .= "...";
									}
									echo $titulo;
                                    ?></h5>
								<p class="font-weight-bold mb-0"><?=$value['dias'];?> dias</p>
								<div class="border my-2"></div>
								<!--<p class="font-14 mb-0 mt-1">
                                    <?php
/*                                    $parc = $pacote["valor"] / 12;
                                    $parc = round($parc,2);
                                    $parc = str_replace(".",",",$parc);
                                    echo ("12x de " . $pacote['moeda'] . " " . $parc);
                                    */?>
                                </p>-->
                                <p class="text-azul font-weight-bold mb-0"><?/*=$pacote["moeda"];*/?>
                                    <span class="h4 font-weight-bold">
                                        <?php
                                            $valor = $value["valor"];
                                            if ($valor === "0"){
                                                $valor = "Consulte-nos";
                                            } else {
                                                $valor .= ",00";
                                            }
                                            echo $valor;
                                        ?>
                                    </span>
                                </p>

                                <div class="row">
                                    <div class="col-4 col-sm-4 col-lg-4 p-0 align-self-center">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/destinos/ancoradouro.png" class="img-fluid">
                                    </div>
                                    <div class="col-8 col-sm-8  col-lg-8">
                                         <button class="btn btn-lg bg-laranja text-white px-4 detalhes-btn"
                                        data-regiao="<?=$value["regiao"];?>" data-pacote="<?=$value["id"];?>">
                                        DETALHES
                                        </button>
                                    </div>
                                </div>
							</div>
						</div>
					</div>
			                    <?php
		                    }
	                    }
                    }
                    ?>

				</div>
			</div>
		</section>

	</main>

<?php get_footer('newtmpl'); ?>