$(document).ready(function () {
    //$("#continente,#regiao,#destino,#buscar").prop("disabled", true);
    $("#buscar").prop("disabled", true);
});

$("#destiny").blur(function () {
    var destino = $("#destiny").val();
    destino = destino.trim();
    if ( destino !== "" ){
        $("#buscar").prop("disabled", false);
    }
});


$("#data").change(function () {

    var data = $("#data").val();

    var url = "/wp-content/themes/mendestur-front/webservices/busca_continentes.php";


    var url_atual = window.location.href;



    $.ajax({
        type:"POST",
        url:url,
        data: {
            data: data
        },
        beforeSend: function(){
            $('.loader').show();
        },
        success: function (data) {
            /*alert(typeof data);
            data = JSON.stringify(data);
            alert(typeof data);*/
            $.each(data,function () {
                var paises = this.paises;

                var continente = "<option value='"+this.id+"'>"+this.nome+"</option>";
                //$("#continente").append(continente);
                //console.log("continente:" + this.nome);

                for (var pais in paises){
                    //console.log(paises[pais].nome);

                    if (paises[pais].pacotes){
                        $("#continente").prop("disabled",false);
                        $("#regiao").prop("disabled",false);
                        $("#destino").prop("disabled",false);
                        $("#buscar").prop("disabled", false);


                        var country = "<option value='"+paises[pais].id+"'>"+pais+"</option>";
                        $("#regiao").append(country);
                        //console.log("pais: "+pais);

                        for (var pacote in paises[pais].pacotes){
                            var pack = "<option data-regiao='"+paises[pais].id+"' data-id='"+paises[pais].pacotes[pacote].id+"' value='"+paises[pais].pacotes[pacote].titulo+"'></option>";
                            $("#destinos").append(pack);
                            //console.log("destino: " +paises[pais].pacotes[pacote].titulo);
                        }
                        /*$(paises[pais].pacotes).each(function () {
                           console.log("destino:"+this.titulo);
                        });*/
                    }
                }
                /*$.each(this, function (key,value) {
                    console.log(key, value);
                        /!*for (var counter in jsonData.counters) {
                            console.log(jsonData.counters[counter].counter_name);
                        }*!/
                });*/
            });

        },
        complete: function(){
            $('.loader').hide();
            $("#buscar").prop("disabled", false);
        }
    });
    //alert(data);
    $(".datepicker").hide();
});

var hide = [];
var show = [];
var check = true;
$("#regiao").change(function () {

    var regiao = $("#regiao").val();
    regiao = parseInt(regiao);

    if (hide.length > 0){
        for(var i = 0; i < hide.length; i++){
            $("#destinos").append(hide[i][0]);
        }
        /*$.each(hide, function (key,value) {
        //for (var option in hide){
           $("#destino").append(this);
        });*/
        hide = [];
        check = false;
    }

    $("#destinos option").each(function () {
        //alert($(this).val());

        //alert($(this).data("regiao"));
        //alert(typeof ($(this).data("regiao")));
        //alert(regiao);
        //alert(typeof regiao);


        if ($(this).data("regiao") != regiao){
            hide.push($(this).detach());
            //alert($(this).val());
        } else {
            show.push($(this));
            $(this).remove();

        }
    });

    //$(this).detach();

    $(show).each(function () {
        if ($(this).data("regiao") == regiao){
            $("#destinos").append($(this));
        }
    });
    $("input[name='destino']").val("");

});

$("#buscar").click(function () {
    var url = "/mendestur-front/wp-content/themes/mendestur-front/webservices/busca_pacote.php";

    var val = $("#destiny").val();
    var id = $("#destinos option").filter(function () {
        return this.value == val;
    }).data("id");

    var regiao = $("#destinos option").filter(function () {
        return this.value == val;
    }).data("regiao");

    var id2 = id ? id : "no match";

    if (regiao == null || regiao === ""){
        var regiao2 = $("#regiao").val();
    } else {
        var regiao2 = regiao ? regiao : "no match";
    }



    //alert(regiao);

    //alert(id2);

    $.ajax({
        type:"POST",
        url:url,
        data: {
            id_regiao: regiao2,
            id_pacote: id2
        },
        success: function (data) {
            if (data.error === "false"){
                location.href="./destinos-encontrados";
            } else {
                alert("erro ao salvar pacote");
            }
        }
    });
});


$(".detalhes-btn").each(function () {
    $(this).click(function () {
        var regiao = $(this).data("regiao");
        var pacote = $(this).data("pacote");

        var url = "/mendestur-front/wp-content/themes/mendestur-front/webservices/guarda_ids.php";

        $.ajax({
            type: "POST",
            url: url,
            data: {
                id_pacote: pacote,
                id_regiao: regiao
            },
            success: function (data) {
                if (data.erro === "false"){
                    location.href="./interna-destino?id="+data.id+"&regiao="+data.regiao;
                }
            }
        });

        /*alert(regiao);
        alert(pacote);*/
    }) ;
});

$("#data-orc,#data-lua").change(function () {
    $(".datepicker").hide();
});

$("#procurarlua").click(function () {

    var noivos = $("#noivos").val();
    var data = $("#data-lua").val();


    var url = "/mendestur-front/wp-content/themes/mendestur-front/webservices/guardalua.php";

    $.ajax({
        type: "POST",
        url: url,
        data: {
            noivos: noivos,
            data: data
        },
        success: function (data) {
            if (data.erro === "false"){
                location.href="../busca-lua-de-mel/";
            }
        }
    });

});

$("#partida").change(function () {
    $("#partida + .datepicker").hide();
});

$("#retorno").change(function () {
    $("#retorno + .datepicker").hide();
});

$(document).ready(function() {
    var bigimage = $("#big");
    var thumbs = $("#thumbs");
    var totalslides = 10;
    var syncedSecondary = true;

    bigimage.owlCarousel({
        items : 1,
        slideSpeed : 2000,
        nav: true,
        autoplay: true,
        dots: false,
        loop: true,
        responsiveRefreshRate : 200,
        navText: ['<i class="fa fa-arrow-left" aria-hidden="true"></i>', '<i class="fa fa-arrow-right" aria-hidden="true"></i>'],
    }).on('changed.owl.carousel', syncPosition);

    thumbs
        .on('initialized.owl.carousel', function () {
            thumbs.find(".owl-item").eq(0).addClass("current");
        })
        .owlCarousel({
            items : 4,
            dots: true,
            nav: true,
            navText: ['<i class="fa fa-arrow-left" aria-hidden="true"></i>', '<i class="fa fa-arrow-right" aria-hidden="true"></i>'],
            smartSpeed: 200,
            slideSpeed : 500,
            slideBy: totalslides,
            responsiveRefreshRate : 100
        }).on('changed.owl.carousel', syncPosition2);

    function syncPosition(el) {
        //if loop is set to false, then you have to uncomment the next line
        //var current = el.item.index;

        //to disable loop, comment this block
        var count = el.item.count-1;
        var current = Math.round(el.item.index - (el.item.count/2) - .5);

        if(current < 0) {
            current = count;
        }
        if(current > count) {
            current = 0;
        }
        //to this
        thumbs
            .find(".owl-item")
            .removeClass("current")
            .eq(current)
            .addClass("current");
        var onscreen = thumbs.find('.owl-item.active').length - 1;
        var start = thumbs.find('.owl-item.active').first().index();
        var end = thumbs.find('.owl-item.active').last().index();

        if (current > end) {
            thumbs.data('owl.carousel').to(current, 100, true);
        }
        if (current < start) {
            thumbs.data('owl.carousel').to(current - onscreen, 100, true);
        }
    }

    function syncPosition2(el) {
        if(syncedSecondary) {
            var number = el.item.index;
            bigimage.data('owl.carousel').to(number, 100, true);
        }
    }

    thumbs.on("click", ".owl-item", function(e){
        e.preventDefault();
        var number = $(this).index();
        bigimage.data('owl.carousel').to(number, 300, true);
    });
});

$("#origem").keyup(function () {
    if ($("#origem").val().length > 2){


        var origem = $("#origem").val();
        /*alert(origem);*/


        $.ajax({
            type:"POST",
            url: "https://ancd.efacilplus.com.br/OnlineTravelFrameMVC/(S(d35kr1tfylfivpttbajpxj0b))/Aereo/AutoCompleteAereo/?LojaChave=TG9qYSBPbkxpbmUgTWVuZGVzVHVyIC0gTWF0cml6ICgyMDQ2KSAtIDIwLzAzLzIwMTggMTE6NDI6Mjk=&q="+origem,
            success: function (data) {
                //alert(data);
                var arr = data.split(";");
                //alert(typeof arr);
                var arr2 = [];
                $.each(arr, function (key, value) {
                    var fim = value.length;
                    var inicio = fim - 4;
                    var cod = value.substring(inicio,fim - 1);
                    //alert(key + value);
                    //var pack = "<option data-regiao='"+paises[pais].id+"' data-id='"+paises[pais].pacotes[pacote].id+"' value='"+paises[pais].pacotes[pacote].titulo+"'></option>";
                     var origem2 = "<option data-iata='"+cod+"' value='"+value+"'></option>";
                     arr2.push(origem2);
                    
                });

                $.each(arr2, function (key,value) {
                    $("#origemaereo").append(value);
                });
            }
        });
    }
});

$("#destinoaereo").keyup(function () {
    if ($("#destinoaereo").val().length > 2){

        var destino = $("#destinoaereo").val();

        $.ajax({
            type:"POST",
            url: "https://ancd.efacilplus.com.br/OnlineTravelFrameMVC/(S(d35kr1tfylfivpttbajpxj0b))/Aereo/AutoCompleteAereo/?LojaChave=TG9qYSBPbkxpbmUgTWVuZGVzVHVyIC0gTWF0cml6ICgyMDQ2KSAtIDIwLzAzLzIwMTggMTE6NDI6Mjk=&q="+destino,
            success: function (data) {

                //alert(data);
                var arr = data.split(";");
                //alert(typeof arr);
                var arr2 = [];
                $.each(arr, function (key, value) {
                    var fim = value.length;
                    var inicio = fim - 4;
                    var cod = value.substring(inicio,fim - 1);
                    //alert(key + value);
                    //var pack = "<option data-regiao='"+paises[pais].id+"' data-id='"+paises[pais].pacotes[pacote].id+"' value='"+paises[pais].pacotes[pacote].titulo+"'></option>";
                    var destino2 = "<option data-iata='"+cod+"' value='"+value+"'></option>";
                    arr2.push(destino2);


                });

                $.each(arr2, function (key, value) {
                    $("#aereodestino").append(value);
                });
            }
        });
    }
});

$("#buscaraereo").click(function () {
    var trecho = $("input[name='trecho']:checked").val();
    /*var origem = $("#origem").val();
    var destino = $("#destinoaereo").val();*/

    var origem = $("#origem").val();
    var iata_origem = $("#origemaereo option").filter(function () {
      return this.value === origem;
    }).data("iata");

    var cod_iata_origem = iata_origem ? iata_origem : "no match";

    var destino = $("#destinoaereo").val();
    var iata_destino = $("#aereodestino option").filter(function () {
        return this.value === destino;
    }).data("iata");

    var cod_iata_destino = iata_destino ? iata_destino : "no match";



    /*var val = $("#destiny").val();
    var id = $("#destinos option").filter(function () {
        return this.value == val;
    }).data("id");

    var regiao = $("#destinos option").filter(function () {
        return this.value == val;
    }).data("regiao");

    var id2 = id ? id : "no match";

    if (regiao == null || regiao === ""){
        var regiao2 = $("#regiao").val();
    } else {
        var regiao2 = regiao ? regiao : "no match";
    }*/



    var dataPartida = $("#partida").val();
    var dataRetorno = $("#retorno").val();
    var adultos = $("#adultos").val();
    var criancas = $("#criancas").val();
    var bebes = $("#bebes").val();


    /*var url = "https://ws.efacilplus.com.br/WCF/wcfTravellink/Aereo.svc/Autenticar";

    $.ajax({
        type: "POST",
        url: url,
        data: {
            login: "reservas.mendes",
            senha: "7I66tjaN"
        },
        success: function (data) {
            alert(data);
            alert(typeof data);
        }
    });*/

    var url = "/wp-content/themes/mendestur-front/webservices/aereo-disponibilidade.php";
    $.ajax({
        type: "POST",
        url: url,
        data: {
            trecho: trecho,
            origem: cod_iata_origem,
            destino: cod_iata_destino,
            dataPartida: dataPartida,
            dataRetorno: dataRetorno,
            adultos: adultos,
            criancas: criancas,
            bebes: bebes
        },
        success: function (data) {
            alert(data);
        }

    });

});