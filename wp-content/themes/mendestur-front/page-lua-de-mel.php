<?php
// Template Name: Lua de mel
get_header('newtmpl');
?>


	<main>
		<section id="lua-de-mel-header" class="py-5">
			<div class="container">
				<h1 class="text-center text-white text-uppercase bottom-line-header line-center">LUA DE MEL</h1>
			</div>
		</section>

		<!--PESQUISA-->
		<?php
		    require_once (TEMPLATEPATH."/includes/buscanoivos.php");
		?>

		<section id="page-lua-de-mel" class="py-5">
			<div class="container">
				<img src="<?php echo get_template_directory_uri(); ?>/img/lua-de-mel/img-lua-de-mel.jpg" class="img-fluid mb-4">
				<h3 class="text-azul font-weight-bold text-center">Faça sua Lista de Lua de Mel com a Mendes Tur</h3>
				<h5 class="text-center">Ganhe sua viagem de Lua de Mel de presente. Para isso, basta fazer uma Lista de Casamento na Mendes Tur.</h5>

				<div class="row justify-content-center py-5">
					<div class="col-lg-3">
						<div class="border p-5 mb-3">
							<img src="<?php echo get_template_directory_uri(); ?>/img/icons/restaurant.svg" class="img-fluid w-50 m-auto d-block">
						</div>
						<p class="text-center">Os convidados podem comprar parte da viagem </p>
					</div>
					<div class="col-lg-1 align-self-center">
						<p class="text-center text-laranja">X</p>
					</div>
					<div class="col-lg-3">
						<div class="border p-5 mb-3">
							<img src="<?php echo get_template_directory_uri(); ?>/img/icons/travel.svg" class="img-fluid w-50 m-auto d-block">
						</div>
						<p class="text-center">ou até mesmo, oferecer a viagem inteira de presente.</p>
					</div>
				</div>
			</div>
		</section>

		<section id="como-funciona" class="py-5 bg-cinza-claro">
			<div class="container">
				<h2 class="text-center text-azul font-weight-bold">Como Funciona</h2>
				

				<img src="<?php echo get_template_directory_uri(); ?>/img/lua-de-mel/04.png" class="img-fluid d-none d-sm-block">
				<div class="justify-content-center mb-5 card-deck">
						<div class="card">
							<img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/lua-de-mel/01.png" alt="Card image cap">
							<div class="card-body">
								<p class="card-text font-14">Os noivos podem cadastrar sua Lista de Casamento diretamente nas agências Mendes Tur, ou através do <a href="#" class="text-laranja">contato</a>, e escolhem a viagem de Lua de Mel de seus sonhos. </p>
							</div>
						</div>
						<div class="card">
							<img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/lua-de-mel/02.png" alt="Card image cap">
							<div class="card-body">
								<p class="card-text font-14">Em seguida, a Mendes Tur disponibilizará no site as informações sobre o casamento e a viagem de lua de mel desejada. Assim os noivos anexarão ao convite de casamento um cartão com a informação da lista para conhecimento dos convidados.</p>
							</div>
						</div>
						<div class="card">
							<img class="card-img-top" src="<?php echo get_template_directory_uri(); ?>/img/lua-de-mel/03.png" alt="Card image cap">
							<div class="card-body">
								<p class="card-text font-14">Após isso, é só dizer “SIM” e embarcar rumo a uma Lua de Mel inesquecível. Afinal de contas, em uma ocasião tão especial como esta, todos os mimos são poucos.</p>
							</div>
						</div>
				</div>
				
			<div class="col-lg-8 m-auto">
				<p class="text-center font-14">Alguns dias antes do casamento, a Mendes Tur enviará aos noivos, junto com as passagens, uma listagem com as informações sobre os convidados que optaram por esta forma de presentear os noivos.</p>
			</div>
			</div>
		</section>

		<section id="form-lua-de-mel" class="py-5">
			<div class="container">
				<div class="form-orcamento bg-cinza p-4 mt-4">
					<h5 class="font-weight-bold">Fomulário de Lua de Mel</h5>
					<!-- <form>
						<div class="row">
							<div class="col-12 col-sm-12 col-lg-6 pb-2">
								<input type="text" class="form-control" placeholder="Nome completo*">
							</div>
							<div class="col-12 col-sm-12 col-lg-6 pb-2">
								<input type="text" class="form-control" placeholder="Email*">
							</div>
							<div class="col-12 col-sm-12 col-lg-6 pb-2">
								<input type="text" class="form-control" placeholder="Telefone*">
							</div>
							<div class="col-12 col-sm-12 col-lg-6 pb-2">
								<input type="text" class="form-control" placeholder="Telefone">
							</div>
							
							<div class="col-12 col-sm-12 col-lg-3 pb-2">
								<input type="text" class="form-control" placeholder="Noivo(a)">
							</div>
							<div class="col-12 col-sm-12 col-lg-3 pb-2">
								<input type="text" class="form-control" placeholder="Noiva(a)">
							</div>
							<div class="col-12 col-sm-12 col-lg-3 pb-2">
								<div class="form-group mb-0">
									<div class="input-group date">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">
												<svg aria-hidden="true" data-prefix="fal" data-icon="calendar-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-calendar-alt fa-w-14 text-laranja"><path fill="currentColor" d="M400 64h-48V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H128V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h352c8.8 0 16 7.2 16 16v48H32v-48c0-8.8 7.2-16 16-16zm352 384H48c-8.8 0-16-7.2-16-16V192h384v272c0 8.8-7.2 16-16 16zM148 320h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 96h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm192 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12z" class=""></path></svg>
											</span>
										</div>
										<input type='date' class="form-control" placeholder="Data da cerimonia" />
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-12 col-lg-3 pb-2">
								<input type="text" class="form-control" placeholder="Destino desejado">
							</div>
						</div>
						<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" placeholder="Informações complementares"></textarea>
						<button type="submit" class="btn bg-laranja text-white px-5 mt-3 mb-2">ENVIAR</button>
					</form> -->	
					<?php echo do_shortcode( '[contact-form-7 id="206" title="Lua_de_Mel"]' ); ?>
				</div>
			</div>
		</section>

    </main>

<?php get_footer('newtmpl'); ?>