<?php
// Template Name: Busca lua de mel
session_start();
get_header('newtmpl');

$luademel = $_SESSION["luademel"];


$day = $_SESSION["luademel"][1];

$m = substr($day,0,2);
$d = substr($day,3,2);
$y = substr($day,6,4);
$day = $y . $m . $d;

/*var_dump($luademel[0]);
var_dump($luademel[1]);
var_dump($day);*/


$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'luas_de_mel',
	'meta_query'	=> array(
		'relation'		=> 'OR',
		array(
			'key'		=> 'noivos',
			'value'		=> $luademel[0],
			'compare'	=> 'LIKE'
		),
		array(
			'key'		=> 'data',
			'value'		=> $day,
			'compare'	=> '='
		)
	)
);


$the_query = new WP_Query( $args );
?>


    <main>
        <!--PESQUISA-->
        <?php
            require_once (TEMPLATEPATH."/includes/buscanoivos.php");
        ?>

        <section id="busca-lua-de-mel" class="py-5">
            <div class="container">
				<?php
				if ($luademel[0] !== null){
					$noivos = $luademel[0];
				}

				if ($luademel[1] !== null){
					$data = $luademel[1];
				}
				?>
                <h1 class="font-weight-bold text-uppercase">RESULTADOS BUSCA "<?php
					$busca = "";
					if (isset($noivos))
						$busca .= $noivos;

					if (isset($data)){
						$busca .= " " . $data;
					}
					echo (trim($busca)) ;
					?>"
                </h1>



                <div class="row mt-5">

					<?php
					if($the_query->have_posts()){
						while ($the_query->have_posts()){
							$the_query->the_post();
							?>

                            <div class="col-md-4 col-lg-4">
                                <a href="<?php the_permalink();?>">
                                <img src="<?php the_field('foto_casal');?>" class="img-fluid">
                                </a>
                                <h4 class="text-center font-weight-bold mt-2 mb-0"><?php
                                    $nomeNoivo = get_field('primeiro_nome_noivo');
                                    $nomeNoiva = get_field('primeiro_nome_noiva');

                                    echo ('<a href="'. get_permalink() .'">');
                                    echo ($nomeNoivo . " e " . $nomeNoiva);
                                    echo ('</a>')

                                    ?>
                                </h4>
                                <p class="text-center"><?php the_field('data');?></p>
                            </div>

							<?php
						}
					} else {
						_e("Lua de mel não encontrada");
					}
					?>

                    <!--<div class="col-md-4  col-lg-4">
						<img src="img/lua-de-mel/casal.jpg" class="img-fluid">
						<h4 class="text-center font-weight-bold mt-2 mb-0">Jõao e Luiza</h4>
						<p class="text-center">30.08.2018</p>
					</div>
					<div class="col-md-4  col-lg-4">
						<img src="img/lua-de-mel/casal.jpg" class="img-fluid">
						<h4 class="text-center font-weight-bold mt-2 mb-0">Jõao e Claudia</h4>
						<p class="text-center">30.08.2018</p>
					</div>-->

                </div>
            </div>
        </section>
    </main>

<?php get_footer('newtmpl'); ?>