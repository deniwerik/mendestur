<!Doctype html>
<html lang="pt-BR">
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112763572-2"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-112763572-2');
</script>

<!-- Hotjar Tracking Code for http://mendestur.com.br/2018/ -->
<script>
(function(h,o,t,j,a,r){
h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
h._hjSettings={hjid:872840,hjsv:6};
a=o.getElementsByTagName('head')[0];
r=o.createElement('script');r.async=1;
r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
a.appendChild(r);
})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mendes Tur</title>

    <!--normalize-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.css" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css" type="text/css" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!--FontAwesome-->
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>

    <!--Google-fonts-->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script:400,700" rel="stylesheet">

    <!--DatePicker-->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.standalone.css">
    
    <!--OwlCarosel-->
    <!--<link rel="stylesheet" href="<?/*= get_stylesheet_directory_uri();*/?>/inc/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="<?/*= get_stylesheet_directory_uri();*/?>/inc/owlcarousel/owl.theme.default.min.css">-->

    <!--css-->
    <!--<link rel="stylesheet" href="<?/*= get_stylesheet_directory_uri();*/?>/style.css">-->
    <!--<script src="<?/*= get_template_directory_uri();*/?>/scripts/jquery-3.3.1.js" type="application/javascript"></script>-->

    <style>
        .intrinsic-container {
            position: relative;
            height: 0;
            overflow: hidden;
        }

        /* 16x9 Aspect Ratio */
        .intrinsic-container-16x9 {
            padding-bottom: 56.25%;
        }

        /* 4x3 Aspect Ratio */
        .intrinsic-container-4x3 {
            padding-bottom: 75%;
        }

        .intrinsic-container iframe {
            position: absolute;
            top:0;
            left: 0;
            width: 102%;
            height: 100%;
        }
    </style>


    <?php wp_head(); ?>
</head>

<body data-spy="scroll" data-target="#navbar-example" id="page-top">

<header class="bg-light pt-3 d-none d-lg-block">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <a href="http://mendestur.com.br"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" class="img-fluid w-50"></a>
            </div>
            <div class="col-lg-10">
                <p class="float-right px-2 text-azul">Miramar Shopping: <a href="tel:+551332089000" class="text-azul mr-3 font-weight-bold">(13) 3208-9000</a> Praiamar Shopping: <a href="tel:+551332799000" class="text-azul font-weight-bold">(13) 3279-9000</a></p>
            </div>
            <span class="border-header"></span>
        </div>
    </div>
</header>

<div id="mainNav" class="content affix-top" data-spy="affix" data-offset-top="100">
<nav  class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand d-none d-sm-block d-lg-none" href="http://mendestur.com.br"><img src="<?php echo get_template_directory_uri(); ?>/img/logo-02.png" class="img-fluid w-50"></a>
        <div class="d-inline d-none d-sm-block d-lg-none">
            <a class="text-laranja mr-3" data-toggle="modal" data-target="#contato-modal" href="#"><i class="fa fa-phone fa-lg text-laranja"></i></a>
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars fa-lg text-laranja" aria-hidden="true"></i>
            </button>
        </div>


        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
	        <?php
	        /*$args = array("menu"=>"principal","theme_location"=>"menu-principal","container"=>false);
	        wp_nav_menu($args);*/

	        wp_nav_menu( array(
	            'menu'=>'principal',
		        'theme_location' => 'menu-principal',
		        'container' => 'ul',
		        'menu_class'=> 'navbar-nav ml-auto mt-2 mt-lg-0'
	        ) );


	        ?>
            <!--<ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="/page-home.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/page-aereo.php">Aéreos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/page-cruzeiro.php">Cruzeiros</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/page-destino.php">Destinos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/page-orcamento.php">Orçamento</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/page-lua-de-mel.php">Lua de Mel</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://www.confidencecambio.com.br/conversor-moedas/">Câmbio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/page-quem-somos.php">Quem somos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/page-contato.php">Contato</a>
                </li>
            </ul>-->
        </div>
    </div>
</nav>
</div>



<!-- Modal -->
<!-- <div class="modal fade" id="contato-modal" tabindex="-1" role="dialog" aria-labelledby="contato-modal" aria-hidden="true">
  <div class="modal-dialog p-2" role="document">
    <div class="modal-content rounded-0">
      <div class="modal-header border-0">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body px-3">
        <p class="px-2">Miramar Shopping: <a href="tel:+551332089000" class="text-azul mr-3 font-weight-bold">(13) 3208-9000</a></p>
        <p class="px-2">Praiamar Shopping: <a href="tel:+551332799000" class="text-azul font-weight-bold">(13) 3279-9000</a></p>
      </div>
    </div>
  </div>
</div> -->