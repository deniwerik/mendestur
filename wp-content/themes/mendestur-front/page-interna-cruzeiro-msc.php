<?php
// Template Name: Cruzeiro MSC
get_header('newtmpl');
?>
<!-- Cruzeiro MSC -->
	<main>
		<section id="interna-cruzeiro-header">
			<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/header-cruzeiro-msc.jpg" class="img-fluid w-100 d-block">
			<div class="hero">
				<h1 class="text-center text-white text-uppercase bottom-line-header line-center">MSC Cruzeiros teste</h1>
			</div>
		</section>

		<section id="page-interna-cruzeiro" class="pb-2">
			<div class="container">
				<ul class="nav mb-4">
					<li class="nav-item">
						<a class="nav-link active" href="#sobre">Sobre</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#solicitar">Solicitar</a>
					</li>
				</ul>

				<div class="bg-azul-marinho">
					<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/logo-msc.png" class="m-auto d-block logo">
				</div>

				<div class="row py-4" id="sobre">
					<div class="col-lg-6">
						<h3 class="text-azul">MSC Cruzeiros</h3>
						<p>Explorando as mares há mais de <strong>300 anos.</strong> O que começou com apenas um navio, cresceu e transformou-se em uma frota magnífica. No decorrer dos séculos, a família <strong>Aponte</strong> manteve sua dedicação à navegação dos mares e, em 1970, fundou a Mediterranean Shipping Company, que agora é a segunda maior transportadora de contêineres do mundo. Com uma tradição marítima e amor pelo mar tão sólidos, a <strong>MSC Cruzeiros</strong> nasceu em <strong>1988</strong> e tornou-se uma das empresas de viagem de crescimento mais rápido do mundo, com uma frota de <strong>17 navios.</strong></p>
						<p class="text-uppercase font-weight-bold heading pl-2">
							Informações extras
							<span class="divider-left"></span>
						</p>
						<p><strong>Frota:</strong> 17 Navios</p>
						<p><strong>Destinos:</strong> Caribe, Cuba e Antilhas, Norte da Europa, Sul da África, Mediterrâneo, América do Norte, MSC Grand Voyages, América do Sul, Ilhas Canárias e Marrocos.</p>
						<p><strong>Serviços:</strong> Hospitalidade, Cabines e Suítes, Refeições e Restaurantes, Bares e Lounges públicos, Entretenimento e lazer, Spa e Relaxamento, Centro Esportivo, Instalações esportivas, Lavanderia, Recepção 24h, Correio, Compras, MSC Babycare,  e muito mais.</p>

					</div>
					<div class="col-lg-6">
						<div class="outer">
							<div id="big" class="owl-carousel owl-theme">
								<div class="item">
									<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/01.jpg" class="img-fluid">
									<div class="carousel-caption">
										<p class="font-14 text-white mb-0 text-uppercase font-weight-bold">1</p>
									</div>
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/01.jpg" class="img-fluid">
									<div class="carousel-caption">
										<p class="font-14 text-white mb-0 text-uppercase font-weight-bold">2</p>
									</div>
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/01.jpg" class="img-fluid">
									<div class="carousel-caption">
										<p class="font-14 text-white mb-0 text-uppercase font-weight-bold">3</p>
									</div>
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/01.jpg" class="img-fluid">
									<div class="carousel-caption">
										<p class="font-14 text-white mb-0 text-uppercase font-weight-bold">4</p>
									</div>
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/01.jpg" class="img-fluid">
									<div class="carousel-caption">
										<p class="font-14 text-white mb-0 text-uppercase font-weight-bold">5</p>
									</div>
								</div>
							</div>
							<div id="thumbs" class="owl-carousel owl-theme">
								<div class="item">
									<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/01.jpg" class="img-fluid">
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/01.jpg" class="img-fluid">
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/01.jpg" class="img-fluid">
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/01.jpg" class="img-fluid">
								</div>
								<div class="item">
									<img src="<?php echo get_template_directory_uri(); ?>/img/cruzeiros/nowergian/01.jpg" class="img-fluid">
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="text-center bg-cinza py-4">
					<a href="http://www.msctrade.com/pnp/bengine.aspx?uid=CCAFF3CB425F46DCBB49C5C5DF6AC75C" class="btn btn-lg bg-azul text-uppercase text-white px-5" target="_blank">
						Buscar todos os cruzeiros	
					</a>
				</div>

				<div class="form-orcamento bg-cinza p-4 mt-5" id="solicitar">
					<h5 class="font-weight-bold">Ficou interessado?</h5>
					<p>Preencha o formulário abaixo, e solicite o cruzeiro que deseja.</p>
					<form>
						<div class="row">
							<div class="col-12 col-sm-12 col-lg-4 pb-2">
								<input type="text" class="form-control" placeholder="Nome">
							</div>
							<div class="col-12 col-sm-12 col-lg-4 pb-2">
								<input type="text" class="form-control" placeholder="Telefone">
							</div>
							<div class="col-12 col-sm-12 col-lg-4 pb-2">
								<input type="text" class="form-control" placeholder="Email">
							</div>

							<div class="col-12 col-sm-12 col-lg-4 pb-2">
								<div class="form-group mb-0">
									<div class="input-group date" data-provide="datepicker">
										<div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">
												<i class="far fa-calendar-alt text-laranja"></i>
											</span>
										</div>
										<input type='text' class="form-control" placeholder="Mês de saída" />
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-12 col-lg-4 pb-2">
								<input type="text" class="form-control" placeholder="Destino">
							</div>
							<div class="col-12 col-sm-12 col-lg-4 pb-2">
								<select class="form-control classic" id="exampleFormControlSelect1">
									<option selected>Navio</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>

							<div class="col-12 col-sm-12 col-lg-4 pb-2">
								<select class="form-control classic" id="exampleFormControlSelect2">
									<option selected>Porto de embarque</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>
							<div class="col-12 col-sm-12 col-lg-4 pb-2">
								<select class="form-control classic" id="exampleFormControlSelect3">
									<option selected>Tipo de cabine</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>
							<div class="col-6 col-sm-6 col-lg-2 pb-2">
								<select class="form-control classic" id="exampleFormControlSelect4">
									<option selected>Adultos</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>
							<div class="col-6 col-sm-6 col-lg-2 pb-2">
								<select class="form-control classic" id="exampleFormControlSelect5">
									<option selected>Crianças</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>
						</div>
						<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" placeholder="Informações complementares">oiiii</textarea>
						<button type="submit" class="btn bg-laranja text-white px-5 mt-3 mb-2">CONFIRMAR INTERESSE</button>
					</form>
				</div>
			</div>
		</section>

		<!--     <?php
        require_once (TEMPLATEPATH."/includes/instagram.php");
        ?> -->

    </main>

<?php get_footer('newtmpl'); ?>