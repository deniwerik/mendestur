<?php
// Template Name: Categoria Cruzeiros
get_header('newtmpl');
?>
<!-- Categoria Cruzeiros -->
    <main>
        <section id="interna-cruzeiro-header">
            <img src="<?php the_field('header_cruzeiro');?>" class="img-fluid w-100 d-block">
            <div class="hero">
                <h1 class="text-center text-white text-uppercase bottom-line-header line-center"><?php the_field('titulo_cruzeiro');?></h1>
            </div>
        </section>

        <section id="page-interna-cruzeiro" class="pb-2">
            <div class="container">
                <ul class="nav mb-4">
                    <li class="nav-item">
                        <a class="nav-link active" href="#sobre">Sobre</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#solicitar">Solicitar</a>
                    </li>
                </ul>

                <div class="<?php the_field('bglogo_cruzeiro');?>">
                    <img src="<?php the_field('logo_cruzeiro');?>" class="m-auto d-block logo">
                </div>

                <div class="row py-4" id="sobre">
                    <div class="col-lg-6">
                        <h3 class="text-azul"><?php the_field('titulo_cruzeiro');?></h3>
						<?php the_field('descricao_cruzeiro');?>
                        <p class="text-uppercase font-weight-bold heading pl-2">
                            Informações extras
                            <span class="divider-left"></span>
                        </p>
                        <p><strong>Frota:</strong> <?php the_field('frota');?></p>
                        <p><strong>Destinos:</strong> <?php the_field('destinos');?></p>
                        <p><strong>Serviços:</strong> <?php the_field('servicos');?></p>

                    </div>

					<?php
					if (have_rows('carrossel')) {
					?>
                    <div class="col-lg-6">
						<?php
						while (have_rows('carrossel')) {
						the_row();
						?>

                        <!--carousel-->
                        <div class="outer">
                        <div id="<?php the_sub_field('id_carrossel');?>" class="owl-carousel owl-theme">
							<?php
							if (have_rows('imagem')){
								while(have_rows('imagem')){
									the_row();
									?>
                                    <div class="item">
                                        <img src="<?php the_sub_field('foto'); ?>" class="img-fluid">
										<?php
										if (get_sub_field('caption') !== ""){
											?>
                                            <div class="carousel-caption">
                                                <p class="font-14 text-white mb-0 text-uppercase font-weight-bold">
                                                    <?php the_sub_field('caption'); ?>
                                                </p>
                                            </div>
											<?php
										}
										?>
                                    </div>
									<?php
								}
							}
							?>
                            <!--<div class="item">
                                        <img src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/msc-carrousel/02.jpg"
                                             class="img-fluid">
                                    </div>
                                    <div class="item">
                                        <img src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/msc-carrousel/03.jpg"
                                             class="img-fluid">
                                    </div>
                                    <div class="item">
                                        <img src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/msc-carrousel/04.jpg"
                                             class="img-fluid">
                                    </div>
                                    <div class="item">
                                        <img src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/msc-carrousel/01.jpg"
                                             class="img-fluid">
                                    </div>
                                </div>-->
							<?php
							}
							?>

                            <!--<div id="sync2" class="owl-carousel owl-theme">
                            <div class="item">
                                <img src="<?php /*echo get_template_directory_uri(); */
							?>/img/cruzeiros/msc-carrousel/01.jpg" class="img-fluid">
                            </div>
                            <div class="item">
                                <img src="<?php /*echo get_template_directory_uri(); */
							?>/img/cruzeiros/msc-carrousel/02.jpg" class="img-fluid">
                            </div>
                            <div class="item">
                                <img src="<?php /*echo get_template_directory_uri(); */
							?>/img/cruzeiros/msc-carrousel/03.jpg" class="img-fluid">
                            </div>
                            <div class="item">
                                <img src="<?php /*echo get_template_directory_uri(); */
							?>/img/cruzeiros/msc-carrousel/04.jpg" class="img-fluid">
                            </div>
                            <div class="item">
                                <img src="<?php /*echo get_template_directory_uri(); */
							?>/img/cruzeiros/msc-carrousel/01.jpg" class="img-fluid">
                            </div>
                        </div>-->
                        </div>
						<?php
						}
						?>
                    </div>
                </div>

                    <div class="text-center bg-cinza py-4">
                        <a href="http://www.msctrade.com/pnp/bengine.aspx?uid=CCAFF3CB425F46DCBB49C5C5DF6AC75C" class="btn btn-lg bg-azul text-uppercase text-white px-5" target="_blank">
                            Buscar todos os cruzeiros
                        </a>
                    </div>

                    <div class="form-orcamento bg-cinza p-4 mt-5" id="solicitar">
                        <h5 class="font-weight-bold">Ficou interessado?</h5>
                        <p>Preencha o formulário abaixo, e solicite o cruzeiro que deseja.</p>
             <!--          <form>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-lg-4 pb-2">
                                    <input type="text" class="form-control" placeholder="Nome">
                                </div>
                                <div class="col-12 col-sm-12 col-lg-4 pb-2">
                                    <input type="text" class="form-control" placeholder="Telefone">
                                </div>
                                <div class="col-12 col-sm-12 col-lg-4 pb-2">
                                    <input type="text" class="form-control" placeholder="Email">
                                </div>

                                <div class="col-12 col-sm-12 col-lg-4 pb-2">
                                    <div class="form-group mb-0">
                                        <div class="input-group date" data-provide="datepicker">
                                            <div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">
												<i class="far fa-calendar-alt text-laranja"></i>
											</span>
                                            </div>
                                            <input type='text' class="form-control" placeholder="Mês de saída" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-lg-4 pb-2">
                                    <input type="text" class="form-control" placeholder="Destino">
                                </div>
                                <div class="col-12 col-sm-12 col-lg-4 pb-2">
                                    <select class="form-control classic" id="exampleFormControlSelect1">
                                        <option selected>Navio</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>

                                <div class="col-12 col-sm-12 col-lg-4 pb-2">
                                    <select class="form-control classic" id="exampleFormControlSelect2">
                                        <option selected>Porto de embarque</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-lg-4 pb-2">
                                    <select class="form-control classic" id="exampleFormControlSelect3">
                                        <option selected>Tipo de cabine</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <div class="col-6 col-sm-6 col-lg-2 pb-2">
                                    <select class="form-control classic" id="exampleFormControlSelect4">
                                        <option selected>Adultos</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <div class="col-6 col-sm-6 col-lg-2 pb-2">
                                    <select class="form-control classic" id="exampleFormControlSelect5">
                                        <option selected>Crianças</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="4" placeholder="Informações complementares"></textarea>
                            <button type="submit" class="btn bg-laranja text-white px-5 mt-3 mb-2">CONFIRMAR INTERESSE</button>
                        </form> -->
						<?php echo do_shortcode( '[contact-form-7 id="175" title="Operadoras"]' ); ?>
                    </div>
                </div>
        </section>

        <?php
        $operadora = get_field('operadora');
        
        $args = array(
	        'numberposts'	=> -1,
	        'post_type'		=> 'cruzeiros',
	        'meta_key'		=> 'operadora',
	        'meta_value'	=> $operadora
        );

        // query
        $the_query = new WP_Query( $args );

        if ($the_query->have_posts()){
        ?>

        <section id="cat-cruzeiros" class="py-5">
            <div class="container">
                <h2 class="text-azul text-uppercase mb-3">Cruzeiros</h2>

                <div class="row">

				    <?php

					    while ($the_query->have_posts()){
						    $the_query->the_post();
						    ?>
                            <div class="col-lg-12 border mb-2">
                                <div class="row">
                                    <div class="col-lg-3 py-3">
                                        <img src="<?php the_field('imagem_navio');?>" class="img-fluid">
                                    </div>
                                    <div class="col-lg-3 py-3 border-right">
                                        <p class="font-14 mb-0"><?php the_field('operadora');?></p>
                                        <h3 class="text-azul font-weight-bold mb-0"><?php the_field('navio');?></h3>
                                        <p class="mb-0"><strong>Saída:</strong> <?php the_field('saida');?></p>
                                        <h5 class="text-laranja font-weight-bold"><?php the_field('noites_local');?></h5>
                                    </div>
                                    <div class="col-lg-3 py-3">
                                        <p class="font-12 mb-0">Valor Total</p>
                                        <h1 class="text-azul font-weight-bold"><?php the_field('preco');?></h1>
                                        <p class="mb-0 font-12"><?php the_field('descricao_preco');?></p>
                                        <div class="triangulo"></div>
                                    </div>
                                    <div class="col-lg-3 bg-laranja py-5 text-center">
                                        <a href="<?php the_permalink();?>" class="btn btn-lg btn-border text-white px-4">
                                            VER TODOS
                                        </a>
                                    </div>
                                </div>
                            </div>

						    <?php
					    }
				    }
				    wp_reset_query();
				    wp_reset_postdata();
				    ?>

                </div>
            </div>
        </section>

		<?php
		require_once (TEMPLATEPATH."/includes/instagram.php");
		?>

    </main>

<?php get_footer('newtmpl'); ?>