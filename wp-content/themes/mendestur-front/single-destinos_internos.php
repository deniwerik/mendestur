<?php
// Template Name: Single Destinos Internos

get_header('newtmpl');
echo ("teste");
?>
<!-- Single Destinos Internos - Mas é API -->
<?php


if (have_posts()){
	while (have_posts()) {
		the_post();

		?>


        <main>

            <!--PESQUISA-->
			<?php
			require_once( TEMPLATEPATH . "/includes/busca.php" );
			?>

            <section id="page-interna-destinos" class="py-5">
                <div class="container">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb bg-white font-14">
                            <li class="breadcrumb-item"><a href="<?php echo  get_site_url(); ?>">Home</a>
                            </li>
                            <li class="breadcrumb-item" aria-current="page"><a href="<?php echo get_template_directory_uri('destino'); ?>/">">Destinos</a></li>
                            <li class="breadcrumb-item active text-laranja"
                                aria-current="page"><?= $pacote["titulo"]; ?></li>
                        </ol>
                    </nav>

                    <h1 class="text-laranja pb-3"><?= $pacote["titulo"]; ?></h1>

                    <div class="row">
                        <div class="col-lg-8">
                            <img src="<?= $pacote['imagem']; ?>" class="img-fluid">

                            <ul class="nav nav-pills mb-3 mt-4 border-branco" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active text-uppercase" id="info-tab" data-toggle="pill"
                                       href="#informacoes" role="tab" aria-controls="informacoes" aria-selected="true">Informações</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" id="incluso-tab" data-toggle="pill"
                                       href="#incluso" role="tab" aria-controls="incluso"
                                       aria-selected="false">Incluso</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link text-uppercase" id="condicoes-tab" data-toggle="pill"
                                       href="#condicoes" role="tab" aria-controls="condicoes" aria-selected="false">Condições
                                        Gerais</a>
                                </li>
                            </ul>
                            <div class="tab-content mb-5" id="pills-tabContent">
                                <!--Informações-->
                                <div class="tab-pane fade show active" id="informacoes" role="tabpanel"
                                     aria-labelledby="pills-home-tab">
									<?php
									if ( ! empty( $pacote['descricao'] ) ) {
										echo( "<p class='mb-0'>Descrição: <strong>" . $pacote['descricao'] . "</strong></p>" );
									}
									?>
                                    <p class="mb-0">Local: <strong><?= $pacote['titulo']; ?></strong></p>
                                    <p class="mb-0">Saída: <strong><?= $pacote['saida']; ?></strong></p>
                                    <p class="mb-0">Quantidade de dias: <strong><?= $pacote['dias']; ?> dias</strong>
                                    </p>
                                    <!--<p class="mb-0">Hospedagem: <strong></strong></p>-->
									<?php
									if ( ! empty( $pacote['visitando'] ) ) {
										echo( "<p class='mb-0'>Visitando: <strong>" . $pacote['visitando'] . "</strong></p>" );
									}

									?>

                                </div>
                                <div class="tab-pane fade" id="incluso" role="tabpanel"
                                     aria-labelledby="pills-profile-tab">
									<?php
									$inclui = $pacote["inclui"];
									$inclui = str_replace( ";", "<br>", $inclui );
									echo( $inclui );; ?>
                                </div>
                                <div class="tab-pane fade" id="condicoes" role="tabpanel"
                                     aria-labelledby="pills-contact-tab">
									<?= $pacote['importante']; ?>
                                </div>
                            </div>

                            <div class="border-top"></div>
                            <div class="row mt-3">
                                <div class="col-lg-6">
                                    <a class="btn bg-laranja mb-2 text-white text-uppercase px-4"><i
                                                class="fas fa-share-alt"></i> Compartilhar</a>
                                </div>
                                <div class="col-lg-6">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/destinos/logo-cvc.png"
                                         class="img-fluid w-25 float-right">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 bg-cinza p-5 form-interesse">
                            <p class="text-laranja font-weight-bold mb-1">12x <span class="h4 font-weight-bold">
                                <?php
                                $valor = floatval( $pacote["valor"] ) / 12;
                                $valor = round( $valor, 2 );
                                $valor = str_replace( ".", ",", $valor );
                                if ( strlen( $valor ) === 2 ) {
	                                $valor .= ",00";
                                }
                                echo( $valor );
                                ?>
                            </span>
                            </p>
                            <p class="text-azul font-weight-bold border-bottom pb-3">
                                Total: <?= $pacote["moeda"] . $pacote["valor"]; ?>,00</p>
                            <h5 class="font-weight-bold">Ficou interessado?</h5>
                            <p>Caso tenha gostado desse pacote de viagem, preencha o formulário abaixo, nossa equipe
                                logo entrará em contato com você, para maiores informações.</p>
                            <!-- <form>
                                <input type="name" class="form-control mb-3" id="exampleInputName"
                                       aria-describedby="emailHelp" placeholder="Nome">
                                <input type="phone" class="form-control mb-3" id="exampleInputPhone"
                                       aria-describedby="emailHelp" placeholder="Telefone">
                                <input type="email" class="form-control mb-3" id="exampleInputEmai1"
                                       aria-describedby="emailHelp" placeholder="Email">
                                <button type="submit" class="btn bg-laranja mb-2 text-white px-4">Enviar</button>
                            </form> -->
							<?php echo do_shortcode( '[contact-form-7 id="218" title="Destinos_Interna" your-destino="'.$pacote["titulo"].'"]' ); ?>
                        </div>
                    </div>
                </div>
            </section>

            <section id="page-destinos" class="py-5 bg-cinza-claro">
                <div class="container">
                    <div class="input-group">
                        <h3 class="text-uppercase text-azul align-self-center mb-0 mr-3">VEJA MAIS DESTINOS</h3>
                        <a href="<?php echo get_template_directory_uri(); ?>/page-destino.php"
                           class="btn btn-lg bg-azul text-white px-4">
                            VER TODOS
                        </a>
                    </div>
                    <div class="row pt-5">
						<?php
						foreach ( $pacotes as $key => $value ) {
							if ( $value["id"] !== $pacote["id"] ) {
								?>
                                <div class="col-lg-3 mb-4">
                                    <div class="card">
                                        <img class="img-fluid card-img-top" src="<?= $value['imagem']; ?>"
                                             alt="Card image cap">
                                        <div class="card-body">
                                            <h5 class="font-weight-bold text-laranja mb-0"><?= $value['titulo']; ?></h5>
                                            <p class="font-weight-bold mb-0"><?= $value['dias']; ?> dias</p>
                                            <div class="border"></div>
                                            <p class="font-14 mb-0 mt-1">
												<?php
												$val = floatval( $value["valor"] ) / 12;
												$val = round( $val, 2 );
												$val = str_replace( ".", ",", $val );
												if ( strlen( $val ) === 2 ) {
													$val .= ",00";
												}
												echo( "12x de $val" );
												?>
                                            </p>
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <p class="text-azul font-weight-bold mb-0">R$ <span
                                                                class="h4 font-weight-bold"><?= $value["valor"]; ?>
                                                            ,00</span></p>
                                                </div>
                                                <div class="col-lg-4">
                                                    <img
                                                            src="<?php echo get_template_directory_uri(); ?>/img/destinos/logo-abreu.jpg"
                                                            class="img-fluid">
                                                </div>
                                            </div>
                                            <button class="btn btn-lg bg-laranja text-white px-4 btn-detalhes"
                                                    data-pacote="<?= $value['id']; ?>"
                                                    data-regiao="<?= $_SESSION['id_regiao']; ?>">VER DETALHES
                                            </button>
                                            <!--<a href="#" class="btn btn-lg bg-laranja text-white px-4">
												VER DETALHES
											</a>-->
                                        </div>
                                    </div>
                                </div>
								<?php
							}
						}
						?>

                    </div>
                </div>
            </section>

        </main>

		<?php
	}
}
?>

<?php get_footer('newtmpl'); ?>