<?php
// Template Name: Single Cruzeiros
get_header('newtmpl');
?>

<?php
if (have_posts()){
	while (have_posts()){
		the_post();
		?>
<!-- Single Cruzeiros -->
        <main>
            <section id="interna-cruzeiro-header">
                <img src="<?php the_field('imagem_header'); ?>" class="img-fluid w-100 d-block">
                <div class="hero">
                    <h1 class="text-center text-white text-uppercase bottom-line-header line-center"><?php the_field('operadora'); ?> - <?php the_field('navio'); ?></h1>
                </div>
            </section>

            <section id="page-interna-cruzeiro" class="pb-2">
                <div class="container">
                    <ul class="nav mb-4">
                        <li class="nav-item">
                            <a class="nav-link active" href="#info">Informações</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#solicitar">Solicitar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#navio">Navio</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#condicoes">Condições Gerais</a>
                        </li>
                    </ul>

                    <div class="row mb-4">
                        <div class="col-lg-6 bg-cinza-claro py-3 px-4">
                            <h1 class="font-weigth-bold text-azul mb-0"><?php the_field('noites_local'); ?></h1>
                            <p class="mb-0"><?php the_field('saida'); ?></p>
                            <!-- <?php
                            the_field('noites_local');
                            the_field('saida');
                            ?> -->
                        </div>
                        <div class="col-lg-6 bg-azul">
                            <div class="row py-2">
                                <div class="col-lg-6 align-self-center border-right">
                                    <div class="font-12 text-white mb-0"><?php the_field('total_parcelado');?></div>
                                    <h2 class="font-weigth-bold text-laranja"><?php the_field('valor');?></h2>
                                    <!-- <?php
                                    the_field('total_parcelado');
                                    the_field('valor');
                                    ?> -->
                                </div>
                                <div class="col-lg-6 align-self-center font-12 text-white mb-0">
                                   <?php the_field('descricao_preco'); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p class="text-uppercase font-weight-bold heading">
                        Informações extras
                        <span class="divider-left"></span>
                    </p>

                    <div class="row">
                        <div class="col-lg-8">
                            <!--<p class="mb-0"><?php /*the_field('noites_local'); */?></p>
                            <p class="mb-0"><strong>Saída:</strong> <?php /*the_field('saida'); */?></p>
                            <p class="mb-0"><strong>Embarque:</strong> <?php /*the_field('embarque'); */?></p>
                            <p class="mb-0"><strong>Desembarque:</strong> <?php /*the_field('desembarque'); */?></p>
                            <p class="mb-0 font-weight-bold">Cabine Interna Garantida</p>
                            <p class="font-weight-bold text-laranja">TODAS AS TAXAS INCLUÍDAS!</p>-->
                            <?
                            the_field('informacoes');
                            ?>
                        </div>
                        <div class="col-lg-4 px-5">
                            <img src="<?php the_field('imagem_logo'); ?>" class="img-fluid w-50 m-auto d-block">
                        </div>
                    </div>
                </div>
            </section>

            <section id="solicitar">
                <div class="container">
                    <div class="form-orcamento bg-cinza p-4" id="solicitar">
                        <h5 class="font-weight-bold">Ficou interessado?</h5>
                        <p>Preencha o formulário abaixo, e solicite o cruzeiro que deseja.</p>
                        <p class="border-bottom ">Dados da Viagem</p>
						<div class="dados-viagens mb-3">
                            <?php the_field('noites_local');?>
                            <?php the_field('saida'); ?>
                            <p class="mb-0"><strong>Navio:</strong> <?php the_field('navio');?></p>
                        </div>
                        <!-- <form>
                            <p class="border-bottom">Dados da Viagem</p>
                            <p class="mb-0"><strong>Viagem:</strong> <?php the_field('noites_local'); ?></p>
                            <p class="mb-0"><strong>Saída:</strong> <?php the_field('saida'); ?></p>
                            <p class="mb-0"><strong>Navio:</strong> <?php the_field('navio'); ?></p>
                            <div class="row">
                                <div class="col-6 col-sm-6 col-lg-2 pb-2">
                                    <select class="form-control classic" id="exampleFormControlSelect1">
                                        <option selected>Adultos</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                                <div class="col-6 col-sm-6 col-lg-2 pb-2">
                                    <select class="form-control classic" id="exampleFormControlSelect2">
                                        <option selected>Crianças</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
                            <p class="border-bottom">Dados Pessoais</p>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-lg-4 pb-2">
                                    <input type="text" class="form-control" placeholder="Nome">
                                </div>
                                <div class="col-12 col-sm-12 col-lg-4 pb-2">
                                    <input type="text" class="form-control" placeholder="Telefone">
                                </div>
                                <div class="col-12 col-sm-12 col-lg-4 pb-2">
                                    <input type="text" class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="4" placeholder="Informações complementares"></textarea>
                            <button type="submit" class="btn bg-laranja text-white px-5 mt-3 mb-2">CONFIRMAR INTERESSE</button>
                        </form> -->
						<?php echo do_shortcode( '[contact-form-7 id="278" title="Cruzeiro_Interna"]' ); ?>
                    </div>
                </div>
            </section>

            <section id="navio" class="py-5">
                <div class="container">
                    <p class="text-uppercase font-weight-bold heading">
                        Navio
                        <span class="divider-left"></span>
                    </p>
                    <h2 class="text-azul"><?php the_field('navio'); ?></h2>
                    <div class="row">
                        <div class="col-lg-6">
                            <img src="<?php the_field('imagem_navio'); ?>" class="img-fluid mb-3">
                            <?php the_field('descricao_navio'); ?>
                        </div>
                        <div class="col-lg-6">

                            <!--carousel-->

							<?php
							if (have_rows('carrossel')){
								while (have_rows('carrossel')){
									the_row();
									?>

                                    <div id="<?php the_sub_field('id_carrossel');?>" class="owl-carousel owl-theme show">

										<?php
										if (have_rows('imagem')){
											while (have_rows('imagem')){
												the_row();
												?>
                                                <div class="item">
                                                    <img src="<?php the_sub_field('foto') ;?>" class="img-fluid">

													<?php
													if (get_sub_field('caption') !== ""){
														?>

                                                        <div class="carousel-caption">
                                                            <p class="font-14 text-white mb-0 text-uppercase font-weight-bold"><?php the_sub_field('caption')?></p>
                                                        </div>
														<?php
													}
													?>

                                                </div>
												<?php
											}
										}
										?>

                                        <!--<div class="item">
                                    <img src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/msc-carrousel/02.jpg" class="img-fluid">
                                </div>
                                <div class="item">
                                    <img src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/msc-carrousel/03.jpg" class="img-fluid">
                                </div>
                                <div class="item">
                                    <img src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/msc-carrousel/04.jpg" class="img-fluid">
                                </div>
                                <div class="item">
                                    <img src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/msc-carrousel/01.jpg" class="img-fluid">
                                </div>
                                -->
                                    </div>

									<?php
								}
							}
							?>

                            <!--<div id="sync2" class="owl-carousel owl-theme">
                                <div class="item">
                                    <img src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/nowergian/01.jpg" class="img-fluid">
                                </div>
                                <div class="item">
                                    <img src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/msc-carrousel/02.jpg" class="img-fluid">
                                </div>
                                <div class="item">
                                    <img src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/msc-carrousel/03.jpg" class="img-fluid">
                                </div>
                                <div class="item">
                                    <img src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/msc-carrousel/04.jpg" class="img-fluid">
                                </div>
                                <div class="item">
                                    <img src="<?php /*echo get_template_directory_uri(); */?>/img/cruzeiros/msc-carrousel/01.jpg" class="img-fluid">
                                </div>
                            </div>-->

                        </div>
                    </div>
                </div>
                </div>
            </section>

            <section id="condicoes">
                <div class="container">
                    <p class="text-uppercase font-weight-bold heading">
                        CONDIÇÕES GERAIS
                        <span class="divider-left"></span>
                    </p>
                    <?php
                    the_field('condicoes_gerais');
                    ?>
                </div>
            </section>


        </main>

		<?php
	}
}
?>

<?php get_footer('newtmpl'); ?>