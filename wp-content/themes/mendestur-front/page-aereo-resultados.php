<?php
// Template Name: Resultados-Aereos
session_start();
get_header('newtmpl');
$disponibilidade = json_decode($_SESSION["disponibilidade"], true);
$melhoresPrecos = $_SESSION["melhoresPrecos"];
$data = $_SESSION["data"];

var_dump($melhoresPrecos);
//var_dump($disponibilidade);

//echo($disponibilidade);

?>
    <main>
        <section id="pesquisa" class="py-3 bg-laranja">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-check form-check-inline mb-3">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" checked>
                            <label class="form-check-label" for="inlineRadio1">Ida e volta</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                            <label class="form-check-label" for="inlineRadio2">Somente ida</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
                            <label class="form-check-label" for="inlineRadio3">Multiplos trechos</label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <a href="page-orcamento.php" class="font-14 nav-link float-right"><u>Não encontrou a viagem que gostaria?</u></a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-8">
                        <div class="row">
                            <!--Origem e destino-->
                            <div class="col-12 col-sm-12 col-lg-3">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><svg aria-hidden="true" data-prefix="far" data-icon="map-marker-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-map-marker-alt fa-w-12 text-laranja"><path fill="currentColor" d="M192 0C85.903 0 0 86.014 0 192c0 71.117 23.991 93.341 151.271 297.424 18.785 30.119 62.694 30.083 81.457 0C360.075 285.234 384 263.103 384 192 384 85.903 297.986 0 192 0zm0 464C64.576 259.686 48 246.788 48 192c0-79.529 64.471-144 144-144s144 64.471 144 144c0 54.553-15.166 65.425-144 272zm-80-272c0-44.183 35.817-80 80-80s80 35.817 80 80-35.817 80-80 80-80-35.817-80-80z" class=""></path></svg></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Origem" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-lg-3">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><svg aria-hidden="true" data-prefix="far" data-icon="map-marker-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-map-marker-alt fa-w-12 text-laranja"><path fill="currentColor" d="M192 0C85.903 0 0 86.014 0 192c0 71.117 23.991 93.341 151.271 297.424 18.785 30.119 62.694 30.083 81.457 0C360.075 285.234 384 263.103 384 192 384 85.903 297.986 0 192 0zm0 464C64.576 259.686 48 246.788 48 192c0-79.529 64.471-144 144-144s144 64.471 144 144c0 54.553-15.166 65.425-144 272zm-80-272c0-44.183 35.817-80 80-80s80 35.817 80 80-35.817 80-80 80-80-35.817-80-80z" class=""></path></svg></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Destino" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                            </div>

                            <!--Data-->
                            <div class="col-6 col-sm-6 col-lg-3">
                                <div class="form-group">
                                    <div class="input-group date" data-provide="datepicker">
                                        <div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">
												<svg aria-hidden="true" data-prefix="fal" data-icon="calendar-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-calendar-alt fa-w-14 text-laranja"><path fill="currentColor" d="M400 64h-48V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H128V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h352c8.8 0 16 7.2 16 16v48H32v-48c0-8.8 7.2-16 16-16zm352 384H48c-8.8 0-16-7.2-16-16V192h384v272c0 8.8-7.2 16-16 16zM148 320h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 96h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm192 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12z" class=""></path></svg>
											</span>
                                        </div>
                                        <input type='text' class="form-control" placeholder="Partida" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-6 col-sm-6 col-lg-3">
                                <div class="form-group">
                                    <div class="input-group date" data-provide="datepicker">
                                        <div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">
												<svg aria-hidden="true" data-prefix="fal" data-icon="calendar-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-calendar-alt fa-w-14 text-laranja"><path fill="currentColor" d="M400 64h-48V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H128V12c0-6.6-5.4-12-12-12h-8c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V112c0-26.5-21.5-48-48-48zM48 96h352c8.8 0 16 7.2 16 16v48H32v-48c0-8.8 7.2-16 16-16zm352 384H48c-8.8 0-16-7.2-16-16V192h384v272c0 8.8-7.2 16-16 16zM148 320h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 96h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm-96 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12zm192 0h-40c-6.6 0-12-5.4-12-12v-40c0-6.6 5.4-12 12-12h40c6.6 0 12 5.4 12 12v40c0 6.6-5.4 12-12 12z" class=""></path></svg>
											</span>
                                        </div>
                                        <input type='text' class="form-control" placeholder="Retorno" />
                                    </div>
                                </div>
                            </div>
                        </div><!--row-->
                    </div><!--col-lg-8-->

                    <div class="col-lg-4">
                        <div class="row">
                            <!--Select-->
                            <div class="col-4 col-sm-3 col-lg-4 pr-0">
                                <select class="form-control classic" id="exampleFormControlSelect1">
                                    <option selected>Adultos</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="col-4 col-sm-3 col-lg-4 pr-0">
                                <select class="form-control classic" id="exampleFormControlSelect1">
                                    <option selected>Crianças</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                            <div class="col-4 col-sm-3 col-lg-4">
                                <select class="form-control classic" id="exampleFormControlSelect1">
                                    <option selected>Bebês</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <small class="text-white">Tecnologia eFácilPlus</small>
                    </div>
                    <div class="col-lg-6">
                        <div class="text-right">
                            <a href="#" class="btn btn-lg bg-azul text-white px-5">
                                PROCURAR
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="aereos-busca-precos" class="py-5">
            <div class="container">
                <h1 class="font-weight-bold text-uppercase mb-5">Resultados Aéreos</h1>

                <div class="row border-azul d-none d-lg-block">
                    <div class="col-lg-12 bg-azul p-2">
                        <h5 class="text-uppercase text-white mb-0">Melhores preços por companhia</h5>
                    </div>
                    <div class="col-lg-12 border-bottom">
                        <div class="row">
                            <div class="col">

                            </div>
							<?php
							foreach ($melhoresPrecos as $value) {
								?>

                                <div class="col py-2">
									<?php
									if ($value["Nome"] === "Avianca") {
										$img = "avianca-logo.png";
										$wd = "w-50";
									} else if ($value["Nome"] === "Gol"){
										$img = "gol-logo.png";
										$wd = "w-25";
									} else if ($value["Nome"] === "Latam"){
										$img = "latam-logo.png";
										$wd = "w-50";
									} else if ($value["Nome"] === "Azul"){
										$img = "azul-logo.png";
										$wd = "w-50";
									}
									?>
                                    <img src="<?=get_template_directory_uri();?>/img/aereos/<?=$img;?>" class="img-fluid <?=$wd;?> m-auto d-block">
                                </div>
								<?php
							}
							?>

                            <!-- <div class="col py-2">
								<img src="img/aereos/gol-logo.png" class="img-fluid w-25 m-auto d-block">
							</div>
							<div class="col py-2">
								<img src="img/aereos/avianca-logo.png" class="img-fluid w-50 m-auto d-block">
							</div>
							<div class="col py-2">
								<img src="img/aereos/azul-logo.png" class="img-fluid w-50 m-auto d-block">
							</div> -->
                        </div>
                    </div>
                    <div class="col-lg-12 border-bottom">
                        <div class="row">
                            <div class="col align-self-center">
                                <h5 class="text-uppercase text-laranja text-right mb-0">Total Geral</h5>
                            </div>

							<?php
							foreach ($melhoresPrecos as $value) {
								?>
                                <div class="col py-2">
                                    <h5 class="text-uppercase text-azul text-center mb-0">R$ <?=number_format($value["TotalGeral"],2,",",".");?></h5>
                                </div>

								<?php
							}
							?>

                            <!-- <div class="col align-self-center">
								<h5 class="text-uppercase text-laranja text-right mb-0">Voos diretos</h5>
							</div>
							<div class="col py-2">
							
							</div>
							<div class="col py-2">
								<h5 class="text-uppercase text-azul text-center mb-0">R$ 3.710,64</h5>
							</div>
							<div class="col py-2">
								<h5 class="text-uppercase text-azul text-center mb-0">R$ 4.410,64</h5>
							</div>
							<div class="col py-2">
							
							</div> -->
                        </div>
                    </div>
                    <!-- <div class="col-lg-12 border-bottom">
						<div class="row">
							<div class="col align-self-center">
								<h5 class="text-uppercase text-laranja text-right mb-0">1 Parada</h5>
							</div>
							<div class="col py-2">
								<h5 class="text-uppercase text-azul text-center mb-0">R$ 3.710,64</h5>
							</div>
							<div class="col py-2">
								<h5 class="text-uppercase text-azul text-center mb-0">R$ 3.710,64</h5>
							</div>
							<div class="col py-2">
								<h5 class="text-uppercase text-azul text-center mb-0">R$ 4.410,64</h5>
							</div>
							<div class="col py-2">
								<h5 class="text-uppercase text-azul text-center mb-0">R$ 4.410,64</h5>
							</div>
						</div>
					</div> -->
                    <!-- <div class="col-lg-12 ">
						<div class="row">
							<div class="col align-self-center">
								<h5 class="text-uppercase text-laranja text-right mb-0">mais Parada</h5>
							</div>
							<div class="col py-2">
								<h5 class="text-uppercase text-azul text-center mb-0">R$ 3.710,64</h5>
							</div>
							<div class="col py-2">
					
							</div>
							<div class="col py-2">
					
							</div>
							<div class="col py-2">
								<h5 class="text-uppercase text-azul text-center">R$ 4.410,64</h5>
							</div>
						</div>
					</div> -->
                </div>
            </div>
        </section>

        <section id="aereos-filtros" class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 bg-laranja p-3">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="bagagens" name="bagagens" value="sim">
                            <label class="form-check-label text-white" for="bagagens">Apenas voos com bagagens</label>
                        </div>
                    </div>
                    <div class="col align-self-center">
                        <a href="">
                            <div class="media">
                                <div class="media-body">
                                    <p class="text-muted float-right mb-0">Limpar filtros</p>
                                </div>
                                <img src="<?=get_template_directory_uri();?>/img/aereos/icons/delete.svg" class="icon">
                            </div>
                        </a>
                    </div>
                </div>
                <div class="row border">
                    <div class="col border-right text-center p-3">
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" data-toggle="dropdown" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?=get_template_directory_uri();?>/img/aereos/icons/plane.svg" class="icon-filtro">
                                <p class="text-azul font-14 mb-0">Companhias aéreas</p>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <div class="form-check px-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label font-14" for="customCheck1">Todas</label>
                                    </div>
                                </div>
                                <div class="dropdown-divider"></div>
                                <div class="form-check px-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label font-14" for="customCheck2">Latam</label>
                                    </div>
                                </div>
                                <div class="form-check px-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck3">
                                        <label class="custom-control-label font-14" for="customCheck3">Gol Linhas Aéreas</label>
                                    </div>
                                </div>
                                <div class="form-check px-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck4">
                                        <label class="custom-control-label font-14" for="customCheck4">Avianca Brasil</label>
                                    </div>
                                </div>
                                <div class="form-check px-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck5">
                                        <label class="custom-control-label font-14" for="customCheck5">Azul Linhas Aéreas</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col border-right text-center p-3">
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" data-toggle="dropdown" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?=get_template_directory_uri();?>/img/aereos/icons/escala.svg" class="icon-filtro">
                                <p class="text-azul font-14 mb-0">Número de Paradas</p>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <div class="form-check px-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck6">
                                        <label class="custom-control-label font-14" for="customCheck6">Todas</label>
                                    </div>
                                </div>
                                <div class="dropdown-divider"></div>
                                <div class="form-check px-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck7">
                                        <label class="custom-control-label font-14" for="customCheck7">Voo Direto</label>
                                    </div>
                                </div>
                                <div class="form-check px-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck8">
                                        <label class="custom-control-label font-14" for="customCheck8">1 parada</label>
                                    </div>
                                </div>
                                <div class="form-check px-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck9">
                                        <label class="custom-control-label font-14" for="customCheck9">2 paradas</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col border-right text-center p-3">
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" data-toggle="dropdown" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?=get_template_directory_uri();?>/img/aereos/icons/clock.svg" class="icon-filtro">
                                <p class="text-azul font-14 mb-0">Horários de voo</p>
                            </button>
                            <div class="dropdown-menu p-3" aria-labelledby="dropdownMenuButton">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <p>Horário de ida</p>
                                    </div>
                                    <div class="col-lg-6">
                                        <span class="slider-time-ida text-laranja">00:00 AM</span> - <span class="slider-time-ida-2 text-laranja">11:59 PM</span>
                                    </div>

                                </div>
                                <div class="slider-range-container">
                                    <div id="slider-range-date-ida" class="slider-range"></div>
                                </div>

                                <div class="border-top pb-3"></div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <p>Horário de volta</p>
                                    </div>
                                    <div class="col-lg-6">
                                        <span class="slider-time text-laranja">00:00 AM</span> - <span class="slider-time2 text-laranja">11:59 PM</span>
                                    </div>
                                </div>
                                <div class="slider-range-container">
                                    <div id="slider-range-date-volta" class="slider-range"></div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col border-right text-center p-3">
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" data-toggle="dropdown" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?=get_template_directory_uri();?>/img/aereos/icons/valores.png" class="img-fluid">
                                <p class="text-azul font-14 mb-0">Valores</p>
                            </button>
                            <div class="dropdown-menu p-3" aria-labelledby="dropdownMenuButton">
                                <p class="text-right" style="padding: 4% 4%;">
                                    <input id="amount" type="text" class="amount text-laranja" readonly>
                                </p>
                                <div class="slider-range-container">
                                    <div id="slider-range" class="slider-range"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col text-center p-3">
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" data-toggle="dropdown" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?=get_template_directory_uri();?>/img/aereos/icons/airport.svg" class="icon-filtro">
                                <p class="text-azul font-14 mb-0">Aeroportos</p>
                            </button>
                            <div class="dropdown-menu pb-0" aria-labelledby="dropdownMenuButton">
                                <div class="form-check px-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck10">
                                        <label class="custom-control-label font-14" for="customCheck10">Todos</label>
                                    </div>
                                </div>
                                <div class="dropdown-divider"></div>
                                <div class="form-check px-2">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck11">
                                        <label class="custom-control-label font-14" for="customCheck11">Ida e volta pelo mesmo aeroporto</label>
                                    </div>
                                </div>
                                <div class="bg-light pb-3">
                                    <p class="pl-2 mb-0 pt-2">Ida/Volta</p>

                                    <div class="form-check px-2">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck12">
                                            <label class="custom-control-label font-14" for="customCheck12">Congonhas</label>
                                        </div>
                                    </div>
                                    <div class="form-check px-2">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck13">
                                            <label class="custom-control-label font-14" for="customCheck13">Guarulhos</label>
                                        </div>
                                    </div>
                                    <div class="form-check px-2">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck14">
                                            <label class="custom-control-label font-14" for="customCheck14">Viracopos</label>
                                        </div>
                                    </div>
                                    <div class="dropdown-divider"></div>
                                    <p class="pl-2 mb-0 pb-2">Chegada/Partida</p>

                                    <div class="form-check px-2">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck15">
                                            <label class="custom-control-label font-14" for="customCheck15">Pinto Martins</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>

        <section id="resultados-voo" class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 px-0 border-top-laranja">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-3 bg-azul p-3">
                                    <h4 class="text-white text-uppercase mb-0"><img src="<?=get_template_directory_uri();?>/img/aereos/icons/air-plane-branco.png" class="img-fluid mr-2">Ida</h4>
                                    <p class="text-white font-14 mb-0">Terça, 15 MAI/18</p>
                                </div>
                                <div class="col-lg-9 bg-cinza p-3">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <h4 class="text-uppercase mb-0 text-laranja">GRU</h4>
                                            <p class="font-14 mb-0">São Paulo</p>
                                        </div>
                                        <div class="col-lg-3 align-self-center">
                                            <img src="<?=get_template_directory_uri();?>/img/aereos/icons/air-plane-cinza.png" class="img-fluid">
                                        </div>
                                        <div class="col-lg-3">
                                            <h4 class="text-uppercase mb-0 text-laranja">FOR</h4>
                                            <p class="font-14 mb-0">Fortaleza</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 border-left">
                            <div class="row">
                                <div class="col">
                                    <p class="text-cinza mb-1 font-14">CIA</p>
                                </div>
                                <div class="col">
                                    <p class="text-cinza mb-0 font-14">VOO</p>
                                </div>
                                <div class="col">
                                    <p class="text-cinza mb-0 font-14">SAÍDA</p>
                                </div>
                                <div class="col">
                                </div>
                                <div class="col">
                                    <p class="text-cinza mb-0 font-14">CHEGADA</p>
                                </div>
                                <div class="col">
                                    <p class="text-cinza mb-0 font-14 text-center">BAGAGEM</p>
                                </div>
                                <div class="col">
                                    <p class="text-cinza mb-0 font-14">INFORMAÇÕES</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 border-bottom border-left">
                            <div class="row">
                                <div class="col">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadio1"><img src="<?=get_template_directory_uri();?>/img/aereos/gol-logo.png" class="img-fluid w-50"></label>
                                    </div>
                                </div>
                                <div class="col">
                                    <p>1530</p>
                                </div>
                                <div class="col">
                                    <p><span class="text-cinza">15/10</span> - 20h05</p>
                                </div>
                                <div class="col">
                                    <p class="text-azul font-14 text-center"><span>Voo Direto</span><br>3h15min</p>
                                </div>
                                <div class="col">
                                    <p><span class="text-cinza">15/10</span> - 23h20</p>
                                </div>
                                <div class="col col_bag">
                                    <img src="<?=get_template_directory_uri();?>/img/aereos/icons/com_bagagem.png" class="m-auto d-block">
                                    <div class="com_bagagem">
                                        <span class="valor_bagagem com_bag"><i class="fas fa-check"></i></span>
                                    </div>
                                </div>
                                <div class="col">
                                    <a class="text-azul" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">Detalhes <i class="fas fa-plus-circle"></i></a>
                                </div>
                            </div>
                            <div class="collapse pb-3" id="collapseExample">
                                <div class="bg-cinza p-3">
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <h5 class="text-azul text-uppercase">Voo 01</h5>

                                            <p class="font-14 text-azul mb-0">N° do voo</p>
                                            <p>Voo AD 2410</p>

                                            <p class="font-14 text-azul mb-0">Duração</p>
                                            <p>1h10min</p>

                                            <p class="font-14 text-azul mb-0">Companhia</p>
                                            <p>Gol Linhas Aéreas</p>

                                            <p class="font-14 text-azul mb-0">Avião</p>
                                            <p>E95</p>
                                        </div>
                                        <div class="col-lg-3">
                                            <h5 class="text-azul text-uppercase">SAÍDA</h5>

                                            <p class="font-14 text-azul mb-0">Local</p>
                                            <p>GRU - São Paulo</p>

                                            <p class="font-14 text-azul mb-0">Data</p>
                                            <p>15/10/2018</p>

                                            <p class="font-14 text-azul mb-0">Horário</p>
                                            <p>06:00h</p>
                                        </div>
                                        <div class="col-lg-2">
                                            <img src="<?=get_template_directory_uri();?>/img/aereos/border-air.png" class="img-fluid">
                                        </div>
                                        <div class="col-lg-3">
                                            <h5 class="text-azul text-uppercase">CHEGADA</h5>

                                            <p class="font-14 text-azul mb-0">Local</p>
                                            <p>CNF - Belo Horizonte</p>

                                            <p class="font-14 text-azul mb-0">Data</p>
                                            <p>15/10/2018</p>

                                            <p class="font-14 text-azul mb-0">Horário</p>
                                            <p>07:10h</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                        <div class="col-lg-12 border-bottom border-left">
                            <div class="row">
                                <div class="col">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadio2"><img src="<?=get_template_directory_uri();?>/img/aereos/gol-logo.png" class="img-fluid w-50"></label>
                                    </div>
                                </div>
                                <div class="col">
                                    <p>1530</p>
                                </div>
                                <div class="col">
                                    <p><span class="text-cinza">15/10</span> - 20h05</p>
                                </div>
                                <div class="col">
                                    <p class="text-azul font-14 text-center"><span>Voo Direto</span><br>3h15min</p>
                                </div>
                                <div class="col">
                                    <p><span class="text-cinza">15/10</span> - 23h20</p>
                                </div>
                                <div class="col col_bag">
                                    <img src="<?=get_template_directory_uri();?>/img/aereos/icons/com_bagagem.png" class="m-auto d-block">
                                    <span class="valor_bagagem">x</span>
                                </div>
                                <div class="col">
                                    <a href="#" class="text-azul">Detalhes <i class="fas fa-plus-circle"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 border-left">
                            <div class="row">
                                <div class="col">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadio3"><img src="<?=get_template_directory_uri();?>/img/aereos/gol-logo.png" class="img-fluid w-50"></label>
                                    </div>
                                </div>
                                <div class="col">
                                    <p>1530</p>
                                </div>
                                <div class="col">
                                    <p><span class="text-cinza">15/10</span> - 20h05</p>
                                </div>
                                <div class="col">
                                    <p class="text-azul font-14 text-center"><span>Voo Direto</span><br>3h15min</p>
                                </div>
                                <div class="col">
                                    <p><span class="text-cinza">15/10</span> - 23h20</p>
                                </div>
                                <div class="col col_bag">
                                    <img src="<?=get_template_directory_uri();?>/img/aereos/icons/com_bagagem.png" class="m-auto d-block">
                                    <span class="valor_bagagem">x</span>
                                </div>
                                <div class="col">
                                    <a href="#" class="text-azul">Detalhes <i class="fas fa-plus-circle"></i></a>
                                </div>
                            </div>
                        </div>


                        <!---VOLTA--->
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-3 bg-azul p-3">
                                    <h4 class="text-white text-uppercase mb-0"><img src="<?=get_template_directory_uri();?>/img/aereos/icons/air-plane-branco-volta.png" class="img-fluid mr-2">VOLTA</h4>
                                    <p class="text-white font-14 mb-0">Terça, 15 MAI/18</p>
                                </div>
                                <div class="col-lg-9 bg-cinza p-3">
                                    <div class="row">
                                        <div class="col col-sm-3 col-lg-3">
                                            <h4 class="text-uppercase mb-0 text-laranja">GRU</h4>
                                            <p class="font-14 mb-0">São Paulo</p>
                                        </div>
                                        <div class="col col-sm-3 col-lg-3 align-self-center">
                                            <img src="<?=get_template_directory_uri();?>/img/aereos/icons/air-plane-cinza.png" class="img-fluid">
                                        </div>
                                        <div class="col col-sm-3 col-lg-3">
                                            <h4 class="text-uppercase mb-0 text-laranja">FOR</h4>
                                            <p class="font-14 mb-0">Fortaleza</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 border-left">
                            <div class="row">
                                <div class="col">
                                    <p class="text-cinza mb-1 font-14">CIA</p>
                                </div>
                                <div class="col">
                                    <p class="text-cinza mb-0 font-14">VOO</p>
                                </div>
                                <div class="col">
                                    <p class="text-cinza mb-0 font-14">SAÍDA</p>
                                </div>
                                <div class="col">
                                </div>
                                <div class="col">
                                    <p class="text-cinza mb-0 font-14">CHEGADA</p>
                                </div>
                                <div class="col">
                                    <p class="text-cinza mb-0 font-14 text-center">BAGAGEM</p>
                                </div>
                                <div class="col">
                                    <p class="text-cinza mb-0 font-14">INFORMAÇÕES</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 border-bottom border-left">
                            <div class="row">
                                <div class="col">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio14" name="customRadio" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadio14"><img src="<?=get_template_directory_uri();?>/img/aereos/gol-logo.png" class="img-fluid w-50"></label>
                                    </div>
                                </div>
                                <div class="col">
                                    <p>1530</p>
                                </div>
                                <div class="col">
                                    <p><span class="text-cinza">15/10</span> - 20h05</p>
                                </div>
                                <div class="col">
                                    <p class="text-azul font-14 text-center"><span>Voo Direto</span><br>3h15min</p>
                                </div>
                                <div class="col">
                                    <p><span class="text-cinza">15/10</span> - 23h20</p>
                                </div>
                                <div class="col col_bag">
                                    <img src="<?=get_template_directory_uri();?>/img/aereos/icons/com_bagagem.png" class="m-auto d-block">
                                    <span class="valor_bagagem">x</span>
                                </div>
                                <div class="col">
                                    <a href="#" class="text-azul">Detalhes <i class="fas fa-plus-circle"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 border-bottom border-left">
                            <div class="row">
                                <div class="col">
                                    <div class="custom-control custom-radio">
                                        <input type="radio" id="customRadio13" name="customRadio" class="custom-control-input">
                                        <label class="custom-control-label" for="customRadio13"><img src="<?=get_template_directory_uri();?>/img/aereos/gol-logo.png" class="img-fluid w-50"></label>
                                    </div>
                                </div>
                                <div class="col">
                                    <p>1530</p>
                                </div>
                                <div class="col">
                                    <p><span class="text-cinza">15/10</span> - 20h05</p>
                                </div>
                                <div class="col">
                                    <p class="text-azul font-14 text-center"><span>Voo Direto</span><br>3h15min</p>
                                </div>
                                <div class="col">
                                    <p><span class="text-cinza">15/10</span> - 23h20</p>
                                </div>
                                <div class="col col_bag">
                                    <img src="<?=get_template_directory_uri();?>/img/aereos/icons/com_bagagem.png" class="m-auto d-block">
                                    <span class="valor_bagagem">x</span>
                                </div>
                                <div class="col">
                                    <a href="#" class="text-azul">Detalhes <i class="fas fa-plus-circle"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 px-0">
                        <div class="bg-laranja p-3">
                            <p class="text-center text-white text-uppercase mb-0">Tarifa por pessoa</p>
                            <h4 class="text-azul text-center text-uppercase">R$ 713,80</h4>

                            <div class="row">
                                <div class="col-8 col-sm-8 col-lg-6">
                                    <p class="text-white font-14 mb-0">2 Adultos</p>
                                    <p class="text-white font-14 mb-0">2 Crianças</p>
                                    <p class="text-white font-14 mb-0">Taxas:</p>
                                </div>
                                <div class="col-4 col-sm-4 col-lg-6">
                                    <p class="text-white font-14 mb-0 float-right">R$ 1.427,60</p>
                                    <p class="text-white font-14 mb-0 float-right">R$ 1.070,60</p>
                                    <p class="text-white font-14 mb-0 float-right">R$ 517,06</p>
                                </div>
                            </div>
                            <div class="border-bottom mb-2"></div>
                            <div class="row">
                                <div class="col-6 col-sm-6 col-lg-6">
                                    <h5 class="text-white text-uppercase">Total</h5>
                                </div>
                                <div class="col-6 col-sm-6 col-lg-6">
                                    <h5 class="text-azul text-uppercase float-right">R$ 3.015,34</h5>
                                </div>
                            </div>
                        </div>
                        <div class="bg-cinza p-3">
                            <p class="mb-0">Ficou interessado?</p>
                            <p>Preencha o formulário e solicite a reserva para esses voos.</p>
                            <form>
                                <input type="name" class="form-control mb-3" id="nome" aria-describedby="emailHelp" placeholder="Nome">
                                <input type="phone" class="form-control mb-3" id="telefone" aria-describedby="emailHelp" placeholder="Telefone">
                                <input type="email" class="form-control mb-3" id="email" aria-describedby="emailHelp" placeholder="Email">
                                <button type="submit" class="btn bg-laranja mb-2 text-white px-4 w-100">Enviar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

    <script type="text/javascript">
        var disponibilidade = [];

        alert(typeof disponibilidade);



        var trecho = "<?php print $data['trecho'];?>";
        var dataPartida = "<?php print $data['dataPartida'];?>";
        var dataRetorno = "<?php print $data['dataRetorno'];?>";
        var origem = "<?php print $data['origem'];?>";
        var destino = "<?php print $data['destino'];?>";
        var adultos = "<?php print $data['adultos'];?>";
        var criancas = "<?php print $data['criancas'];?>";
        var bebes = "<?php print $data['bebes'];?>";

        $("#bagagens").change(function() {
            if ($("#bagagens").prop("checked") === true) {
                var bagagens = true;

                var url = "/mendestur-front/wp-content/themes/mendestur-front/webservices/busca-bagagens.php";

                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        trecho: trecho,
                        origem: origem,
                        destino: destino,
                        dataPartida: dataPartida,
                        dataRetorno: dataRetorno,
                        adultos: adultos,
                        criancas: criancas,
                        bebes: bebes
                    },
                    success: function (data) {
                        alert(data);
                    }
                });

                var url2 = "/mendestur-front/wp-content/themes/mendestur-front/webservices/recupera-disponibilidade.php";

                $.ajax({
                    type: "POST",
                    url: url2,
                    data: {
                        trecho: trecho,
                        origem: origem,
                        destino: destino,
                        dataPartida: dataPartida,
                        dataRetorno: dataRetorno,
                        adultos: adultos,
                        criancas: criancas,
                        bebes: bebes
                    },
                    success: function (data2){
                        disponibilidade = data2;
                        alert(disponibilidade);
                    }
                });
            }
        });


    </script>


<?php get_footer('newtmpl'); ?>