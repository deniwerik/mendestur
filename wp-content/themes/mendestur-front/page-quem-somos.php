<?php
// Template Name: Quem Somos
get_header('newtmpl');
?>

    <main>
        <section id="quem-somos-header" class="py-5">
            <div class="container">
                <h1 class="text-center text-white text-uppercase bottom-line-header line-center">QUEM SOMOS</h1>
            </div>
        </section>

        <section id="page-quem-somos" class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-8">
                        <h4 class="text-laranja text-uppercase font-weight-bold font-italic">Mendes Tur 30 anos!</h4>
                        <p>Se chegamos até aqui, é porque tem muito de você na nossa história.</p>
                        <p>A vida nos ensina: 30 anos correspondem à boa parte da trajetória de qualquer pessoa.</p>
                        <p>É um tempo suficiente para conhecermos pessoas especiais, aprendermos lições importantes e vivermos momentos únicos e inesquecíveis.</p>
                        <p>Foram muitos desafios e muito trabalho. Hoje, a Mendes Tur, após todos esses anos de profissionalismo e dedicação, sente-se gratificada por ter chegado até aqui.</p>
                        <p>E o melhor dessa história é que não foi uma conquista solitária. Em nenhum desses quase 11 mil dias estivemos sozinhos nessa caminhada, ao contrário, crescemos e amadurecemos nesse trajeto graças aos nossos clientes e aos nossos parceiros.</p>
                        <p>É justamente por termos chegado aos 30 anos com essa capacidade de superação que mantemos uma fé inabalável e muita confiança no futuro.</p>
                        <p>Desejamos nesse momento agradecer imensamente a todos os que acreditaram nos nossos serviços. A todos vocês, que em algum momento estiveram juntos com a Mendes Tur nessa viagem de 30 anos, o nosso muito obrigado.</p>
                    </div>
                    <!-- <div class="col-md-4 col-lg-4">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/quem-somos/mendes-tur.jpg" class="img-fluid pb-4">
                         <img src="<?php echo get_template_directory_uri(); ?>/img/quem-somos/equipe.jpg" class="img-fluid pb-4">
                    </div> -->
                </div>
            </div>
        </section>

        <section id="home-quem-somos" class="py-5">
            <div class="container">
                <div class="text-white text-center my-4">
                    <h5>A agência <strong>Mendes Tur Câmbio e Turismo</strong> foi fundada em <strong>1987</strong> e atua no setor de câmbio, turismo de lazer e corporativo.</h5>

                    <button type="button" class="video-btn input-group justify-content-center my-4" data-toggle="modal" data-src="https://www.youtube.com/embed/NG3ifyCxMy0" data-target="#myModal">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/icons/player.svg" class="img-fluid mr-3 icon-player">
                        <h5 class="align-self-center font-weight-bold text-white">ASSISTA AO VÍDEO</h5>
                    </button>
                </div>
            </div>
        </section>

        <!--MODAL-VIDO-->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <!-- 16:9 aspect ratio -->
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always">></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div> 

        <section id="equipe" class="py-5">
            <div class="container">
                <h3 class="text-center text-laranja text-uppercase font-weight-bold">Nossa equipe</h3>

                <h5 class="text-uppercase font-weight-bold">Matriz</h5>
                <div class="border-laranja"></div>
                <div class="border-branco "></div>

                <div class="row pt-4 mb-5 justify-content-center">
                    <div class="col-12 col-sm-12 col-lg-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/quem-somos/ana.jpg" class="img-fluid m-auto d-block">
                        <a href="#" class="text-center input-group font-14 pt-2 m-auto d-block"><i class="far fa-envelope text-laranja mr-2 align-self-center"></i> anamaria@mendestur.com.br</a>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/quem-somos/caroline.jpg" class="img-fluid m-auto d-block">
                        <a href="#" class="text-center input-group font-14 pt-2 m-auto d-block"><i class="far fa-envelope text-laranja mr-2 align-self-center"></i> caroline@mendestur.com.br</a>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/quem-somos/kelly.jpg" class="img-fluid m-auto d-block">
                        <a href="#" class="text-center input-group font-14 pt-2 m-auto d-block"><i class="far fa-envelope text-laranja mr-2 align-self-center"></i> kelly@mendestur.com.br</a>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/quem-somos/lailane.jpg" class="img-fluid m-auto d-block">
                        <a href="#" class="text-center input-group font-14 pt-2 m-auto d-block"><i class="far fa-envelope text-laranja mr-2 align-self-center"></i> lailaine@mendestur.com.br</a>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/quem-somos/tania.jpg" class="img-fluid m-auto d-block">
                        <a href="#" class="text-center input-group font-14 pt-2 m-auto d-block"><i class="far fa-envelope text-laranja mr-2 align-self-center"></i> tania@mendestur.com.br</a>
                    </div>
                </div>

                <h5 class="text-uppercase font-weight-bold">Filial</h5>
                <div class="border-laranja"></div>
                <div class="border-branco "></div>

                <div class="row pt-4 justify-content-center">
                    <div class="col-12 col-sm-12 col-lg-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/quem-somos/celia.jpg" class="img-fluid m-auto d-block">
                        <a href="#" class="text-center input-group font-14 pt-2 m-auto d-block"><i class="far fa-envelope text-laranja mr-2 align-self-center"></i> celia@mendestur.com.br</a>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/quem-somos/cristina.jpg" class="img-fluid m-auto d-block">
                        <a href="#" class="text-center input-group font-14 pt-2 m-auto d-block"><i class="far fa-envelope text-laranja mr-2 align-self-center"></i> cristina@mendestur.com.br</a>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-2">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/quem-somos/marta.jpg" class="img-fluid m-auto d-block">
                        <a href="#" class="text-center input-group font-14 pt-2 m-auto d-block"><i class="far fa-envelope text-laranja mr-2 align-self-center"></i> marta@mendestur.com.br</a>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-2">
                       <img src="<?php echo get_template_directory_uri(); ?>/img/quem-somos/simone.jpg" class="img-fluid m-auto d-block">
                        <a href="#" class="text-center input-group font-14 pt-2 m-auto d-block"><i class="far fa-envelope text-laranja mr-2 align-self-center"></i> rosa@mendestur.com.br</a>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-2">
                    	<img src="<?php echo get_template_directory_uri(); ?>/img/quem-somos/rosa.jpg" class="img-fluid m-auto d-block">
                        <a href="#" class="text-center input-group font-14 pt-2 m-auto d-block"><i class="far fa-envelope text-laranja mr-2 align-self-center"></i> simone@mendestur.com.br</a>
                    </div>
                </div>
            </div>
        </section>



        <!-- <?php
        require_once (TEMPLATEPATH."/includes/instagram.php");
        ?> -->
    </main>

<?php get_footer('newtmpl'); ?>