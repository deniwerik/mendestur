<?php
	get_header('newtmpl');
?>
	<main>
		<section id="page-404" class="pt-3">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-l-g-6 img">
						<img src="<?php echo get_template_directory_uri(); ?>/img/404.png" class="img-fluid">
					</div>
					<div class="col-md-6 col-l-g-6 text align-self-center pl-5">
						<h3 class="text-azul bottom-line pb-4">404! Página não encontrada...</h3>

						<p class="mb-0">Página não encontrada ou outro erro deve ter acontecido.</p>
						<p>Volte para a <a onclick="window.history.back();" class="text-laranja">PÁGINA ANTERIOR</a> ou vá para a:</p>
						<a href="http://mendestur.com.br" class="btn btn-lg bg-laranja text-white text-center px-5 mt-3">
                            HOME
                        </a>
					</div>
				</div>
			</div>
		</section>

		<section id="page-destinos" class="py-5 bg-cinza-claro">
				<div class="container">
					<div class="input-group">
						<h3 class="text-uppercase text-azul align-self-center mb-0 mr-3">CONFIRA ALGUNS DE NOSSOS DESTINOS</h3>
						<a href="<?php echo get_permalink( get_page_by_path( 'destino' ) ); ?>"
						   class="btn btn-lg bg-azul text-white px-4">
							VER TODOS
						</a>
					</div>
					<div class="row align-items-stretch pt-5">
						<?php

						$args = array("post_type"=>"destinos","order"=>"ASC");
						$the_query = new WP_Query($args);

						if ($the_query->have_posts()){
							while ($the_query->have_posts()){
								$the_query->the_post();
								?>
								<div class="col-sm-4 col-md-6 col-lg-3 mb-4 d-flex align-items-stretch">
									<div class="card">
										<img class="img-fluid card-img-top" src="<?php the_field('imagem'); ?>">
										<div class="card-body">
											<p class="font-14 mb-0 mt-1"><?php the_field('saida');?></p>
											<h5 class="font-weight-bold text-laranja mb-0"><?php
                                                $titulo = get_field('titulo');
												$titulo = mb_strtolower($titulo,'UTF-8');
												if (strlen($titulo) > 24){
													$titulo = mb_substr($titulo,0,21);
													$titulo = mb_convert_case($titulo, MB_CASE_TITLE,"UTF-8");
													$titulo .= "...";
												}
												echo $titulo;
                                                ?></h5>
											<p class="font-weight-bold mb-0"><?php the_field('dias'); ?> dias</p>
											<div class="border my-2"></div>
											<p class="font-14 mb-0"><?php the_field('entrada');?></p>
											<h4 class="text-azul font-weight-bold mb-0"><?php the_field('valor_parcelado');?></h4>
                                        	<p class="font-14 mb-0 mt-1">Total: <?php the_field('valor_total');?></p>
										</div>
										 <div class="card-footer">
                                        <div class="row">
                                            <div class="col-4 col-sm-4 col-lg-4 p-0">
                                                <img src="<?php the_field('logo');?>" class="img-fluid img-logo">
                                            </div>
                                            <div class="col-8 col-sm-8  col-lg-8">
                                                <a href="<?php the_permalink();?>"
                                                   class="btn btn-lg bg-laranja text-white px-4">
                                                    DETALHES
                                                </a>
                                            </div>
                                        </div>
                                    </div>
									</div>
								</div>
								<?php
							}
						}
						?>

					</div>
				</div>
			</section>




	</main>
<?php get_footer('newtmpl'); ?>