<?php
// Template Name: Seguro viagem
get_header('newtmpl');
?>

	<main>
		<section id="seguro-header" class="py-5">
			<div class="container">
				<h1 class="text-center text-white text-uppercase bottom-line-header line-center">SEGURO VIAGEM</h1>
			</div>
		</section>

		<section id="page-seguro" class="py-5">
			<div class="container">
				<h3 class="text-azul text-center">Sua viagem mais tranquila e segura!</h3>

				<img src="<?php echo get_template_directory_uri(); ?>/img/seguro-viagem/banner-seguro-viagem.jpg" class="img-fluid m-auto d-block py-5">

				<p>A Mendes Tur através da Global Travel Assistance possui um portfólio de produtos para atender aos diversos tipos de viagens como: a Lazer, trabalho, religioso, estudantil, cruzeiros marítimos e esportivos podendo ser de curta ou longa duração.</p>
				<p>Os produtos são uma composição de Seguro Viagem somados ao pacote de Assistência de Viagem para atender os passageiros em viagem pelo Brasil  ou para o Exterior.</p>
				<p>Cuidamos de pessoas e vamos assistir os viajantes em situações de emergências, pois imprevistos podem ocorrer.</p>
				<p>A Global Travel Assistance atua a mais de 25 anos no mercado brasileiro e é especializada na organização e atendimento nos momentos de urgência e emergência.</p>

				<div class="form-orcamento bg-cinza p-4 mt-4">
					<h5 class="font-weight-bold">Quer fazer um Seguro Viagem?</h5>
					<p>Preencha o formulário abaixo, e solicite maiores informações</p>
					<!-- <form>
						<p class="border-bottom">Dados da Viagem</p>
						<div class="row">
							<div class="col-12 col-sm-12 col-lg-4 pb-2">
								<input type="text" class="form-control" placeholder="Destino*">
							</div>
							<div class="col-12 col-sm-12 col-lg-2 pb-2">
								<select class="form-control classic" id="exampleFormControlSelect1">
									<option selected>Adultos</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>

							<div class="col-12 col-sm-12 col-lg-2 pb-2">
								<select class="form-control classic" id="exampleFormControlSelect1">
									<option selected>Crianças</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
								</select>
							</div>
							<div class="col-12 col-sm-12 col-lg-4 pb-2">
								<input type="text" class="form-control" placeholder="Quantidade de dias">
							</div>
						</div>
						<p class="border-bottom">Dados Pessoais</p>
						<div class="row">
							<div class="col-12 col-sm-12 col-lg-4 pb-2">
								<input type="text" class="form-control" placeholder="Nome">
							</div>
							<div class="col-12 col-sm-12 col-lg-4 pb-2">
								<input type="text" class="form-control" placeholder="Telefone">
							</div>
							<div class="col-12 col-sm-12 col-lg-4 pb-2">
								<input type="text" class="form-control" placeholder="Email">
							</div>
						</div>
						<textarea class="form-control" id="exampleFormControlTextarea1" rows="4" placeholder="Informações complementares"></textarea>
						<button type="submit" class="btn bg-laranja text-white px-5 mt-3 mb-2">ENVIAR</button>
					</form> -->
					<?php echo do_shortcode( '[contact-form-7 id="174" title="Seguro_Viagem"]' ); ?>
				</div>
			</div>
		</section>


    </main>

<?php get_footer('newtmpl'); ?>