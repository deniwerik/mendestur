<?php
session_start();
//header("Access-Control-Allow-Origin: *");
require_once ("../classes/nusoap.php");


$cliente = new nusoap_client("http://ancoradouro.com.br/mondiale/webservice/service.php?wsdl");

$parametros = array('login'=>'mendestur','senha'=>'@1D2#$65par#');

$resultado = $cliente->call('getUser',$parametros);
$chave = $resultado['chave'];

//var_dump($chave);

$parametros2 = array($chave,0);
$resultado2 = $cliente->call('getContinente',$parametros2);

//var_dump($resultado2);

$now = new DateTime("NOW");

$ids_paises = array();
foreach ($resultado2 as $key => $value){
    $params = array($chave,$value['id']);
    $resultado_pais = $cliente->call('getPais',$params);
    foreach ($resultado_pais as $key2 => $value2){
        array_push($ids_paises,$value2['id']);
    }
}

$pacotes = array();
foreach ($ids_paises as $id ){
    $params2 = array($chave,$id);
    $resultado_pacotes = $cliente->call("getPacotes",$params2);
    $resultado_pacotes["regiao"] = $id;
    array_push($pacotes,$resultado_pacotes);
}

$destinos = array();
foreach ($pacotes as $key => $value){
    if ($value !== null){
        foreach ($value as $key2 => $value2){
        	if (is_array($value2)){
		        $data_destino = $value2["data"];
		        $data_destino = new DateTime($data_destino);

		        if ($now > $data_destino){
			        $value2["regiao"] = $value["regiao"];
			        array_push($destinos,$value2);
		        }
	        }

        }
    }

}

$destinos_validos = array();
foreach ($destinos as $key => $value){
    $saida = $value["saida"];
    $saida = substr($saida,-10);
    $saida = str_replace(".","/",$saida);
    $dia = substr($saida,0,2);
    $mes = substr($saida,3,2);
    $ano = substr($saida,6,4);

    /*$date = $value["data"];
    $date = new DateTime($date);*/

    if (is_numeric($dia) && is_numeric($mes) && is_numeric($ano)){
        $data = $ano . "/" . $mes . "/" . $dia;
        $inicial = strpos($data,"/");
        $final = strrpos($data,"/");
        if ($inicial === 4 && $final === 7){
            $data = new DateTime($data);

            if ($now < $data){
                array_push($destinos_validos, $value);
            }
        }
    }

    if ($value["saida"] === "" || empty($value["saida"])){
        array_push($destinos_validos, $value);
    }
}

shuffle($destinos_validos);

$destinos_validos_limitados = array();
for ($i=0; $i < 12; $i++){
    array_push($destinos_validos_limitados,$destinos_validos[$i]);
}


$_SESSION["destinos_limitados"] = $destinos_validos_limitados;

header("Content-type:application/json");
echo json_encode($destinos_validos_limitados);
