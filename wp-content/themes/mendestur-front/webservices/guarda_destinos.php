<?php
session_start();
header("Access-Control-Allow-Origin: *");

$destino = $_POST["destino"];

$_SESSION["destino"] = $destino;

$return["err"] = "false";

header("Content-type:application/json");
echo json_encode($return);
