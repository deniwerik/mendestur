<?php
session_start();


if (isset($_SESSION["pacote"])){

    $pacote = array();
    $pacote = $_SESSION["pacote"];
} else {
    $pacotes = array();
    foreach ($_SESSION["pacotes"] as $key => $value){
        array_push($pacotes,$value);
    }
}


$idpacote = $_SESSION["idpacote"];
$idregiao = $_SESSION["idregiao"];
?>
<!Doctype html>
<html lang="pt-BR">
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mendes Tur</title>

    <!--normalize-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.css" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css" type="text/css" />

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!--FontAwesome-->
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>

    <!--Google-fonts-->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,700i" rel="stylesheet">

    <!--DatePicker-->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.standalone.css">

    <!--OwlCarosel-->
    <link rel="stylesheet" href="inc/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="inc/owlcarousel/owl.theme.default.min.css">

    <!--css-->
    <link rel="stylesheet" href="css/style.css">
</head>

<body data-spy="scroll" data-target="#navbar-example" data-offset="20" id="page-top">

<header class="bg-light pt-3">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <img src="img/logo.png" class="img-fluid w-50">
            </div>
            <div class="col-lg-10">
                <p class="float-right px-2">Miramar Shopping: <span class="text-azul mr-3">(13) 3279-9000</span> Praiamar Shopping: <span class="text-azul">(13) 3279-9000</span></p>
            </div>
            <span class="border-header"></span>
        </div>
    </div>
</header>

<nav id="mainNav" class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand d-none" href="#page-top"><img src="img/logo.png" class="img-fluid w-25"></a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <!--<span class="navbar-toggler-icon"></span>-->
            <span></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="index.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Aéreos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="cruzeiro.php">Cruzeiros</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="destino.php">Destinos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="page-orcamento.php">Orçamento</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Lua de Mel</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Câmbio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Quem somos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contato</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<main>

    <!--PESQUISA-->
    <section id="pesquisa" class="py-3 bg-laranja">
        <div class="container">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link pl-0 active" id="destino-tab" data-toggle="pill" href="#destino" role="tab" aria-controls="destino" aria-selected="true">Destinos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="aereo-tab" data-toggle="pill" href="#aereo" role="tab" aria-controls="aereo" aria-selected="false">Aéreos</a>
                </li>
                <li class="nav-item ml-auto">
                    <a href="" class="font-14 nav-link"><u>Não encontrou a viagem que gostaria?</u></a>
                </li>
            </ul>

            <div class="tab-content" id="pills-tabContent">

                <!--TAB-DESTINO-->
                <div class="tab-pane fade show active" id="destino" role="tabpanel" aria-labelledby="destino-tab">
                    <div class="row">
                        <!--data-->
                        <div class="col-lg-3">
                            <div class="form-group">
                                <div class="input-group date" data-provide="datepicker">
                                    <div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">
												<i class="far fa-calendar-alt"></i>
											</span>
                                    </div>
                                    <input type='text' class="form-control" placeholder="Escolha uma data" />
                                </div>
                            </div>
                        </div>

                        <!--cidade-->
                        <div class="col-lg-4">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><svg aria-hidden="true" data-prefix="fal" data-icon="map-marker-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-map-marker-alt fa-w-12 fa-lg"><path fill="currentColor" d="M192 96c-52.935 0-96 43.065-96 96s43.065 96 96 96 96-43.065 96-96-43.065-96-96-96zm0 160c-35.29 0-64-28.71-64-64s28.71-64 64-64 64 28.71 64 64-28.71 64-64 64zm0-256C85.961 0 0 85.961 0 192c0 77.413 26.97 99.031 172.268 309.67 9.534 13.772 29.929 13.774 39.465 0C357.03 291.031 384 269.413 384 192 384 85.961 298.039 0 192 0zm0 473.931C52.705 272.488 32 256.494 32 192c0-42.738 16.643-82.917 46.863-113.137S149.262 32 192 32s82.917 16.643 113.137 46.863S352 149.262 352 192c0 64.49-20.692 80.47-160 281.931z" class=""></path></svg></span>
                                </div>
                                <input type="text" class="form-control" placeholder="Escolha seu destino" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>

                        <!--Região-->
                        <div class="col-lg-3">
                            <select class="form-control classic" id="exampleFormControlSelect1">
                                <option selected>Escolha uma região</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <a href="#" class="btn btn-lg bg-azul text-white px-5">
                                PROCURAR
                            </a>
                        </div>
                    </div>
                </div>

                <!--TAB-AEREO-->
                <div class="tab-pane fade" id="aereo" role="tabpanel" aria-labelledby="aereo-tab">
                    <div class="form-check form-check-inline mb-3">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" checked>
                        <label class="form-check-label" for="inlineRadio1">Ida e volta</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                        <label class="form-check-label" for="inlineRadio2">Somente ida</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
                        <label class="form-check-label" for="inlineRadio3">Multiplos destinos</label>
                    </div>

                    <div class="row">
                        <!--Origem e destino-->
                        <div class="col">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><svg aria-hidden="true" data-prefix="fal" data-icon="map-marker-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-map-marker-alt fa-w-12 fa-lg"><path fill="currentColor" d="M192 96c-52.935 0-96 43.065-96 96s43.065 96 96 96 96-43.065 96-96-43.065-96-96-96zm0 160c-35.29 0-64-28.71-64-64s28.71-64 64-64 64 28.71 64 64-28.71 64-64 64zm0-256C85.961 0 0 85.961 0 192c0 77.413 26.97 99.031 172.268 309.67 9.534 13.772 29.929 13.774 39.465 0C357.03 291.031 384 269.413 384 192 384 85.961 298.039 0 192 0zm0 473.931C52.705 272.488 32 256.494 32 192c0-42.738 16.643-82.917 46.863-113.137S149.262 32 192 32s82.917 16.643 113.137 46.863S352 149.262 352 192c0 64.49-20.692 80.47-160 281.931z" class=""></path></svg></span>
                                </div>
                                <input type="text" class="form-control" placeholder="Origem" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><svg aria-hidden="true" data-prefix="fal" data-icon="map-marker-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-map-marker-alt fa-w-12 fa-lg"><path fill="currentColor" d="M192 96c-52.935 0-96 43.065-96 96s43.065 96 96 96 96-43.065 96-96-43.065-96-96-96zm0 160c-35.29 0-64-28.71-64-64s28.71-64 64-64 64 28.71 64 64-28.71 64-64 64zm0-256C85.961 0 0 85.961 0 192c0 77.413 26.97 99.031 172.268 309.67 9.534 13.772 29.929 13.774 39.465 0C357.03 291.031 384 269.413 384 192 384 85.961 298.039 0 192 0zm0 473.931C52.705 272.488 32 256.494 32 192c0-42.738 16.643-82.917 46.863-113.137S149.262 32 192 32s82.917 16.643 113.137 46.863S352 149.262 352 192c0 64.49-20.692 80.47-160 281.931z" class=""></path></svg></span>
                                </div>
                                <input type="text" class="form-control" placeholder="Destino" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>

                        <!--Data-->
                        <div class="col">
                            <div class="form-group">
                                <div class="input-group date" data-provide="datepicker">
                                    <div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">
												<i class="far fa-calendar-alt"></i>
											</span>
                                    </div>
                                    <input type='text' class="form-control" placeholder="Partida" />
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <div class="input-group date" data-provide="datepicker">
                                    <div class="input-group-prepend">
											<span class="input-group-text" id="basic-addon1">
												<i class="far fa-calendar-alt"></i>
											</span>
                                    </div>
                                    <input type='text' class="form-control" placeholder="Retorno" />
                                </div>
                            </div>
                        </div>

                        <!--Select-->
                        <div class="col">
                            <select class="form-control classic" id="exampleFormControlSelect1">
                                <option selected>Adultos</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                        <div class="col">
                            <select class="form-control classic" id="exampleFormControlSelect1">
                                <option selected>Crianças</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                        <div class="col">
                            <select class="form-control classic" id="exampleFormControlSelect1">
                                <option selected>Bebês</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>
                    <div class="text-right">
                        <a href="#" class="btn btn-lg bg-azul text-white px-5">
                            PROCURAR
                        </a>
                    </div>
                </div><!--tab-aereo-->

            </div><!--tab-content-->
        </div><!--container-->
    </section>

    <section id="destino-header" class="py-5">
        <div class="container">
            <h1 class="text-center text-white text-uppercase bottom-line-header line-center">Destino</h1>
        </div>
    </section>

    <section id="page-destinos" class="bg-cinza-claro py-5">
        <div class="container">
            <div class="row">

                <?php
                if (isset($pacote)) {
                    ?>
                    <div class="col-lg-3 mb-4">
                        <div class="card">
                            <img class="img-fluid card-img-top" src="<?=$pacote['imagem'];?>" alt="Card image cap">
                            <div class="card-body">
                                <h5 class="font-weight-bold text-laranja mb-0"><?=$pacote['titulo'];?></h5>
                                <p class="font-weight-bold mb-0"><?=$pacote['dias'];?> dias</p>
                                <div class="border"></div>
                                <p class="font-14 mb-0 mt-1">
                                    <?php
                                    $parc = $pacote["valor"] / 12;
                                    $parc = round($parc,2);
                                    $parc = str_replace(".",",",$parc);
                                    echo ("12x de " . $pacote['moeda'] . " " . $parc);
                                    ?>
                                    <!--2x de R$ 96,00-->
                                </p>
                                <div class="row">
                                    <div class="col-lg-8">
                                        <p class="text-azul font-weight-bold mb-0"><?=$pacote["moeda"];?> <span
                                                    class="h4 font-weight-bold"><?=$pacote['valor'].",00";?></span></p>
                                    </div>
                                    <div class="col-lg-4">
                                        <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                    </div>
                                </div>
                                <button class="btn btn-lg bg-laranja text-white px-4 btn-detalhes"
                                        data-regiao="<?=$idregiao;?>" data-pacote="<?=$idpacote;?>">
                                    VER DETALHES
                                </button>

                            </div>
                        </div>
                    </div>


                    <?php
                } else {
                    foreach ($pacotes as $key => $value) {
                        ?>

                        <div class="col-lg-3 mb-4">
                            <div class="card">
                                <img class="img-fluid card-img-top" src="<?= $value['imagem']; ?>" alt="Card image cap">
                                <div class="card-body">
                                    <h5 class="font-weight-bold text-laranja mb-0"><?= $value['titulo']; ?></h5>
                                    <p class="font-weight-bold mb-0"><?= $value['dias']; ?> dias</p>
                                    <div class="border"></div>
                                    <p class="font-14 mb-0 mt-1">
                                        <?php
                                        $parc = $value["valor"] / 12;
                                        $parc = round($parc, 2);
                                        $parc = str_replace(".", ",", $parc);
                                        echo("12x de " . $value['moeda'] . " " . $parc);
                                        ?>
                                        <!--2x de R$ 96,00-->
                                    </p>
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <p class="text-azul font-weight-bold mb-0"><?= $value["moeda"]; ?> <span
                                                        class="h4 font-weight-bold"><?= $value['valor'] . ",00"; ?></span>
                                            </p>
                                        </div>
                                        <div class="col-lg-4">
                                            <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                        </div>
                                    </div>
                                    <button class="btn btn-lg bg-laranja text-white px-4 btn-detalhes"
                                            data-regiao="<?= $idregiao; ?>" data-pacote="<?= $value["id"]; ?>">
                                        VER DETALHES
                                    </button>

                                </div>
                            </div>
                        </div>

                        <?php
                    }
                }
                ?>

                <!--<div class="col-lg-3 mb-4">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                            <p class="font-weight-bold mb-0">8 dias</p>
                            <div class="border"></div>
                            <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                            <div class="row">
                                <div class="col-lg-8">
                                    <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                </div>
                                <div class="col-lg-4">
                                    <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                </div>
                            </div>
                            <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                VER DETALHES
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                            <p class="font-weight-bold mb-0">8 dias</p>
                            <div class="border"></div>
                            <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                            <div class="row">
                                <div class="col-lg-8">
                                    <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                </div>
                                <div class="col-lg-4">
                                    <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                </div>
                            </div>
                            <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                VER DETALHES
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                            <p class="font-weight-bold mb-0">8 dias</p>
                            <div class="border"></div>
                            <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                            <div class="row">
                                <div class="col-lg-8">
                                    <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                </div>
                                <div class="col-lg-4">
                                    <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                </div>
                            </div>
                            <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                VER DETALHES
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                            <p class="font-weight-bold mb-0">8 dias</p>
                            <div class="border"></div>
                            <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                            <div class="row">
                                <div class="col-lg-8">
                                    <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                </div>
                                <div class="col-lg-4">
                                    <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                </div>
                            </div>
                            <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                VER DETALHES
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                            <p class="font-weight-bold mb-0">8 dias</p>
                            <div class="border"></div>
                            <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                            <div class="row">
                                <div class="col-lg-8">
                                    <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                </div>
                                <div class="col-lg-4">
                                    <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                </div>
                            </div>
                            <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                VER DETALHES
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                            <p class="font-weight-bold mb-0">8 dias</p>
                            <div class="border"></div>
                            <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                            <div class="row">
                                <div class="col-lg-8">
                                    <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                </div>
                                <div class="col-lg-4">
                                    <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                </div>
                            </div>
                            <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                VER DETALHES
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                            <p class="font-weight-bold mb-0">8 dias</p>
                            <div class="border"></div>
                            <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                            <div class="row">
                                <div class="col-lg-8">
                                    <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                </div>
                                <div class="col-lg-4">
                                    <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                </div>
                            </div>
                            <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                VER DETALHES
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                            <p class="font-weight-bold mb-0">8 dias</p>
                            <div class="border"></div>
                            <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                            <div class="row">
                                <div class="col-lg-8">
                                    <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                </div>
                                <div class="col-lg-4">
                                    <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                </div>
                            </div>
                            <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                VER DETALHES
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                            <p class="font-weight-bold mb-0">8 dias</p>
                            <div class="border"></div>
                            <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                            <div class="row">
                                <div class="col-lg-8">
                                    <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                </div>
                                <div class="col-lg-4">
                                    <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                </div>
                            </div>
                            <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                VER DETALHES
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                            <p class="font-weight-bold mb-0">8 dias</p>
                            <div class="border"></div>
                            <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                            <div class="row">
                                <div class="col-lg-8">
                                    <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                </div>
                                <div class="col-lg-4">
                                    <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                </div>
                            </div>
                            <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                VER DETALHES
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mb-4">
                    <div class="card">
                        <img class="img-fluid card-img-top" src="img/destinos/01.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="font-weight-bold text-laranja mb-0">Porto Seguro</h5>
                            <p class="font-weight-bold mb-0">8 dias</p>
                            <div class="border"></div>
                            <p class="font-14 mb-0 mt-1">12x de R$ 96,00</p>
                            <div class="row">
                                <div class="col-lg-8">
                                    <p class="text-azul font-weight-bold mb-0">R$ <span class="h4 font-weight-bold">1152,00</span></p>
                                </div>
                                <div class="col-lg-4">
                                    <img src="img/destinos/logo-abreu.jpg" class="img-fluid">
                                </div>
                            </div>
                            <a href="#" class="btn btn-lg bg-laranja text-white px-4">
                                VER DETALHES
                            </a>
                        </div>
                    </div>
                </div>-->
            </div>
        </div>
    </section>

    <section id="instagram" class="pb-5">
        <div class="container">
            <div class="input-group justify-content-center mt-5">
                <a href="#">
                    <i class="fab fa-instagram fa-3x mr-2"></i>
                    <h2 class="align-self-center text-laranja text-uppercase">#mendestur</h2>
                </a>
            </div>
        </div>
    </section>
</main>

<footer class="bg-azul pt-5 pb-4">
    <div class="container">
        <h5 class="text-white mb-4">Cadastre-se e receba ofertas no seu e-mail!</h5>
        <div class="row">
            <div class="col-lg-8">
                <form>
                    <div class="row">
                        <div class="col">
                            <input type="text" class="form-control" id="inputName" placeholder="Nome">
                        </div>
                        <div class="col">
                            <input type="password" class="form-control" id="inputEmail" placeholder="Email">
                        </div>
                        <div class="col-2">
                            <button type="submit" class="btn bg-laranja mb-2 text-white px-4">Enviar</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-3 align-self-center ml-auto">
                <div class="social-media float-right">
                    <h5 class="text-white">Siga-nos:</h5>
                    <div class="btn-circle">
                        <i class="fab fa-instagram fa-1x fa-inverse" aria-hidden="true"></i>
                    </div>
                    <div class="btn-circle">
                        <i class="fab fa-facebook-f fa-1x fa-inverse" aria-hidden="true"></i>
                    </div>
                </div>

            </div>
        </div>
        <nav id="mainNav" class="navbar navbar-expand-lg navbar-light pt-4">
            <ul class="navbar-nav m-auto text-uppercase">
                <li class="nav-item">
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Aéreos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Cruzeiros</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Destinos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Orçamento</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Lua de Mel</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Câmbio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Quem somos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contato</a>
                </li>
            </ul>
        </nav>
    </div>
</footer>

<div class="bg-azul-marinho py-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-8">
                <p class="text-white mb-0 font-weight-light">© Copyright - Alguns direitos reservados <strong>Mendes Tur</strong></p>
            </div>
            <div class="col-lg-6 col-md-4">
                <p class="text-white mb-0 float-right">Desenvolvido por <span class="font-weight-bold text-laranja">PXP Digital</span></p>
            </div>
        </div>
    </div>
</div>


<!-- jQuery first, then Popper.scripts, then Bootstrap JS -->
<!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>-->
<script src="scripts/jquery-3.3.1.js" type="application/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

<script src="scripts/js.js" type="application/javascript"></script>

<!--OwlCarousel-->
<script src="inc/owlcarousel/owl.carousel.min.js"></script>

<!--DatePicker-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.pt-BR.min.js"></script>

<!--Afixx-->
<script src="https://rawgit.com/bassjobsen/affix/master/assets/js/affix.js"></script>

<script>
    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:20,
            nav:true,
            dots: false,
            navText : ['<span class="uk-margin-small-right uk-icon"><img src="img/icons/arrows-01.svg" class="prev-icon"></span>','<span class="uk-margin-small-left uk-icon"><img src="img/icons/arrows-02.svg" class="prev-icon"></span>'],
            responsive:{
                0:{
                    items:1
                },
                640:{
                    items:2
                },
                960:{
                    items:3
                },
                1200:{
                    items:4
                }
            }
        })
    });
</script>

<script>
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        language: 'pt-BR',
        autoclose: true,
        zIndexOffset: 10,
    });
</script>

<script>
    $( document ).ready(function() {
        var affixElement = '#mainNav';

        $(affixElement).affix({
            offset: {
                // Distance of between element and top page
                top: function () {
                    return (this.top = $(affixElement).offset().top)
                },
                // when start #footer
                bottom: function () {
                    return (this.bottom = $('#footer').outerHeight(true))
                }
            }
        });
    });
</script>
</body>
</html>